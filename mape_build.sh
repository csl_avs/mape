#!/bin/bash

catkin_make -DCMAKE_EXPORT_COMPILE_COMMANDS=1 --pkg acronis_common
catkin_make -DCMAKE_EXPORT_COMPILE_COMMANDS=1 --pkg mape_misc mape_monitor mape_analyzer can_simulator
catkin_make -DCMAKE_EXPORT_COMPILE_COMMANDS=1 --pkg planner executor
