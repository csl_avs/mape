# A simple planner
# It can find the succesor of a failure partition
# and plan to replace the failure with its successor
import xmlparse
import nodes
# import pylxd
import os.path


class SimplePlanner:
    def __init__(self, config):
        self.nodes = nodes.nodes().getNodesList()
        if os.path.isfile(config):
            self.config = config
        else:
            self.config = "nodes.xml"
            
    def _GetNodesStatus(self):
        return self.nodes.GetNodesStatus()

    # find a successor of node 'nodeName'
    def FindSuccessor(self, nodeName):
        parser = xmlparse.xmlparse(self.config)
        list = parser.GetNodeList()

        # find the name of the group which the node belongs to
        keys = list.iterkeys()
        while(True):
            tmp = keys.next()
            if tmp in nodeName:
                group = tmp
                break

        # get all the nodes belonging to the group
        nodes = list.get(group)
        if nodeName == group:
            nextId = 1
        else:
            # if nodeName is already a subnode, then we need another
            # subnode which belongs to the same group
            
            nextId = int(nodes.get(nodeName).get('id')) + 1
            
        iter = nodes.itervalues()
        while(True):
            try:
                node = iter.next()
                # look up all subnodes
                if (node is not None and len(node) > 1):
                    # if a subnode's id == 1
                    if(int(node.get('id')) == nextId):
                        return node
            except StopIteration:
                return None
            
        return None

    # stop a given node and start its successor
    def ReplaceNode(self, nodeName):
        if nodeName not in self.nodes:
            print('Node is not a valid partition.')
            return ~0
        
        successor = self.FindSuccessor(nodeName)
        if successor is None:
            print('No candidate found. System will not replace node:%s'
                  % nodeName)
            return ~0

        # start successor first then pause current node
        # successor's .bashrc is modified to start necessary jobs automatically
        nodes.nodes().startNode(successor.get('name'))
        # TODO: How to launch nodes automatically in a container?
        # Currently we use a service script in /etc/init.d to automatically start nodes
        # in the container, so we only have to launch the node.
        
        # launchPath = successor.get('launch')
        # os.system('ssh ubuntu@'+successor.get('ipv4'))
        # os.system('lxc exec '+successor.get('name') +
        #          ' -- roslaunch ' + launchPath)
        nodes.nodes().pauseNode(nodeName)
