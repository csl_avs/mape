#!/usr/bin/env python
# Pnodes can generate messages and automatically send them
# Usage suggests:
# create a Pnodes instance in your planner node
# use generateAction() to create messages which will be kept by pnodes instance
# pnodes will check if the msg buffer is null
import rospy
from acronis_common.msg import Plan, Action, Payload
# from enum import Enum
from multiprocessing import Lock
from mape_misc.opcode import OPCODE


# class OPCODE(Enum):
#     NULL = 0
#     START = 1
#     STOP = 2
#     FREEZE = 3
#     RESTART = 4
#     EXECUTE = 11


action_seq = 0
action_seq_lock = Lock()


class Pnodes(object):
    # Publisher nodes (Pnodes)
    # Pnodes is suppose to be used by a planner to generate, store, publish
    # plan_msg automatically
    
    def __init__(self, topic, planner):
        # self.pub = publisher
        self.msg = list()
        self.topic = topic
        self.node = planner
        # self.action_seq = 1

        self.pub = rospy.Publisher(self.topic, Plan, queue_size=10)
        self.rate = rospy.Rate(10)
        
    def generateAction(self, opcode, node_name, *args):
        newmsg = Action()
        newmsg.op_header.stamp = rospy.Time().now()

        global action_seq, action_seq_lock
        action_seq_lock.acquire()
        # newmsg.op_header.seq = self.action_seq
        newmsg.op_header.seq = action_seq
        # self.action_seq += 1
        action_seq += 1
        action_seq_lock.release()

        # TODO: check the validity of opcode
        newmsg.opcode = opcode
        newmsg.node_name = node_name
        print(args)
        print(type(args))
        # args must be dict
        if len(args[0][0]) > 0:
            #newmsg.payload = list(args)
            for key, value in args[0][0].iteritems():
                temp = Payload()
                temp.key = key
                temp.value = str(value)
                
                newmsg.payload.append(temp)

        self.msg.append(newmsg)

    def _resetPlan(self):
        del self.msg[0:len(self.msg)]

        # Safely change Pnodes.action_seq while multiple instances exist
        # global action_seq, action_seq_lock
        # action_seq_lock.acquire()
        # action_seq += self.action_seq
        # action_seq_lock.release()
        
        # self.action_seq = 1

    def publishPlan(self):
        # pub = rospy.publish(self.topic, Plan)
        # pub_rate = rospy.Rate(10)

        # while not rospy.is_shutdown():
        if not rospy.is_shutdown():
            if len(self.msg) > 0:
                # integrate all Action msgs into a Plan msg
                plan = Plan()
                plan.decisions = self.msg
                rospy.loginfo(plan)
                # while(True):
                    # if (self.pub.get_num_connections() > 0):
                        # print(self.pub.get_num_connections())
                self.pub.publish(plan)
                print("plan published")
                # self.rate.sleep()
                # break
                self._resetPlan()
            # empty the msg list
            # del self.msg[0:len(self.msg)]

        # pub_rate.sleep()


if __name__ == '__main__':
    # do test
    from rosnodeplanner import ROSNodePlanner
    rospy.init_node('test_pnodes')
    planner = ROSNodePlanner('nodes.xml')
    pub = Pnodes('test_pnodes', planner)
    pub.generateAction(OPCODE.EXECUTE.value, 'foobar', 'foo', 'bar')
    print(pub.msg)
