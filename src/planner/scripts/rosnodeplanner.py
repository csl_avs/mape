# Generate plan action messages to handle ROS nodes (processes)
# from sampleplanner import SamplePlanner
import rosnode
import rospy
import re
from pnodes import Pnodes, OPCODE
from xmlparse import xmlparse


class ROSNodePlanner(object):
    cmd_run = 'rosrun'
    cmd_launch = 'roslaunch'

    def __init__(self, config):
        self.nodes = rosnode.get_node_names()
        self.Pnodes = Pnodes('rosnode_plan', 'rosnodeplanner')
        self.nodes_list = xmlparse(config).GetNodeList()
        self.config = config
        # record latest msg time stamp for comparision
        self.last_stamp_sec = 0

    def action_start_node(self, node_name, pkg_name, exe_name):
        # new_node = self.FindSuccessor(node_name)
        
        return self.Pnodes.generateAction(
            OPCODE.ROSRUN.value, node_name, ROSNodePlanner.cmd_run, pkg_name,
            exe_name)

    def action_launch_node(self, node_name, pkg_name, launch_file):
        return self.Pnodes.generateAction(
            OPCODE.ROSLAUNCH.value, node_name,
            ROSNodePlanner.cmd_launch, pkg_name,
            launch_file)

    def __get_name_re(self, node_name):
        # common anonymous node name pattern is:
        # /NAME_STRING_DIGIT1_DIGIT2
        # e.g. /node_a_12345_67890123

        re1 = '(.).*?'+node_name
        re2 = '(_)(\\d+)(_)(\\d+)'

        name_pattern = re.compile(re1+re2)
        return name_pattern
        
    def action_stop_node(self, node_name):
        name_pattern = self.__get_name_re(node_name)
        match = ''
        for item in self.nodes:
            # assuming there is only one match
            if name_pattern.match(item):
                match = item
                break

        if match is not None and match != '':
            return self.Pnodes.generateAction(OPCODE.STOP.value, match)

    def plan_replace_node(self, node_name):
        # find a successor of node_name
        # start the successor
        # stop node_name
        new_node = self.FindSuccessor(node_name)

        # Error returned if no succesor is found
        if new_node is None:
            rospy.loginfo('No succesor of %s found!'.format(node_name))
            return ~0

        parse_err = ''

        # locate pakcage and either an executable or launch file
        if 'package' in new_node and 'executable' in new_node:
            pkg_name = new_node.get('package')
            exe_name = new_node.get('executable')
            
            # if package is invalid then it will pop up error
            if pkg_name is not None or pkg_name != '':
                self.action_start_node(new_node.get('name'),
                                       pkg_name, exe_name)
            else:
                parse_err += 'Package ERROR: %s\n'.format(new_node)
        elif 'launch' in new_node:
            launch_file = new_node.get('launch')
            pkg_name = new_node.get('package')

            # if launch file is invalid then it will pop up error
            if launch_file is not None or launch_file != '':
                self.action_launch_node(new_node.get('name'),
                                        pkg_name, launch_file)
            else:
                parse_err += 'launch file ERROR: %s\n'.format(new_node)
        else:
            parse_err += 'No start method for node: %s\n'.format(new_node)

        self.action_stop_node(node_name)

        if parse_err != '':
            rospy.logerr(parse_err)
            return ~0
        else:
            return self.Pnodes.publishPlan()

    def __plan(self, msg):
        # can be called by start()
        # keep parsing msgs from Analysis and generate Plans
        for node in msg.failures:
            # simply kill failure node
            self.Pnodes.generateAction(OPCODE.STOP.value, node, None)

        for node in msg.patients:
            # find a patient first
            succesor = self.FindSuccessor(node)
            if 'executable' in succesor and \
               succesor.get('executable') is not None:
                self.action_start_node(succesor.get('name'),
                                       succesor.get('package'),
                                       succesor.get('executable'))
            elif 'launch' in succesor and \
                 succesor.get('launch') is not None:
                self.action_launch_node(succesor.get('name'),
                                        succesor.get('package'),
                                        succesor.get('launch'))
            # then kill the patient
            self.action_stop_node(node)

    def __test(self):
        return self.plan_replace_node('nodea')

    def __is_new(self, msg):
        # check if msg is new
        # require a std_msgs.Header in msg
        if msg.msg_header:
            # compare time stamp (stamp.sec) to check if it's new
            rospy.loginfo("sec is {0}".
                          format(msg.msg_header.stamp))
            if msg.msg_header.stamp > self.last_stamp_sec:
                self.last_stamp = msg.msg_header.stamp
                return True
        return False

    def start(self, msg, keep_alive=False):
        # return self.plan_replace_node('nodea')
        if keep_alive:
            while(True):
                if self.__is_new(msg):
                    self.__plan(msg)
        else:
            self.__test()
