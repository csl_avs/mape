# Used to parse XML-based configurations
from os.path import isfile
from xml.etree import ElementTree
import os


class xmlparse:
    __srvPath = './service.xml'
    __nodePath = './nodes.xml'
    
    def __init__(self, path):
        # self.tree = ''
        print('[debug] config file path is %s' % path)
        print('[debug] current path is %s' % os.getcwd())
        if(isfile(path)):
            self.path = path
        else:
            self.path = xmlparse.__srvPath
            
    def _openXml(self):
        # assert(isfile(path))

        self.tree = ElementTree.parse(self.path)
        if (self.tree is not None):
            self.root = self.tree.getroot()
        else:
            return None

        return self.root

    def getServiceList(self):
        root = self._openXml()
        
        if (root is None):
            return None

        # Service list is a dictionaty containing all services
        serviceList = dict()
        
        for service in root:
            # Service is a dictionaty holding all nodes (subservices)
            if (service is None):
                continue

            # newService = dict()
            newService = dict([('name', service.attrib['name'])])

            for node in service:
                # Parse each node into a dict
                if (node is not None):
                    # get member of node into a dict
                    newNode = dict([('name', node.attrib['funct'])])
                    # newNode['name'] = node.attrib['funct']
                    for item in node:
                        if(item.tag == 'args'):
                            newNode[item.text] = item.attrib['type']
                        else:
                            newNode[item.tag] = item.text
                    
                # put the new node into the list
                # (of the service it belongs to)
                newService[newNode['name']] = newNode

            # put the new constructed service into service list
            serviceList[service.attrib['name']] = newService
            # or serviceList.update({service.attrib['name']: newService})

        return serviceList

    def GetNodeList(self):
        root = self._openXml()

        # get <nodelist>
        if (root is None):
            return None

        # Service list is a dictionaty containing all services
        nodeList = dict([('master', root.attrib['master'])])
        nodeConfig = dict([('master', root.attrib['master'])]) 

        # get <list>
        for nodes in root:
            # Service is a dictionaty holding all nodes (subservices)
            if (nodes is None):
                continue

            # newService = dict()
            newNode = dict([('name', nodes.attrib['name'])])
            newNodeConfig = dict([('name', nodes.attrib['name'])])
            newNodeConfig['id'] = newNode['id'] = nodes.attrib['id']
            
            newNodeConfig['group'] = newNode['group'] = newNode['name']
            
            if 'class' in nodes.attrib:
                newNode['class'] = int(nodes.attrib['class'])
            else:
                newNode['class'] = 0

            newNodeConfig['class'] = newNode['class']
            
            nodeConfig[newNode['name']] = newNodeConfig
            print(nodeConfig[newNode['name']])
            # get <node>
            for node in nodes:
                # Parse each node into a dict
                if (node is not None):
                    # get member of node into a dict
                    tmpNode = dict([('name', node.attrib['name'])])
                    tmpNode['id'] = node.attrib['id']
                    # newNode['name'] = node.attrib['funct']
                    # children nodes inherit parent's class
                    tmpNode['class'] = newNode['class']
                    tmpNode['group'] = newNode['group']
                    for item in node:
                        if(item.tag == 'args'):
                            tmpNode[item.text] = item.attrib['type']
                        else:
                            tmpNode[item.tag] = item.text

                    nodeConfig[tmpNode['name']] = tmpNode
                    # print(nodeConfig[tmpNode['name']])
                    
                # put the new node into the list
                # (of the service it belongs to)
                newNode[tmpNode['name']] = tmpNode
                # print(nodeConfig[newNode['name']])

            # put the new constructed service into service list
            nodeList[nodes.attrib['name']] = newNode
            # or serviceList.update({service.attrib['name']: newService})

        return nodeList, nodeConfig


# TEST: parse nodes.xml
def __printNestedDict(d):
    for key, value in d.iteritems():
        if type(value) is dict:
            __printNestedDict(value)
        else:
            print("{0} - {1}".format(key, value))

            
if __name__ == '__main__':
    tree = xmlparse('../nodes.xml')
    list, config = tree.GetNodeList()
    # print(list)
    print(config)
    print("*******")
    __printNestedDict(config)
    print(config)
    print(config['test1']['class'])
    
