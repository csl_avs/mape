#!/usr/bin/env python
#import simpleplanner


#lanner = simpleplanner.SimplePlanner()
#node = planner.FindSuccessor('nodes.xml', 'serviceA_1')
#print(node)

#from multiprocessing import Lock, Process
from threading import Lock, Thread
var = 0
var_lock = Lock()


class TestCase(object):
    var = 0
    var_lock = Lock()

    def update(self, value):
        global var_lock
        var_lock.acquire()
        global var
        var = var + value
        print("process %d: var %d" % (value, var))
        var_lock.release()


class Foo(object):
    def __init__(self):
        self.tag = 'foo'

    def getTag(self):
        print(self.tag)


class Bar(Foo):
    def __init__(self):
        self.tag = 'bar'

    def printTag(self):
        return super(Foo, self).getTag()


def addvar(value):
    global var
    var_lock.acquire()
    print("before: process %d, var %d" % (value, var))
    var += value
    print("process %d, var %d" % (value, var))
    var_lock.release()

    
if __name__ == '__main__':
    newCase = TestCase()
    newCase.var = 5
    bar = Bar()
    print(bar.getTag())
    #print newCase.var
    #newCase2 = TestCase()
    #print newCase2.var
    for i in range(1, 11):
        t = Thread(target=newCase.update, args=(i,))
        t.start()
        print("loop %d" % i)
