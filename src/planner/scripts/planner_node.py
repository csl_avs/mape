#!/usr/bin/env python
from sampleplanner import SamplePlanner
# from rosnodeplanner import ROSNodePlanner
# from mape_msg.msg import NodeConfig
# from mape_msg.msg import BadNodes
from acronis_common.msg import BadNodes
import rospy
import rospkg
# from multiprocessing import Process

node_tree = dict()
node_config = None
partition_planner = None


# Call start() to execute planning
def start_planner(planner):
    if planner is None:
        return ~0

    planner.start(None, keep_alive=False)


def failures_handler(data):
    # process_planner = ROSNodePlanner(node_config)
    global node_config, partition_planner
    # partition_planner = SamplePlanner(node_config)
    if len(data.failures) > 0:
        for partition in data.failures:
            # partition_planner.ReplaceNode(partition)
            # try unfreeze to speed up [ck]
            # partition_planner.ReplaceNodeNew(partition)
            partition_planner.PlanforNode(partition)
        

# def NodeConfigHandler(data):
#     # TODO: define and parse the msg of node list
#     master_uri = data.master_uri
#     global node_tree
#     node_tree = dict([('master', master_uri)])
    
#     node_list = data.node_list
#     for list in node_list:
#         newlist = dict([('name', list.list_name)])
#         newlist['id'] = list.id
#         newlist['class'] = list.class

#         for node in list.subnodes:
#             newNode = dict([('name', node.node_name)])
#             newNode['id'] = node.node_id
#             newNode['launch'] = node.launch_path
#             newNode['ipv4'] = node.ipv4_addr
#             newNode['user'] = node.user_name
#             newNode['class'] = list.class
            
#             newlist[newNode['name']] = newNode

#         node_tree[newlist['name']] = newlist

#     return node_tree

    
def init_node():
    # start planner node
    rospy.init_node('MAPE_p_ROS_node', anonymous=True)
    
    # TODO: subscribe and parse node_list from executor
    # rospy.Subscriber("TP_nodes_list", NodeConfig, NodeConfigHandler)

    global node_config
    if rospy.has_param('node_config'):
        # search param server for node_config
        node_config = rospy.get_param('node_config')
        # ros_plan = ROSNodePlanner('nodes.xml')
    else:
        # nodes.xml should be in $(PLANNER_PATH)/nodes.xml
        pkg_handler = rospkg.RosPack()
        node_config = pkg_handler.get_path('planner')+'/nodes.xml'
        # rospy.loginfo("[debug] config is %s" % node_config)
        rospy.set_param('node_config', node_config)

    rospy.loginfo("node config in %s" % node_config)
    # multiple planners can be started
    # consider using multiprocessing/multithreading
    # ros_plan = ROSNodePlanner(node_config)
    
    # start_planner(ros_plan)
    global partition_planner
    partition_planner = SamplePlanner(node_config)
    rospy.Subscriber("partition_failures", BadNodes, failures_handler)
    # spin to keep alive
    rospy.spin()


if __name__ == '__main__':
    init_node()
