import xmlparse
import nodes
from pnodes import Pnodes, OPCODE
from rosnodeplanner import ROSNodePlanner
import os.path


class SamplePlanner(object):
    # provide a base class for all planners
    def __init__(self, config):
        self.nodes = nodes.nodes().getNodesList()
        self.Pnodes = Pnodes('sample_plan', 'sampleplanner')
        # self.Pnodes.publishPlan()

        if os.path.isfile(config):
            self.config = config
        else:
            self.config = "nodes.xml"

        parser = xmlparse.xmlparse(self.config)
        self.configList, self.configListall = parser.GetNodeList()
        
        self.process_planner = ROSNodePlanner(config)

    def _GetNodesStatus(self):
        return self.nodes.GetNodesStatus()

    def _GetNodeConfig(self, nodeName):
        for key in self.configListall.keys():
            if key == nodeName:
                return self.configListall[key]
        return None

    def PlanforNode(self, nodeName):
        # if  nodeName not in self.nodes:
           #  print("%s is not found in partitions." % nodeNam)
           #  return ~0
        
        nodeConfig = self._GetNodeConfig(nodeName)
        if nodeConfig['class'] == 2:
            # in a container
            self.ReplaceNode(nodeName, nodeConfig)
        elif nodeConfig['class'] == 1:
            self.rebootNode(nodeName, nodeConfig)
            
    # find a successor of node 'nodeName'
    def FindSuccessor(self, nodeName):
        # parser = xmlparse.xmlparse(self.config)
        # list = parser.GetNodeList()

        # find the name of the group which the node belongs to
        keys = self.configList.iterkeys()
        if keys is not None:
            i = 0
            while(i < len(self.configList)):
                tmp = keys.next()
                if tmp in nodeName:
                    group = tmp
                    break
                i = i + 1
            if i == len(self.configList):
                # cannot find nodeName in the list
                return None
        else:
            return None

        # get all the nodes belonging to the group
        nodes = self.configList.get(group)
        if nodeName == group:
            nextId = 1
        else:
            # if nodeName is already a subnode, then we need another
            # subnode which belongs to the same group
            
            nextId = int(nodes.get(nodeName).get('id')) + 1
            
        iter = nodes.itervalues()
        while(True):
            try:
                node = iter.next()
                print('[debug]%s' % node)
                # look up all subnodes
                # if (node is not None and len(node) > 1):
                if (type(node) is dict):
                    # if a subnode's id == 1
                    if(int(node.get('id')) == nextId):
                        return node
            except StopIteration:
                return None
            
        return None

    def FindSuccessorNew(self, nodeName):
        old = self._GetNodeConfig(nodeName)
        id = int(old['id'])+1
        
        for key, item in self.configListall.iteritems():
            if type(item) != dict:
                continue
            if item['group'] == nodeName and int(item['id']) == id:
                return item
        
        return None
    
    # stop a given node and start its successor
    def ReplaceNode(self, nodeName, *args):
        if nodeName not in self.nodes:
            print('Node is not a valid partition.')
            return ~0
        
        successor = self.FindSuccessorNew(nodeName)
        if successor is None:
            print('No candidate found. System will not replace node:%s'
                  % nodeName)
            return ~0

        # start successor first then pause current node
        # successor's .bashrc is modified to start necessary jobs automatically
        # nodes.nodes().startNode(successor.get('name'))
        self.Pnodes.generateAction(
            OPCODE.START.value, successor['name'], args)
        self.Pnodes.publishPlan()
        # use launch file to start the backup service ros node
        # published by ROSNodePlanner via another topic
        # self.process_planner.action_launch_node(
        #     successor.get('name'), '', successor.get('launch'))
        # self.process_planner.Pnodes.publishPlan()
        # TODO: How to launch nodes automatically in a container?
        # Currently we use a service script in /etc/init.d to
        # automatically start nodes in the container,
        # so we only have to launch the node.
        
        # launchPath = successor.get('launch')
        # os.system('ssh ubuntu@'+successor.get('ipv4'))
        # os.system('lxc exec '+successor.get('name') +
        #          ' -- roslaunch ' + launchPath)
        # nodes.nodes().pauseNode(nodeName)
        self.Pnodes.generateAction(OPCODE.STOP.value, nodeName, args)

        self.Pnodes.publishPlan()

    def ReplaceNodeNew(self, nodeName, *args):
        if nodeName not in self.nodes:
            print('Node is not a valid partition.')
            return ~0
        
        successor = self.FindSuccessor(nodeName)
        if successor is None:
            print('No candidate found. System will not replace node:%s'
                  % nodeName)
            return ~0

        # start successor first, then pause current node
        # successor's .bashrc is modified to start necessary jobs automatically
        # nodes.nodes().startNode(successor.get('name'))
        self.Pnodes.generateAction(
            OPCODE.RESTART.value, successor.get('name'), args)
        # self.Pnodes.publishPlan()

        self.Pnodes.generateAction(
            OPCODE.EXECUTE.value, successor.get('name'), '',
            successor.get('launch'))
        
        self.Pnodes.generateAction(OPCODE.STOP.value, nodeName, args)

        self.Pnodes.publishPlan()

    # reboot partititon 'nodeName'
    def rebootNode(self, nodeName, *args):
        nodeConfig = self.configListall.get(nodeName)
        if nodeConfig['class'] ==2 and nodeName not in self.nodes:
            print('%s is not a valid partition.' % nodeName)
            return ~0
        self.Pnodes.generateAction(OPCODE.RESTART.value, nodeName, args)
        self.Pnodes.publishPlan()

    def __start(self):
        # invoke this private function to test basic features
        self.ReplaceNode('u1')

    def __plan(self, msg, *args):
        # can be called by start()
        # keep parsing msgs from Analysis and generate Plans
        for node in msg.failures:
            self.Pnodes.generateAction(OPCODE.STOP.value, node, None)

        for node in msg.patients:
            succesor = self.FindSuccessor(node)
            if succesor.get('launch') is not None:
                self.Pnodes.generateAction(OPCODE.START.value, succesor.get('name'), succesor.get('package'),succesor.get())

    def start(self):
        self.__start()
