#!/usr/bin/env python
#import pylxd
import nodes
from time import time
import datetime

if __name__ =='__main__':
	nodes = nodes.nodes()
	elapsed = 0.0
	end = 0.0
	for i in range(100):
		start = datetime.datetime.now()
		nodes.startNode('bench5')
		while True:
			if 'Running' in nodes.getNodeStatus('bench5'):
				end = datetime.datetime.now()
				break
			break

		elapsed = elapsed + (end - start).microseconds

		nodes.stopNode('bench5')
		#print('test %s completed' %i)
    
	print(elapsed/100)
