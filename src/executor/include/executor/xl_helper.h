extern "C" {
void init_xlhelper(void);
void xl_partition_create(const char *name);
void xl_partition_shutdown(const char *name);
void xl_partition_restart(const char *name);
void xl_partition_pause(const char *name);
void xl_partition_resume(const char *name);
}
