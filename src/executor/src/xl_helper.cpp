#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

// no need to add extern "c" as it is done in xl_helper.h
//#ifdef __cplusplus
//extern "C" {
//#endif

//#include "xl_helper.h"    
//#include "xl.h"
#include <executor/xl_helper.h>
#include <executor/xl.h>


void init_xlhelper(void)
{
    if(initial_libxl() == EXIT_FAILURE)
    {
        printf("Failed to initailize xl_helper.");
        exit(1);
    }
}

void xl_partition_create(const char *name)
{
    /* name is config file path */
    main_create(name);
}

void xl_partition_shutdown(const char *name)
{
    uint32_t domid;
    if (main_domid(name, &domid) == EXIT_SUCCESS){
        main_shutdown(domid);    
    }
}

void xl_partition_restart(const char *name)
{
    uint32_t domid;
    if (main_domid(name, &domid) == EXIT_SUCCESS){
        main_reboot(domid);
    }
}

void xl_partition_pause(const char *name)
{
    uint32_t domid;
    if (main_domid(name, &domid) == EXIT_SUCCESS){
        main_pause(domid);
    }
}

void xl_partition_resume(const char *name)
{
    uint32_t domid;
    if (main_domid(name, &domid) == EXIT_SUCCESS){
        main_unpause(domid);
    }
}


//#ifdef __cplusplus
//}
//#endif
