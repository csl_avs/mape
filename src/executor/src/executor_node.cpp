#include <iostream>
#include <map>
#include <string>
#include <sys/stat.h>
#include <ros/ros.h>

#include <executor/xl_helper.h>
#include <acronis_common/Plan.h>
#include <acronis_common/Action.h>
#include <acronis_common/NodeList.h>
#include <mape_misc/opcode.h>

/* store partition configurations */
std::map<std::string, std::string> partition_lists;

static void process_decision(const acronis_common::Action &action)
{
    if(partition_lists.find(action.node_name) == partition_lists.end())
    {
        exit(1);
    }
    
    switch(action.opcode)
    {
        case START:       
            xl_partition_create(partition_lists[action.node_name].c_str());
            break;
        case STOP:
            xl_partition_shutdown(action.node_name.c_str());
            break;
        case RESTART:
            xl_partition_restart(action.node_name.c_str());
            break;
        case FREEZE:
            xl_partition_pause(action.node_name.c_str());
            break;
        case RESUME:
            xl_partition_resume(action.node_name.c_str());
            break;
        default:
            ROS_INFO("Unsupported Opcode\n");            
    }
}

/* parse plan msg from planner */
static void parse_msg(const acronis_common::PlanConstPtr &msg)
{
    int i;
    for(i = 0; i < msg->decisions.size(); i++)
    {
        process_decision(msg->decisions[i]);
    }
}

/* check if a file(path) exists */
static bool file_exists(const char *file)
{
    struct stat buf;
    if(stat(file, &buf) != -1)
    {
        return true;
    }

    return false;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "executor_node");
    ros::NodeHandle nh;
	ros::Subscriber nh_sub1;

    bool error_b = false;

    /* Each executor must know where the partition configuration is.
     * Those info must be provided as executor configuration as
     * <partition_name, configuration_file> pairs
     * 'configuration_file' must be a Xen hvm config
     * 'partition_name' must match the 'name' in hvm config
     * EXAMPLE: rosparam set /partitions "{sing_map: '/home/acronis/mape_deploy/sing_map/sing_map.cfg'}"
     */
    
    if(nh.hasParam("partitions"))
    {
        nh.getParam("partitions", partition_lists);
        for(auto iter = partition_lists.begin(); iter != partition_lists.end(); iter++)
        {
            /* validate the partition configuration */
            if(!file_exists(iter->second.c_str()))
            {
                std::cout << "Partition " << iter->first << " error:";
                std::cout << " Configuration doesn't exists." << std::endl;
                error_b = true;
            }
        }
        if(error_b)
        {
            goto error_exit;
        }
    }
    else
    {
        ROS_INFO("No partition configuration provided!");
        goto error_exit;
    }

    /* MUST: initialize xl_helper support */
    init_xlhelper();

    /* TODO: publish and subscribe */
	nh_sub1 = nh.subscribe("sample_plan", 1, parse_msg);

    ros::spin();
    return 0;

error_exit:
    std::cout << "There are unresolved errors, exit." << std::endl;
    exit(1);
}
