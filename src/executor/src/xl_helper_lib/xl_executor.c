//#ifdef __cplusplus
//extern "C" {
//#endif
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <inttypes.h>
#include <regex.h>

#include <libxl.h>
#include <libxl_utils.h>
#include <libxlutil.h>

#include "xl.h"
#include "xl_parse.h"
#include "xl_utils.h"

#ifdef __cplusplus
extern "C" {
#endif
	
int autoballoon = -1;
int run_hotplug_scripts = -1;
int dryrun_only;
int claim_mode = 1;
char *lockfile;
char *blkdev_start;
int max_grant_frames = -1;
int max_maptrack_frames = -1;
const char *common_domname;
int logfile = 2;
bool progress_use_cr = 0;
xentoollog_logger_stdiostream *logger;
xentoollog_level minmsglevel = minmsglevel_default;
enum output_format default_output_format = OUTPUT_FORMAT_JSON;
char *default_remus_netbufscript = NULL;
char *default_colo_proxy_script = NULL;

/* every action in xl use this libxl context */
libxl_ctx *ctx;
xentoollog_logger_stdiostream *logger;
xlchild children[child_max];
static int fd_lock = -1;

char *default_vifscript = NULL;
char *default_bridge = NULL;
char *default_gatewaydev = NULL;
char *default_vifbackend = NULL;

static int xl_reaped_callback(pid_t got, int status, void *user)
{
    int i;
    assert(got);
    for (i = 0; i < child_max; i++)
    {
        xlchild *ch = &children[i];
        if (ch->pid == got)
        {
            ch->reaped = 1;
            ch->status = status;
        }
    }
    return ERROR_UNKNOWN_CHILD;
}

static const libxl_childproc_hooks childproc_hooks = {
    .chldowner = libxl_sigchld_owner_libxl,
    .reaped_callback = xl_reaped_callback,
};

static void xl_ctx_free(void)
{
	if(ctx){
		libxl_ctx_free(ctx);
		ctx = NULL;
	}
	if(logger){
		xtl_logger_destroy((xentoollog_logger*)logger);
		logger = NULL;
	}
	if(lockfile){
		free(lockfile);
		lockfile = NULL;
	}
}

static int auto_autoballoon(void)
{
    const libxl_version_info *info;
    regex_t regex;
    int ret;

    info = libxl_get_version_info(ctx);
    if (!info)
        return 1; /* default to on */

    ret = regcomp(&regex,
                  "(^| )dom0_mem=((|min:|max:)[0-9]+[bBkKmMgG]?,?)+($| )", 
				  REG_NOSUB | REG_EXTENDED);
    if (ret)
        return 1;

    ret = regexec(&regex, info->commandline, 0, NULL, 0);
    regfree(&regex);
    return ret == REG_NOMATCH;
}

static void parse_global_config(const char *configfile,
                              const char *configfile_data,
                              int configfile_len)
{
    long l;
    XLU_Config *config;
    int e;
    const char *buf;
    libxl_physinfo physinfo;

    config = xlu_cfg_init(stderr, configfile);
    if (!config) {
        fprintf(stderr, "Failed to allocate for configuration\n");
        exit(1);
    }

    e = xlu_cfg_readdata(config, configfile_data, configfile_len);
    if (e) {
        fprintf(stderr, "Failed to parse config file: %s\n", strerror(e));
        exit(1);
    }

    if (!xlu_cfg_get_string(config, "autoballoon", &buf, 0)) {
        if (!strcmp(buf, "on") || !strcmp(buf, "1"))
            autoballoon = 1;
        else if (!strcmp(buf, "off") || !strcmp(buf, "0"))
            autoballoon = 0;
        else if (!strcmp(buf, "auto"))
            autoballoon = -1;
        else
            fprintf(stderr, "invalid autoballoon option");
    }
    if (autoballoon == -1)
        autoballoon = auto_autoballoon();

    if (!xlu_cfg_get_long (config, "run_hotplug_scripts", &l, 0))
        run_hotplug_scripts = l;

    if (!xlu_cfg_get_string (config, "lockfile", &buf, 0))
        lockfile = strdup(buf);
    else {
        lockfile = strdup(XL_LOCK_FILE);
    }

    if (!lockfile) {
        fprintf(stderr, "failed to allocate lockfile\n");
        exit(1);
    }

    /*
     * For global options that are related to a specific type of device
     * we use the following nomenclature:
     *
     * <device type>.default.<option name>
     *
     * This allows us to keep the default options classified for the
     * different device kinds.
     */

    if (!xlu_cfg_get_string (config, "vifscript", &buf, 0)) {
        fprintf(stderr, "the global config option vifscript is deprecated, "
                        "please switch to vif.default.script\n");
        free(default_vifscript);
        default_vifscript = strdup(buf);
    }

    if (!xlu_cfg_get_string (config, "vif.default.script", &buf, 0)) {
        free(default_vifscript);
        default_vifscript = strdup(buf);
    }

    if (!xlu_cfg_get_string (config, "defaultbridge", &buf, 0)) {
        fprintf(stderr, "the global config option defaultbridge is deprecated, "
                        "please switch to vif.default.bridge\n");
        free(default_bridge);
        default_bridge = strdup(buf);
    }

    if (!xlu_cfg_get_string (config, "vif.default.bridge", &buf, 0)) {
        free(default_bridge);
        default_bridge = strdup(buf);
    }

    if (!xlu_cfg_get_string (config, "vif.default.gatewaydev", &buf, 0))
        default_gatewaydev = strdup(buf);

    if (!xlu_cfg_get_string (config, "vif.default.backend", &buf, 0))
        default_vifbackend = strdup(buf);

    if (!xlu_cfg_get_string (config, "output_format", &buf, 0)) {
        if (!strcmp(buf, "json"))
            default_output_format = OUTPUT_FORMAT_JSON;
        else if (!strcmp(buf, "sxp"))
            default_output_format = OUTPUT_FORMAT_SXP;
        else {
            fprintf(stderr, "invalid default output format \"%s\"\n", buf);
        }
    }
    if (!xlu_cfg_get_string (config, "blkdev_start", &buf, 0))
        blkdev_start = strdup(buf);

    if (!xlu_cfg_get_long (config, "claim_mode", &l, 0))
        claim_mode = l;

    xlu_cfg_replace_string (config, "remus.default.netbufscript",
        &default_remus_netbufscript, 0);
    xlu_cfg_replace_string (config, "colo.default.proxyscript",
        &default_colo_proxy_script, 0);

    if (!xlu_cfg_get_long (config, "max_grant_frames", &l, 0))
        max_grant_frames = l;
    else {
        libxl_physinfo_init(&physinfo);
        max_grant_frames = (libxl_get_physinfo(ctx, &physinfo) != 0 ||
                            !(physinfo.max_possible_mfn >> 32))
                           ? 32 : 64;
        libxl_physinfo_dispose(&physinfo);
    }
    if (!xlu_cfg_get_long (config, "max_maptrack_frames", &l, 0))
        max_maptrack_frames = l;

    xlu_cfg_destroy(config);
}

int xl_child_pid(xlchildnum child)
{
    xlchild *ch = &children[child];
    return ch->pid;
}

void xl_ctx_alloc(void)
{
    if(libxl_ctx_alloc(&ctx, LIBXL_VERSION, 0, (xentoollog_logger *)logger))
    {
        fprintf(stderr, "cannot initialize xl context\n");
        exit(1);
    }
    libxl_childproc_setmode(ctx, &childproc_hooks, 0);
}

static int acquire_lock(void)
{
    int rc;
    struct flock fl;

    /* lock already acquired */
    if (fd_lock >= 0)
        return ERROR_INVAL;

    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;
    fd_lock = open(lockfile, O_WRONLY|O_CREAT, S_IWUSR);
    if (fd_lock < 0) {
        fprintf(stderr, "cannot open the lockfile %s errno=%d\n", lockfile, errno);
        return ERROR_FAIL;
    }
    if (fcntl(fd_lock, F_SETFD, FD_CLOEXEC) < 0) {
        close(fd_lock);
        fprintf(stderr, "cannot set cloexec to lockfile %s errno=%d\n", lockfile, errno);
        return ERROR_FAIL;
    }
get_lock:
    rc = fcntl(fd_lock, F_SETLKW, &fl);
    if (rc < 0 && errno == EINTR)
        goto get_lock;
    if (rc < 0) {
        fprintf(stderr, "cannot acquire lock %s errno=%d\n", lockfile, errno);
        rc = ERROR_FAIL;
    } else
        rc = 0;
    return rc;
}

static int release_lock(void)
{
    int rc;
    struct flock fl;

    /* lock not acquired */
    if (fd_lock < 0)
        return ERROR_INVAL;

release_lock:
    fl.l_type = F_UNLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;

    rc = fcntl(fd_lock, F_SETLKW, &fl);
    if (rc < 0 && errno == EINTR)
        goto release_lock;
    if (rc < 0) {
        fprintf(stderr, "cannot release lock %s, errno=%d\n", lockfile, errno);
        rc = ERROR_FAIL;
    } else
        rc = 0;
    close(fd_lock);
    fd_lock = -1;

    return rc;
}

/*
 * Returns false if memory can't be freed, but also if we encounter errors.
 * Returns true in case there is already, or we manage to free it, enough
 * memory, but also if autoballoon is false.
 */
static bool freemem(uint32_t domid, libxl_domain_build_info *b_info)
{
    int rc, retries = 3;
    uint64_t need_memkb, free_memkb;

    if (!autoballoon)
        return true;

    rc = libxl_domain_need_memory(ctx, b_info, &need_memkb);
    if (rc < 0)
        return false;

    do {
        rc = libxl_get_free_memory(ctx, &free_memkb);
        if (rc < 0)
            return false;

        if (free_memkb >= need_memkb)
            return true;

        rc = libxl_set_memory_target(ctx, 0, free_memkb - need_memkb, 1, 0);
        if (rc < 0)
            return false;

        /* wait until dom0 reaches its target, as long as we are making
         * progress */
        rc = libxl_wait_for_memory_target(ctx, 0, 10);
        if (rc < 0)
            return false;

        retries--;
    } while (retries > 0);

    return false;
}

static void reload_domain_config(uint32_t domid,
                                 libxl_domain_config *d_config)
{
    int rc;
    uint8_t *t_data;
    int ret, t_len;
    libxl_domain_config d_config_new;

    /* In case user has used "config-update" to store a new config
     * file.
     */
    ret = libxl_userdata_retrieve(ctx, domid, "xl", &t_data, &t_len);
    if (ret && errno != ENOENT) {
        LOG("\"xl\" configuration found but failed to load\n");
    }
    if (t_len > 0) {
        LOG("\"xl\" configuration found, using it\n");
        libxl_domain_config_dispose(d_config);
        libxl_domain_config_init(d_config);
        parse_config_data("<updated>", (const char *)t_data,
                          t_len, d_config);
        free(t_data);
        libxl_userdata_unlink(ctx, domid, "xl");
        return;
    }

    libxl_domain_config_init(&d_config_new);
    rc = libxl_retrieve_domain_configuration(ctx, domid, &d_config_new);
    if (rc) {
        LOG("failed to retrieve guest configuration (rc=%d). "
            "reusing old configuration", rc);
        libxl_domain_config_dispose(&d_config_new);
    } else {
        libxl_domain_config_dispose(d_config);
        /* Steal allocations */
        memcpy(d_config, &d_config_new, sizeof(libxl_domain_config));
    }
}

static int domain_wait_event(uint32_t domid, libxl_event **event_r)
{
    int ret;
    for (;;) {
        ret = libxl_event_wait(ctx, event_r, LIBXL_EVENTMASK_ALL, 0,0);
        if (ret) {
            LOG("Domain %u, failed to get event, quitting (rc=%d)", domid, ret);
            return ret;
        }
        if ((*event_r)->domid != domid) {
            char *evstr = libxl_event_to_json(ctx, *event_r);
            LOG("INTERNAL PROBLEM - ignoring unexpected event for"
                " domain %d (expected %d): event=%s",
                (*event_r)->domid, domid, evstr);
            free(evstr);
            libxl_event_free(ctx, *event_r);
            continue;
        }
        return ret;
    }
}

static void wait_for_domain_deaths(libxl_evgen_domain_death **deathws, int nr)
{
    int rc, count = 0;
    LOG("Waiting for %d domains", nr);
    while(1 && count < nr) {
        libxl_event *event;
        rc = libxl_event_wait(ctx, &event, LIBXL_EVENTMASK_ALL, 0,0);
        if (rc) {
            LOG("Failed to get event, quitting (rc=%d)", rc);
            exit(EXIT_FAILURE);
        }

        switch (event->type) {
        case LIBXL_EVENT_TYPE_DOMAIN_DEATH:
            LOG("Domain %d has been destroyed", event->domid);
            libxl_evdisable_domain_death(ctx, deathws[event->for_user]);
            count++;
            break;
        case LIBXL_EVENT_TYPE_DOMAIN_SHUTDOWN:
            LOG("Domain %d has been shut down, reason code %d",
                event->domid, event->u.domain_shutdown.shutdown_reason);
            libxl_evdisable_domain_death(ctx, deathws[event->for_user]);
            count++;
            break;
        default:
            LOG("Unexpected event type %d", event->type);
            break;
        }
        libxl_event_free(ctx, event);
    }
}

/* shutdown an HVM domain */
static void shutdown_domain(uint32_t domid)
{
	int rc;
	
	fprintf(stderr, "Shutting down domain %u\n", domid);
	rc = libxl_domain_shutdown(ctx, domid);

	if (rc == ERROR_NOPARAVIRT)
	{
		/* we don't handle pv domains */
		fprintf(stderr, "[ck] mape:PV control interface not available:  external graceful shutdown not possible \n");
	}

	if (rc)
	{
		fprintf(stderr,"shutdown failed (rc=%d)\n",rc);exit(EXIT_FAILURE);
	}
}

/* reboot an HVM domain */
static void reboot_domain(uint32_t domid)
{
	int rc;

	fprintf(stderr, "Rebooting domain %u\n", domid);
	rc = libxl_domain_reboot(ctx, domid);

	if (rc == ERROR_NOPARAVIRT)
	{
		/* we don't handle pv domains */
		fprintf(stderr, "[ck] mape: PV control interface not available:  external graceful reboot not possible \n");
	}

	if (rc)
	{
		fprintf(stderr,"reboot failed (rc=%d)\n",rc);exit(EXIT_FAILURE);
	}
}

static int valid_dom_id(uint32_t domid)
{
	/* TODO: validate domid first */
	libxl_dominfo *dominfo;
	int nr_dom = 0, i = 0;

	/* halting/rebooting Domain-0 is not allowed */
	if (domid == 0)
	{
		return EXIT_FAILURE;
	}

	/* TODO: we should store all domain info */
	/* so we don't have to check it each time */	 
	dominfo = libxl_list_domain(ctx, &nr_dom);
	if (!dominfo){
		fprintf(stderr, "[ck] libxl_list_domain failed to retrive all domains.\n");
		return EXIT_FAILURE;
	}

	/* iterate all domains */
	for (i = 0; i < nr_dom; i++)
	{
		/* domid found */
		if (dominfo[i].domid == domid){
			return EXIT_SUCCESS;
		}
	}

	return EXIT_FAILURE;
}

	
/* handle shutdown/reboot of a domain */
static int main_shutdown_or_reboot(int option, uint32_t domid)
{
	if (valid_dom_id(domid) == EXIT_FAILURE)
	{
		/* domid is not valid */
		return EXIT_FAILURE;
	}
	/* option == 1: reboot, option == 0: shutdown */
	/* do not wait for domains to shutdown/reboot */
	if (option){
		reboot_domain(domid);
	}else{
		shutdown_domain(domid);
	}

	return EXIT_SUCCESS;
}

int main_reboot(uint32_t domid)
{
	return main_shutdown_or_reboot(1, domid);
}

int main_shutdown(uint32_t domid)
{
	return main_shutdown_or_reboot(0, domid);
}

int main_destroy(uint32_t domid)
{
	int rc;
	
	if (valid_dom_id(domid) == EXIT_FAILURE)
	{
		/* domid is not valid */
		return EXIT_FAILURE;
	}

	rc = libxl_domain_destroy(ctx, domid, 0);
	if (rc)
	{
		fprintf(stderr, "[ck] destroy dom %u failed (rc=%d)\n", domid, rc);
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}

int main_domid(const char *domname, uint32_t *domid)
{
	if (libxl_name_to_domid(ctx, domname, domid))
	{
		fprintf(stderr, "[ck] Cannot find domid of name %s\n", domname);
		return EXIT_FAILURE;
	}

	printf("[ck] domid is %u\n", *domid);

	return EXIT_SUCCESS;
}

int main_domname(const uint32_t domid, char *domname)
{
	domname = libxl_domid_to_name(ctx, domid);
	if (!domname)
	{
		fprintf(stderr, "[ck] Can't get domain name of domain id '%u'.\n", domid);
		return EXIT_FAILURE;
	}

	printf("[ck] domname is %s\n", domname);
	return EXIT_SUCCESS;
}

/* Can update r_domid if domain is destroyed */
static domain_restart_type handle_domain_death(uint32_t *r_domid,
                                               libxl_event *event,
                                               libxl_domain_config *d_config)
{
    domain_restart_type restart = DOMAIN_RESTART_NONE;
    libxl_action_on_shutdown action;

    switch (event->u.domain_shutdown.shutdown_reason) {
    	case LIBXL_SHUTDOWN_REASON_POWEROFF:
        	action = d_config->on_poweroff;
        	break;
    	case LIBXL_SHUTDOWN_REASON_REBOOT:
        	action = d_config->on_reboot;
        	break;
    	case LIBXL_SHUTDOWN_REASON_SUSPEND:
        	LOG("Domain has suspended.");
        	return 0;
    	case LIBXL_SHUTDOWN_REASON_CRASH:
        	action = d_config->on_crash;
        	break;
    	case LIBXL_SHUTDOWN_REASON_WATCHDOG:
        	action = d_config->on_watchdog;
        	break;
    	case LIBXL_SHUTDOWN_REASON_SOFT_RESET:
       		action = d_config->on_soft_reset;
        	break;
    	default:
        	LOG("Unknown shutdown reason code %d. Destroying domain.",
            	event->u.domain_shutdown.shutdown_reason);
        	action = LIBXL_ACTION_ON_SHUTDOWN_DESTROY;
    }

    LOG("Action for shutdown reason code %d is %s",
        event->u.domain_shutdown.shutdown_reason,
        get_action_on_shutdown_name(action));

    if (action == LIBXL_ACTION_ON_SHUTDOWN_COREDUMP_DESTROY || action == LIBXL_ACTION_ON_SHUTDOWN_COREDUMP_RESTART) {
        char *corefile;
        int rc;

        xasprintf(&corefile, XEN_DUMP_DIR "/%s", d_config->c_info.name);
        LOG("dumping core to %s", corefile);
        rc = libxl_domain_core_dump(ctx, *r_domid, corefile, NULL);
        if (rc) LOG("core dump failed (rc=%d).", rc);
        free(corefile);
        /* No point crying over spilled milk, continue on failure. */

        if (action == LIBXL_ACTION_ON_SHUTDOWN_COREDUMP_DESTROY)
            action = LIBXL_ACTION_ON_SHUTDOWN_DESTROY;
        else
            action = LIBXL_ACTION_ON_SHUTDOWN_RESTART;
    }

    switch (action) {
    case LIBXL_ACTION_ON_SHUTDOWN_PRESERVE:
        break;

    case LIBXL_ACTION_ON_SHUTDOWN_RESTART_RENAME:
        reload_domain_config(*r_domid, d_config);
        restart = DOMAIN_RESTART_RENAME;
        break;

    case LIBXL_ACTION_ON_SHUTDOWN_RESTART:
        reload_domain_config(*r_domid, d_config);
        restart = DOMAIN_RESTART_NORMAL;
        /* fall-through */
    case LIBXL_ACTION_ON_SHUTDOWN_DESTROY:
        LOG("Domain %d needs to be cleaned up: destroying the domain",
            *r_domid);
        libxl_domain_destroy(ctx, *r_domid, 0);
        *r_domid = INVALID_DOMID;
        break;

    case LIBXL_ACTION_ON_SHUTDOWN_SOFT_RESET:
        reload_domain_config(*r_domid, d_config);
        restart = DOMAIN_RESTART_SOFT_RESET;
        break;

    case LIBXL_ACTION_ON_SHUTDOWN_COREDUMP_DESTROY:
    case LIBXL_ACTION_ON_SHUTDOWN_COREDUMP_RESTART:
        /* Already handled these above. */
        abort();
    }

    return restart;
}

/* Preserve a copy of a domain under a new name. Updates *r_domid */
static int preserve_domain(uint32_t *r_domid, libxl_event *event,
                           libxl_domain_config *d_config)
{
    time_t now;
    struct tm tm;
    char strtime[24];

    libxl_uuid new_uuid;

    int rc;

    now = time(NULL);
    if (now == ((time_t) -1)) {
        LOG("Failed to get current time for domain rename");
        return 0;
    }

    tzset();
    if (gmtime_r(&now, &tm) == NULL) {
        LOG("Failed to convert time to UTC");
        return 0;
    }

    if (!strftime(&strtime[0], sizeof(strtime), "-%Y%m%dT%H%MZ", &tm)) {
        LOG("Failed to format time as a string");
        return 0;
    }

    libxl_uuid_generate(&new_uuid);

    LOG("Preserving domain %u %s with suffix%s",
        *r_domid, d_config->c_info.name, strtime);
    rc = libxl_domain_preserve(ctx, *r_domid, &d_config->c_info,
                               strtime, new_uuid);

    /*
     * Although the domain still exists it is no longer the one we are
     * concerned with.
     */
    *r_domid = INVALID_DOMID;

    return rc == 0 ? 1 : 0;
}

static void evdisable_disk_ejects(libxl_evgen_disk_eject **diskws,
                                 int num_disks)
{
    int i;

    for (i = 0; i < num_disks; i++) {
        if (diskws[i])
            libxl_evdisable_disk_eject(ctx, diskws[i]);
        diskws[i] = NULL;
    }
}

pid_t xl_waitpid(xlchildnum child, int *status, int flags)
{
    xlchild *ch = &children[child];
    pid_t got = ch->pid;
    assert(got);
    if (ch->reaped) {
        *status = ch->status;
        ch->pid = 0;
        return got;
    }
    for (;;) {
        got = waitpid(ch->pid, status, flags);
        if (got < 0 && errno == EINTR) continue;
        if (got > 0) {
            assert(got == ch->pid);
            ch->pid = 0;
        }
        return got;
    }
}

void postfork(void)
{
    libxl_postfork_child_noexec(ctx); /* in case we don't exit/exec */
    ctx = 0;

    xl_ctx_alloc();
}

void xl_report_child_exitstatus(xentoollog_level level,
                                xlchildnum child, pid_t pid, int status)
{
    libxl_report_child_exitstatus(ctx, level, children[child].description,
                                  pid, status);
}

pid_t xl_fork(xlchildnum child, const char *description) {
    xlchild *ch = &children[child];
    int i;

    assert(!ch->pid);
    ch->reaped = 0;
    ch->description = description;

    ch->pid = fork();
    if (ch->pid == -1) {
        perror("fork failed");
        exit(-1);
    }

    if (!ch->pid) {
        /* We are in the child now.  So all these children are not ours. */
        for (i=0; i<child_max; i++)
            children[i].pid = 0;
    }

    return ch->pid;
}


int child_report(xlchildnum child)
{
    int status;
    pid_t got = xl_waitpid(child, &status, 0);
    if (got < 0) {
        fprintf(stderr, "xl: warning, failed to waitpid for %s: %s\n",
                children[child].description, strerror(errno));
        return ERROR_FAIL;
    } else if (status) {
        xl_report_child_exitstatus(XTL_ERROR, child, got, status);
        return ERROR_FAIL;
    } else {
        return 0;
    }
}

static void console_child_report(xlchildnum child)
{
    if (xl_child_pid(child))
        child_report(child);
}

int create_domain(struct domain_create *dom_info)
{
    uint32_t domid = INVALID_DOMID;
    libxl_domain_config d_config;

    int debug = dom_info->debug;
    int daemonize = dom_info->daemonize;
    int monitor = dom_info->monitor;
    int paused = dom_info->paused;
    int vncautopass = dom_info->vncautopass;
    const char *config_file = dom_info->config_file;

    /* NULL */
    const char *extra_config = dom_info->extra_config;
    const char *restore_file = dom_info->restore_file;

    const char *config_source = NULL;
    const char *restore_source = NULL;
   
   	int i = 0;	
    int need_daemon = daemonize;
    libxl_evgen_domain_death *deathw = NULL;
    libxl_evgen_disk_eject **diskws = NULL;
    unsigned int num_diskws = 0;
    void *config_data = 0;
    bool config_in_json = false;
    int config_len = 0;
    int ret, rc;
    struct save_file_header hdr;
    uint32_t domid_soft_reset = INVALID_DOMID;
    libxl_asyncprogress_how *autoconnect_console_how;

    /* initial domain config */
    libxl_domain_config_init(&d_config);

    if (config_file)
    {
        //free(config_data);
        //config_data = 0;
        
        ret = libxl_read_file_contents(ctx, config_file, &config_data, &config_len);

        if(ret)
        {
            fprintf(stderr, "Failed to read domain config file: %s: %s\n",
                    config_file, strerror(errno));
            return ERROR_FAIL;

        } 
	 	config_source = config_file;
		config_in_json = false;	
    } else {
        if (!config_data) {
            fprintf(stderr, "Config file not specified and none in save file\n");
            return ERROR_INVAL;
        }
        //config_source = "<saved>";
        //config_in_json = !!(hdr.madatory_flags & XL_MANDATORY_FLAG_JSON);
    }
    
    parse_config_data(config_source, config_data, config_len, &d_config);

    ret = 0;
    
start:
    /* start creation */
    assert(domid == INVALID_DOMID);
    
    rc = acquire_lock();
    if (rc < 0)
    {
        goto error_out;
    }
    
    if (domid_soft_reset == INVALID_DOMID)
    {
        if (!freemem(domid, &d_config.b_info))
        {
            fprintf(stderr, "failed to free memory for the domain\n");
            ret = ERROR_FAIL;
            goto error_out;
        }
    }
    
    autoconnect_console_how = 0;
    //libxl_asyncprogress_how autoconnect_console_how_buf;
    if (domid_soft_reset != INVALID_DOMID)
    {
        /* soft reset */
        ret = libxl_domain_soft_reset(ctx, &d_config, domid_soft_reset,
                                      0, autoconnect_console_how);
        
        domid = domid_soft_reset;
        domid_soft_reset = INVALID_DOMID;
    }
    else {
        ret = libxl_domain_create_new(ctx, &d_config, &domid, 
                                      0, autoconnect_console_how);
    }
    
    if (ret)
    {
        goto error_out;
    }

    release_lock();
    
    if (!paused)
    {
        libxl_domain_unpause(ctx, domid);
    }
    
    ret = domid;

    if (!daemonize && !monitor)
    {
        goto out;
    }

    /* need daemon? */
    if (need_daemon)
    {
        char *name;
        xasprintf(&name, "xl-%s", d_config.c_info.name);
        ret = do_daemonize(name, NULL);
        free(name);
        if (ret)
        {
            ret = (ret == 1)? domid : ret;
            goto out;
        }
        need_daemon = 0;
    }

    LOG("Waiting for domain %s (domid %u) to die [pid %ld]",
        d_config.c_info.name, domid, (long)getpid());

    ret = libxl_evenable_domain_death(ctx, domid, 0, &deathw);
    if (ret)
    {
        goto out;
    }

    if (!diskws)
    {
        diskws = xmalloc(sizeof(*diskws) * d_config.num_disks);
        for (i = 0; i < d_config.num_disks; i++)
        {
            diskws[i] = NULL;
            
        }
        num_diskws = d_config.num_disks;
    }
    for(i = 0; i < num_diskws; i++)
    {
        if (d_config.disks[i].removable)
        {
            ret = libxl_evenable_disk_eject(ctx, domid, 
                                            d_config.disks[i].vdev, 
                                            0, &diskws[i]);
            if (ret) goto out;
        }
    }

    while(1)
    {
        libxl_event *event;
        ret = domain_wait_event(domid, &event);

        if (ret) goto out;

        switch(event->type)
        {
            case LIBXL_EVENT_TYPE_DOMAIN_SHUTDOWN:
                LOG("Domain %u has shut down, reason code %d 0x%x", 
                    domid,
                    event->u.domain_shutdown.shutdown_reason,
                    event->u.domain_shutdown.shutdown_reason);
                switch(handle_domain_death(&domid, event, &d_config))
                {
                    case DOMAIN_RESTART_SOFT_RESET:
                        domid_soft_reset = domid;
                        domid = INVALID_DOMID;
                    case DOMAIN_RESTART_RENAME:
                        if(domid_soft_reset == INVALID_DOMID &&
                           !preserve_domain(&domid, event, &d_config))
                        {
                            libxl_event_free(ctx, event);
                            ret = -1;
                            goto out;
                        }
                    case DOMAIN_RESTART_NORMAL:
                        libxl_event_free(ctx, event);
                        libxl_evdisable_domain_death(ctx, deathw);
                        deathw = NULL;
                        evdisable_disk_ejects(diskws, num_diskws);
                        free(diskws);
                        diskws = NULL;
                        num_diskws = 0;

                        while(!(ret = libxl_event_check(ctx, &event,
                                                        LIBXL_EVENTMASK_ALL, 0, 0))){
                            libxl_event_free(ctx, event);
                        }
                        if (ret != ERROR_NOT_READY)
                        {
                            LOG("warning, libxl_event_check (cleanup) failed (rc=%d)", ret);
                        }

                        dom_info->console_autoconnect = 0;
                        paused = 0;
                        
                        if(common_domname && strcmp(d_config.c_info.name, common_domname)){
                            d_config.c_info.name = strdup(common_domname);
                        }

                        LOG("Done. Rebooting now");
                        /* without sleep(), creation may fail */
                        sleep(2);
                        goto start;
                    case DOMAIN_RESTART_NONE:
                        LOG("Done. Exiting now");
                        libxl_event_free(ctx, event);
                        ret = 0;
                        goto out;
                    default:
                        abort();
                }
            case LIBXL_EVENT_TYPE_DOMAIN_DEATH:
                LOG("Domain %u has been destroyed.", domid);
                libxl_event_free(ctx, event);
                ret = 0;
                goto out;
            case  LIBXL_EVENT_TYPE_DISK_EJECT:
                libxl_cdrom_insert(ctx, domid, &event->u.disk_eject.disk, NULL);
                break;
            default: ;
                char *evstr = libxl_event_to_json(ctx, event);
                LOG("warning, got unexpected event type %d, event=%s",
                    event->type, evstr);
                free(evstr);
        }
        
        libxl_event_free(ctx, event);
    }

error_out:
    release_lock();
    if (libxl_domid_valid_guest(domid))
    {
        libxl_domain_destroy(ctx, domid, 0);
        domid = INVALID_DOMID;
    }

out:
    if(logfile != 2) close(logfile);
    
    libxl_domain_config_dispose(&d_config);
    free(config_data);

    console_child_report(child_console);

    if (deathw) libxl_evdisable_domain_death(ctx, deathw);

    if(diskws)
    {
        evdisable_disk_ejects(diskws, d_config.num_disks);
        free(diskws);
    }

    /* if daemonized then do no return to caller */
    if (daemonize && !need_daemon) exit(ret);

    return ret;
}

int main_pause(uint32_t domid)
{
    libxl_domain_pause(ctx, domid);

    return EXIT_SUCCESS;
}

int main_unpause(uint32_t domid)
{
    libxl_domain_unpause(ctx, domid);

    return EXIT_SUCCESS;
}

int main_create(const char *filename)
{
    struct domain_create dom_info;
    int paused = 0, debug = 0, daemonize = 1, quiet = 0;
    int console_autoconnect = 0, monitor = 1, vnc = 0, vncautopass = 0;
    int opt, rc;

    /* init dom_info */
    memset(&dom_info, 0, sizeof(dom_info));

    dom_info.debug = debug;
    dom_info.daemonize = daemonize;
    dom_info.monitor = monitor;
    dom_info.paused = paused;
    dom_info.dryrun = dryrun_only;
    dom_info.quiet = quiet;
    dom_info.config_file = filename;
    dom_info.migrate_fd = -1;
    dom_info.send_back_fd = -1;
    dom_info.vnc = vnc;
    dom_info.vncautopass = vncautopass;
    dom_info.console_autoconnect = console_autoconnect;

    rc = create_domain(&dom_info);
    return rc;
}

/* NOTE: must be called once before all actions */
int initial_libxl(void)
{
	int ret, config_len = 0;
	void *config_data = 0;

	// initial logger 
	logger = xtl_createlogger_stdiostream(stderr, minmsglevel,
			(progress_use_cr?XTL_STDIOSTREAM_PROGRESS_USE_CR : 0));
	if(!logger) exit(EXIT_FAILURE);

	// create ctx 
	atexit(xl_ctx_free);
	xl_ctx_alloc();

	// read and parse xl global config
	ret = libxl_read_file_contents(ctx, XL_GLOBAL_CONFIG,
			&config_data, &config_len);
	if(ret)
		fprintf(stderr, "Failed to read global config: %s: %s\n",
				XL_GLOBAL_CONFIG, strerror(errno));
	
	parse_global_config(XL_GLOBAL_CONFIG, config_data, config_len);
	
	free(config_data);

	return EXIT_SUCCESS;
}

/*
int main(int argc, char *argv[])
{
	char file[100]={'\0'};	
	int ret, config_len = 0;
	void *config_data = 0;

	if (argc == 2)
	{	
   		strcpy(file, argv[1]);
	}
	
	// initial logger 
	logger = xtl_createlogger_stdiostream(stderr, minmsglevel,
			(progress_use_cr?XTL_STDIOSTREAM_PROGRESS_USE_CR : 0));
	if(!logger) exit(EXIT_FAILURE);

	// create ctx 
	atexit(xl_ctx_free);
	xl_ctx_alloc();

	// read and parse xl global config
	ret = libxl_read_file_contents(ctx, XL_GLOBAL_CONFIG,
			&config_data, &config_len);
	if(ret)
		fprintf(stderr, "Failed to read global config: %s: %s\n",
				XL_GLOBAL_CONFIG, strerror(errno));
	parse_global_config(XL_GLOBAL_CONFIG, config_data, config_len);
	free(config_data);

	// create/start our vm
	main_create(file);

	return 0;
}
*/

#ifdef __cplusplus
}
#endif
/*
int main(int argc, char *argv[])
{
	uint32_t domid = 3;

	if (initial_libxl() == EXIT_FAILURE) {
		fprintf(stderr, "[ck] Failed to initialize libxl.\n");
		exit(EXIT_FAILURE);
	}
	
	main_shutdown(domid);

	return 0;
}
*/
//#ifdef __cplusplus
//}
//#endif
