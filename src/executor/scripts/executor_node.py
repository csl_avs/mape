#!/usr/bin/env python
from nodes import Nodes
import rospy
import datetime
import logging
from vmpartitions import VMManager
from ROSNodes import ROSNode
from acronis_common.msg import Plan, Payload
from acronis_common.msg import NodeList
from mape_misc.opcode import OPCODE


def recoverPlan(rollback):
    # reverse iterate rollback list to recover system to a previous state
    if rollback is None:
        return 0

    # TODO: How to evaluate if the exec succeeds or not
    for action in reversed(rollback):
        exec(action)

    return 0


def recoverLog(rollback):
    # record recover action
    log_title = 'recover log:' + \
                datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    if rollback is not None:
        # logging.basicConfig(filename='executor.log', level=logging.DEBUG)
        logging.info(log_title)
        for item in rollback:
            logging.info(item)

def getDataField(keyname, payload):
    for item in payload:
        print("{}: {}".format(item.key, item.value))
        if item.key == keyname:
            return item.value
    

def plan_parser(data, rb=False):
    # Plan.msg includes decisions[], each decision is:
    # 1. Header op_header
    # 2. uint32 opcode
    # 3. String node_name
    # 4. String[] payload, could be empty if no cmd is required
    # rospy.loginfo("Received new msg @ %s" % data.op_header.stamp)

    # define a recovery action list
    print("[debug] msg is %s" % data)
    rospy.loginfo('[debug] %s' % data)
    rollback = []
    res = -1

    # determine how rollback actions are handled
    if rb is False:
        rbFunct = recoverLog
    else:
        rbFunct = recoverPlan

    nodeMgr = Nodes()
    # check if node_name is valid
    for action in data.decisions:
        rospy.loginfo('New decision @ %s' % action.op_header.stamp)

        if getDataField('class', action.payload) == "1":
            print("reset %s" % action.node_name)
            vmm = VMManager()
            res = vmm.reboot_vm(action.node_name)  
        elif getDataField('class', action.payload) == "2" and action.node_name in nodeMgr.getNodesList():
            # parse opcode
            if action.opcode == OPCODE.START.value:
                # start a node
                # res = nodes.startNode(action.node_name)
                rollback.append('nodes.stopNode(%s)' % action.node_name)
                res = nodeMgr.startNode(action.node_name)
                if res < 0:
                    rbFunct(rollback)
            elif action.opcode == OPCODE.RESTART.value:
                # resume a node (for vm)
                print(action.payload)
                if getDataField('class', action.payload) == 1:
                    print("reset %s" % action.node_name)
                    vmm = VMManager()
                    res = vmm.reboot_vm(action.node_name)
                    # res = nodeMgr.resumeNode(action.node_name)
            elif action.opcode == OPCODE.STOP.value:
                # stop a node
                # res = nodes.stopNode(action.node_name)
                rollback.append('nodes.startNode(%s)' % action.node_name)
                res = nodeMgr.stopNode(action.node_name)
                if res < 0:
                    rbFunct(rollback)
            elif action.opcode == OPCODE.FREEZE.value:
                # pause a node
                # res = nodes.pauseNode(action.node_name)
                rollback.append('nodesresumeNode(%s)' % action.node_name)
                res = nodeMgr.pauseNode(action.node_name)
                if res < 0:
                    rbFunct(rollback)
            elif action.opcode == OPCODE.EXECUTE.value:
                # execute cmd --option in a node
                # TODO: rollback operation. Taking a snapshot for rollback?
                snap_name = action.node_name + '_'
                + datetime.datetime.now().strftime('%Y%m%d%H%M%S')

                # create a snapshot for rollback operation
                nodeMgr.snapshotNode(action.node_name, snap_name)
                rollback.append('nodes.restoreNode({0}, {1})'.format(
                    action.node_name, snap_name))
                res = nodeMgr.nodeExec(action.name, action.playload[0],
                                       action.payload[1])
                if res < 0:
                    rbFunct(rollback)
            elif action.opcode == OPCODE.ROSLAUNCH.value:
                # execute 'roslaunch launch_file' to start a new ros node
                # in a partition
                # TODO: rollback operation. Taking a snapshot for rollback?
                snap_name = action.node_name + '_'
                snap_name += datetime.datetime.now().strftime('%Y%m%d%H%M%S')

                # create a snapshot for rollback operation
                nodeMgr.snapshotNode(action.node_name, snap_name)
                rollback.append('nodes.restoreNode({0}, {1})'.format(
                    action.node_name, snap_name))
                # use ROSNode to execute 'launch'
                rosmgr = ROSNode()
                res = rosmgr.start_launch_file(
                    action.payload[0], action.payload[1])
                if res < 0:
                    rbFunct(rollback)
            else:
                pass
    time_now = rospy.get_rostime()
    rospy.loginfo('current time is {} {}\n'.
                  format(time_now.secs, time_now.nsecs))


def rosnode_plan_parser(data, rb=False):
    # data is a piece of Plan messsage
    print("[debug] msg is %s" % data)
    rospy.loginfo('[debug] %s' % data)
    rollback = []
    res = -1

    # determine how rollback actions are handled
    if rb is False:
        rbFunct = recoverLog
    else:
        rbFunct = recoverPlan

    nodeMgr = ROSNode()
    # check if node_name is valid
    for action in data.decisions:
        rospy.loginfo('New decision @ %s sec' % action.op_header.stamp)

        # if action.node_name in nodeMgr.getNodesList():
        # parse opcode
        if action.opcode == OPCODE.START.value:
            # start a node
            # when launch_file is used, it's should be in action.payload
            # rollback.append('nodes.stopNode(%s)' % action.node_name)
            if action.payload[0] == 'roslaunch':
                # if a launch file is used
                # roslaunch pkg launch_file
                res = nodeMgr.start_launch_file(action.payload[1],
                                                action.payload[2])
            elif action.payload[0] == 'rosrun':
                # or 'rosrun pkg node_exe_file' should be used
                res = nodeMgr.start_pkg_node(action.payload[1],
                                             action.payload[2])
            else:
                rospy.loginfo('Start node method unknown: %s'
                              % action.payload[0])

            if res < 0:
                rbFunct(rollback)
        elif action.opcode == OPCODE.ROSRUN.value:
            res = nodeMgr.start_pkg_node(action.payload[1],
                                         action.payload[2])
        elif action.opcode == OPCODE.ROSLAUNCH.value:
            res = nodeMgr.start_launch_file(action.payload[1],
                                            action.payload[2])
        elif action.opcode == OPCODE.STOP.value:
            # stop a node
            # res = nodes.stopNode(action.node_name)
            rollback.append('nodes.startNode(%s)' % action.node_name)
            res = nodeMgr.stop_node(action.node_name)
            if res < 0:
                rbFunct(rollback)
        else:
            # ROSNode can only operate in process level
            # so only start & stop is supported
            rospy.loginfo('ROSNode: unknow action: %s' % action)


def publish_topics():
    # publish all topics from executor
    pub = rospy.Publisher("nodes_list", NodeList, queue_size=10)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        pass


def init_listenting():
    # start node and subscribe to msgs
    rospy.init_node('executor_node', anonymous=True)
    print(rospy.get_name())

    rospy.Subscriber('sample_plan', Plan, plan_parser)
    rospy.Subscriber('rosnode_plan', Plan, rosnode_plan_parser)
    # keeps listening
    rospy.spin()


if __name__ == '__main__':
    try:
        logging.basicConfig(filename='executor.log', level=logging.DEBUG)
        init_listenting()
    except rospy.ROSInterruptException:
        pass
