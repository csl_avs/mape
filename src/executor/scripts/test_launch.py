#!/usr/bin/env python
from ROSNodes import ROSNode
import time

if __name__ == '__main__':
    node_mgr = ROSNode()
    launch = 'nmea2tfpose.launch'
    
    print('begin: %.9f' % time.time())
    node_mgr.start_launch_file('gnss_localizer', launch)
    print('end: %.9f' % time.time())
