#!/usr/bin/env python
import sys
import libvirt

class VMManager(object):
    def __init__(self):
        self.__conn = libvirt.open(None)
        self.__doms = self.__conn.listAllDomains()
        
    def reboot_vm(self, vm_name):
        try:
            domu = self.__conn.lookupByName(vm_name)
        except:
            print("An error happened when I am looking for %s." % vm_name)
            sys.exit(1)

        try:
            domu.reboot()
        except:
            print("An error occurred while %s is reset." % vm_name)
            sys.exit(1)

if __name__ == '__main__':
    # test
    mgr = VMManager()
    mgr.reboot_vm('aw_pathfollow')
