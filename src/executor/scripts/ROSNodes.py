import os
import rospy
import warnings
import roslaunch
import roslib.scriptutil as scriptutil
from multiprocessing import Process
import rosnode


class ROSNode(object):
    def __init__(self):
        # perhaps the list has to be refreshed as zombie nodes may exist
        rosnode.rosnode_cleanup()
        self.__node_list = rosnode.get_node_names()
        # launcher is no longer required
        # self.launcher = roslaunch.scriptapi.ROSLaunch()
        # self.launcher.start()
        
    def __refresh_node_list(self):
        # rosnode.rosnode_cleanup()
        alive, zombie = rosnode.rosnode_ping_all()
        if zombie:
            master = scriptutil.get_master()
            rosnode.cleanup_master_blacklist(master, zombie)
        self.__node_list = rosnode.get_node_names()

    def get_node_list(self):
        self.__refresh_node_list()
        return self.__node_list

    def __start_pkg_node(self, pkg, exe_name):
        warnings.warn("Deprecated", DeprecationWarning)
        return
        # do things like 'rosrun package executable_name'
        # pkg is the package name
        # exe_name is the name the executable file
        
        node = roslaunch.core.Node(pkg, exe_name)
        tmp_launcher = roslaunch.scriptapi.ROSLaunch()
        tmp_launcher.start()
        #node_process = self.launcher.launch(node)
        node_process = tmp_launcher.launch(node)
        if node_process.is_alive():
            print("Spawning succeeds")
            return 0
        else:
            return ~0

    def __os_start_node(self, pkg, exe_name):
        os.system('rosrun {0} {1}'.format(pkg, exe_name))

    def start_pkg_node(self, pkg, exe_name):
        node_process = Process(target=self.__os_start_node,
                               args=(pkg, exe_name))
        node_process.start()

    def __start_launch_file(self, file_path):
        warnings.warn("Deprecated", DeprecationWarning)
        # start a node by a given launch_file
        if not os.path.isfile(file_path):
            return ~0
            
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)

        roslaunch.configure_logging(uuid)
        launch = roslaunch.parent.ROSLaunchParent(uuid, file_path)
        launch.start()

        return 0

    def __os_launch_node(self, pkg, launch_file):
        os.system('roslaunch {0} {1}'.format(pkg, launch_file))

    def start_launch_file(self, pkg, launch_file):
        launch_process = Process(target=self.__os_launch_node,
                                 args=(pkg, launch_file))
        launch_process.start()

    def __stop_node(self, node_name):
        warnings.warn('Deprecated', DeprecationWarning)
        return
        # kill a node by its complete name
        rospy.loginfo('killing %s' % node_name)
        success, failure = rosnode.kill_nodes(node_name)
        if node_name in success:
            print('%s killed' % node_name)
        self.__refresh_node_list()

    def __os_kill_node(self, node_name):
        os.system('rosnode kill {0}'.format(node_name))

    def stop_node(self, node_name):
        terminator = Process(target=self.__os_kill_node, args=(node_name,))
        terminator.start()
        # wait for terminator to finish
        terminator.join()
        # print terminator.pid
        while(True):
            if node_name not in self.get_node_list():
                terminator.terminate()
                # terminator.join()
                return 0

    def stop_node_api(self, node_name):
        self.__stop_node(node_name)
