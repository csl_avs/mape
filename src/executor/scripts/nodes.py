import os
import pylxd


class Nodes:
    def __init__(self):
        self.client = pylxd.Client()
        self.api = self.client.api
        
    def getNodesList(self):
        containers = self.client.containers.all()
        # 3.5 all = containers.all()
        # all = containers.all
        if (all is None):
            return None

        nodeList = []
        
        for node in containers:
            nodeList.append(str(node.name))

        # self.nodeList = nodeList
        return nodeList

    def GetNodesStatus(self):
        nodes = self.getNodesList()

        if nodes is None:
            return ~0

        nodeStatusList = list()
        for node in nodes:
            nodeStatusList.append(([node, str(self.getNodeStatus(node))]))
        return nodeStatusList

    def _getNodebyName(self, name):
        containers = self.client.containers
        # all = containers.all()

        if(name is None):
            return None
        else:
            return containers.get(name)

    def getNodeStatus(self, name):
        # containers = self.client.containers
        node = self._getNodebyName(name)
        if(node is None):
            return ~0

        return node.status

    def getNodeAddress(self, name):
        # containers = self.client.containers.all()
        node = self._getNodebyName(name)
        if(node is None):
            return ~0
        elif (node.status != 'Running'):
            return ~0

        state = node.state()
        address = state.network['eth0']['addresses'][0]['address']

        return str(address)
    
    def startNode(self, name):
        # containers = self.client.containers.all()

        node = self._getNodebyName(name)
        if(node is None):
            return ~0

        if(node.status != 'Running'):
            node.start()
            # Sync required?
            node.sync()

        return 0

    def pauseNode(self, name):
        node = self._getNodebyName(name)

        if (node is None):
            return ~0

        if (node.status != 'Frozen'):
            node.freeze()

        return 0

    def resumeNode(self, name):
        node = self._getNodebyName(name)
        if node is None:
            return ~0

        if node.status == 'Frozen':
            node.unfreeze()

        node.sync()

        return 0
    
    def stopNode(self, name):
        # containers = self.client.containers.all()
        node = self._getNodebyName(name)

        if(node is None):
            return ~0

        if(node.status != 'Stopped'):
            node.stop()
            # node.sync()

        return 0
    
    def nodeExec(self, name, command, payload):
        # need fixing
        # containers = self.client.containers.all()
        node = self._getNodebyName(name)
        if(node is None):
            return ~0
        # fix required as we don't know the input format of command
        return node.execute([command, payload])
        # return 0

    def snapshotNode(self, name, snap_name, stateful=False):
        node = self._getNodebyName(name)

        if node is None:
            return ~0

        return node.snapshots.create(snap_name, stateful)
    
    def recoverNode(self, name, snap_name):
        node = self._getNodebyName(name)

        if node is None:
            return ~0
        # return -1 if snap_name doesn't exist
        if node.snapshots.get(snap_name) is None:
            return ~0
        # currently pylxd doesn't provide a restore method
        cmd = 'lxc restore {0} {1}'.format(name, snap_name)
        # commonly 0 is return if the execution is successful
        return os.system(cmd)
    
    # TODO: FIX BUGS
    def _CreateNode(self, name, codepath, package):
        # 1. create a container named 'name' using ros-indigo-base image
        # 2. pass node's code in to the container
        # 3. [optiaonal] call catkin_make_release to compile the code
        # 4. launch the node using roslaunch
        containers = self.client.containers

        newname = name+'_container'
        config = {'name': newname, 'source':
                  {'type': 'image', 'alias': 'ros-indigo-base'}}
        container = containers.create(config, wait=True)

        if containers.get(newname) is None:
            print('Error creating container %s' % (newname))

        container.ephemeral = False
        container.devices = {'root': {'path': '/',
                                      'type': 'disk', 'pool': 'pool'}}
        containers.save

        # codepath should be catkin_ws's, like /home/user/catkin_ws
        pushcmd = 'lxc file push -r %s %s/home/ubuntu/%s' % (
            codepath, newname, os.path.basename(codepath))
        os.system(pushcmd)

        # build source code
        container.start()
        workspace = '/home/ubuntu/%s' % (os.path.basename(codepath))
        container.execute(['cd', workspace])
        container.execute(['catkin_make'])

        # TODO: use roslaunch to start the package
        container.execute(['roslaunch', package, package+'.launch'])
        
