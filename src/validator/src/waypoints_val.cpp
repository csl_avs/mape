#include <iostream>
#include <cmath>
#include <ros/ros.h>

// waypoints are lane.waypont[], vector 
#include <autoware_msgs/lane.h>
#include <autoware_msgs/waypoint.h>

autoware_msgs::lanePtr prev_wp;

double wp_dist_thd = 1.0;
double lane_width = 3.5;


/* determine if the current waypoints is jointable with the previous */
bool wp_is_joint(const autoware_msgs::laneConstPtr &cur)
{
    /* TODO: define physical rules */
}

/* return euclidean distance of two points (x,y) */
static double dist_xy(double x1, double x2, double y1, double y2)
{
    return std::sqrt(std::pow(x1 - x2, 2) + std::pow(y1 - y2, 2));
}

/* return distance in the same axis */
static double dist_axis(double x, double y)
{
    return std::abs(x - y);
}

bool wp_valid(const autoware_msgs::laneConstPtr &cur)
{
    /* TODO: check the internal correlation of vector cur->waypoints */
    double dist = 0, dist_avg = 0;
    double _x1 = 0, _y1 = 0;
    double x1 = 0, y1 = 0;
    int warnings = 0, errors = 0, validcount = 0;

    _x1 = cur->waypoints[0].pose.pose.position.x;
    _y1 = cur->waypoints[0].pose.pose.position.y;
    
    for (uint i = 1; i < cur->waypoints.size(); i++)
    {
        x1 = cur->waypoints[i].pose.pose.position.x;
        y1 = cur->waypoints[i].pose.pose.position.y;
        double _dist = dist_xy(x1, _x1, y1, _y1);

        /* if current wp distance is above average */
        if(_dist > dist_avg)
        {
            warnings += 1;
            if (dist_axis(x1, _x1) > lane_width)
            {
                errors += 1;
            }
        }
        else
        {
            /* update dist */
            validcount += 1;
            dist += _dist;
            dist_avg = dist/validcount;
        }
    }

    
}

void bw_callback(const autoware_msgs::laneConstPtr &msg)
{
    /* skip old known waypoints */
    if (!prev_wp || msg->header.stamp != prev_wp->header.stamp)
    {
        prev_wp->header = msg->header;
        prev_wp->increment = msg->increment;
        prev_wp->waypoints = msg->waypoints;
    }

    if(!wp_valid(msg))
    {
        
    }

    if(prev_wp)
    {
        if(!wp_is_joint(msg))
        {
            /* TODO: waypoint alerts */
        }
    }
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "waypoints_validator");
    ros::NodeHandle nh;

    ros::Subscriber bw_sub = nh.subscribe("/base_waypoints", 1, bw_callback);
    ros::Subscriber fw_sub = nh.subscribe("/final_waypoints", 1, fw_callback);

    ros::spin();
}
