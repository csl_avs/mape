/* Used to generate CAN frames according to control cmd topics
 * 
 */
#include <iostream>
#include <string>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <pthread.h>
#include <linux/can.h>
#include <net/if.h>

#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>
/* Headers from Autoware 1.5.1 */
#include <tablet_socket_msgs/mode_cmd.h>
#include <tablet_socket_msgs/gear_cmd.h>
/*
#include <runtime_manager/accel_cmd.h>
#include <runtime_manager/brake_cmd.h>
#include <runtime_manager/steer_cmd.h>

#include <waypoint_follower/ControlCommandStamped.h>
*/
#include <autoware_msgs/accel_cmd.h>
#include <autoware_msgs/brake_cmd.h>
#include <autoware_msgs/steer_cmd.h>
#include <autoware_msgs/ControlCommandStamped.h>

#include <acronis_common/VehicleStatus.h>
/* defined headers */
//#include <can_simulator/vehicle.h>
/* use headers in monitor to avoid circular dependency */
//#include <monitor/vehicle.h>
#include <can_simulator/sim_utils.h>

static int socket_can;
static struct CommandData command_data;
/* parsing and storing /prius/status */
static struct VehicleStatus vehicle_status;

static int send_single_frame(const struct can_frame frame)
{
    //socket_lock.lock();
    int nbytes = write(socket_can, &frame, sizeof(frame));
    //packet_sent++;
    /*
    if((packet_sent % 100) == 0)
    {
        printf("%d packets sent.\n", packet_sent);
    }
    socket_lock.unlock();
    */
    if (nbytes < 0)
    {
        //std::cout << boost::this_thread::get_id() << '\n';
        printf("socket sending error!\n");
        return -1;
    }
    
    //std::cout << boost::this_thread::get_id() << ": " << nbytes << " bytes sent.\n";
    return 0;
}
                                             
static void twistCMDCallback(const geometry_msgs::TwistStamped& msg)
{
  command_data.linear_x = msg.twist.linear.x;
  command_data.angular_z = msg.twist.angular.z;

  send_can_frame<double>(VEHICLE_CANID, SPEED_CMD_ID, command_data.linear_x);
  send_can_frame<double>(VEHICLE_CANID, AXIS_Z_ID, command_data.angular_z);
}

static void modeCMDCallback(const tablet_socket_msgs::mode_cmd& mode)
{
  if(mode.mode == -1 || mode.mode == 0){
    command_data.reset();
  }

  command_data.modeValue = mode.mode;
  //send_can_frame(MODE_ID);
}

static void gearCMDCallback(const tablet_socket_msgs::gear_cmd& gear)
{
  command_data.gearValue = gear.gear;

  //send_can_frame(GEAR_ID);
}

static void accellCMDCallback(const autoware_msgs::accel_cmd& accell)
{
  command_data.accellValue = accell.accel;

  //send_can_frame(ACCEL_ID);
}

static void steerCMDCallback(const autoware_msgs::steer_cmd& steer)
{
  command_data.steerValue = steer.steer;

  //send_can_frame(STEER_ID);
}

static void brakeCMDCallback(const autoware_msgs::brake_cmd &brake)
{
  command_data.brakeValue = brake.brake;

  //send_can_frame(BRAKE_ID);
}

static void ctrlCMDCallback(const autoware_msgs::ControlCommandStamped& msg)
{
  command_data.linear_velocity = msg.cmd.linear_velocity;
  command_data.steering_angle = msg.cmd.steering_angle;
  //send_can_frame(CTRL_ID);
  
  send_can_frame<double>(VEHICLE_CANID, ACCEL_CMD_ID, command_data.linear_velocity);
  send_can_frame<double>(VEHICLE_CANID, STEER_ANGLE_ID, command_data.steering_angle);
  

  /* generate some steer cmd */
  // command_data.steerValue = command_data.steering_angle;

  // if (command_data.linear_velocity > 0)
  // {
  //     send_can_frame(ACCEL_ID);
  // }
  // else if (command_data.linear_velocity < 0)
  // {
  //     send_can_frame(BRAKE_ID);
  // }

  // if (command_data.steerValue !=0)
  // {
  //     send_can_frame(STEER_ID);
  // }
  
}

/* TODO: waiting for the message defination */
void priusStatusCallback(const acronis_common::VehicleStatusConstPtr &ptr)
{
    vehicle_status.accel_pedal_pos = ptr->accel_pedal_position;
    vehicle_status.brake_pos = ptr->brake_position;
    vehicle_status.steer_wheel_pos = ptr->steering_wheel_position;
    vehicle_status.steering_wheel_sp = ptr->steering_wheel_speed;

    send_can_frame<double>(VEHICLE_CANID, ACCEL_CMD_ID, vehicle_status.accel_pedal_pos);
    send_can_frame<double>(VEHICLE_CANID, BRAKE_POS_ID, vehicle_status.brake_pos);
    send_can_frame<double>(VEHICLE_CANID, STEER_SPEED_ID, vehicle_status.steer_wheel_pos);
    send_can_frame<double>(VEHICLE_CANID, STEER_ANGLE_ID, vehicle_status.steering_wheel_sp);
}

int create_can_socket(void)
{
    struct sockaddr_can addr_can;
    struct ifreq ifr_can;

    /* create a raw socket for CAN */
    if((socket_can = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        printf("Error happened in creating CAN socket!\n");
        return -1;
    }

    /* find and return CAN interface, saved in ifr_can */
    //ifr_can.ifr_name = {'/0'};
    strncpy(ifr_can.ifr_name, CAN_DEV.c_str(), IFNAMSIZ-1);
    ifr_can.ifr_name[IFNAMSIZ-1] = '\0';
    //ifr_can.ifr_ifindex = if_nametoindex(ifr_can.ifr_name);
    if(ioctl(socket_can, SIOCGIFINDEX, &ifr_can) < 0)
    {
        printf("ioctl Error! errid: %d, if_nametoindex %d\n", errno, if_nametoindex(ifr_can.ifr_name));
        return -1;
    }

    addr_can.can_family = AF_CAN;
    addr_can.can_ifindex = ifr_can.ifr_ifindex;

    /* bind socket to the CAN interface */
    if(bind(socket_can, (struct sockaddr *)&addr_can, sizeof(addr_can)) < 0)
    {
        printf("bind error! id %d\n", errno);
        return -1;
    }

    return 0;
}

int main(int argc, char **argv)
{
    ros::init(argc ,argv, "can_simulator") ;
    ros::NodeHandle nh;

    bool can_simulation = true;
    if (nh.hasParam("can_simulation"))
    {
        nh.getParam("/can_simulation", can_simulation);
    }

    if(!can_simulation)
    {
        CAN_DEV.assign(CAN_DEV_REAL);
    }
    else
    {
        CAN_DEV.assign(CAN_DEV_VIRTUAL);
    }
    std::cout << "Use CAN interface " <<  CAN_DEV << std::endl;

    if (create_can_socket() < 0)
    {
        std::cout << "Socket CAN error, exiting..." << std::endl;
        exit(-1);
    }
  
    std::cout << "Vehicle CAN sender" << std::endl;

    ros::Subscriber sub[8];
    sub[0] = nh.subscribe("/twist_cmd", 1, twistCMDCallback);
    sub[1] = nh.subscribe("/mode_cmd",  1, modeCMDCallback);
    sub[2] = nh.subscribe("/gear_cmd",  1, gearCMDCallback);
    sub[3] = nh.subscribe("/accel_cmd", 1, accellCMDCallback);
    sub[4] = nh.subscribe("/steer_cmd", 1, steerCMDCallback);
    sub[5] = nh.subscribe("/brake_cmd", 1, brakeCMDCallback);
    sub[6] = nh.subscribe("/ctrl_cmd", 1, ctrlCMDCallback);
    sub[7] = nh.subscribe("/prius/status", 1, priusStatusCallback);
  
    command_data.reset();
    /*
      pthread_t th;
      if(pthread_create(&th, nullptr, receiverCaller, nullptr) != 0){
      std::perror("pthread_create");
      std::exit(1);
      }

      if (pthread_detach(th) != 0){
      std::perror("pthread_detach");
      std::exit(1);
      }
    */
    ros::spin();
    return 0;
}
