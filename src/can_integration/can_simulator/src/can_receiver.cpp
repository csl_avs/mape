/* Used to translate ECU CAN frames into cmd topics
 * 
 */
#include <iostream>
#include <string>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
//#include <pthread.h>
#include <linux/can.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>

#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>
/* Headers from Autoware 1.5.1 */
#include <tablet_socket_msgs/mode_cmd.h>
#include <tablet_socket_msgs/gear_cmd.h>
/*
#include <runtime_manager/accel_cmd.h>
#include <runtime_manager/brake_cmd.h>
#include <runtime_manager/steer_cmd.h>

#include <waypoint_follower/ControlCommandStamped.h>
*/
#include <autoware_msgs/accel_cmd.h>
#include <autoware_msgs/brake_cmd.h>
#include <autoware_msgs/steer_cmd.h>
#include <autoware_msgs/ControlCommandStamped.h>

#include <acronis_common/VehicleStatus.h>
/* defined headers */
//#include <can_simulator/vehicle.h>
/* use headers in monitor to avoid circular dependency */
//#include <monitor/vehicle.h>
#include <can_simulator/sim_utils.h>

static int socket_can;
static struct CommandData command_data;
static struct VehicleStatus vehicle_status;
// publish /prius/twist_cmd
static ros::Publisher twist_pub, status_pub;

static void receive_from_can(void)
{
    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler

    /* open can bus interface for frame capturing */
    hl = pcap_open_live(CAN_DEV.c_str(), BUFSIZ, 1, 1000, err_buff);
    if (hl == NULL)
    {
        /* open failed */
        ROS_DEBUG("Couldn't open device %s!\n", CAN_DEV.c_str());
        return;
    }
    
    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        /* data is not a socketCAN frame */
        ROS_DEBUG("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return;
    }

    std::cout << CAN_DEV << " opened successfully!" << std::endl;

    struct can_frame *can_f;
    geometry_msgs::TwistStamped new_msg;
    new_msg.twist.linear.x = vehicle_status.linear_x;
    new_msg.twist.angular.z = vehicle_status.angular_z;
    
    acronis_common::VehicleStatus new_status;
    new_status.accel_pedal_position = vehicle_status.accel_pedal_pos;
    new_status.brake_position = vehicle_status.brake_pos;
    new_status.steering_wheel_position = vehicle_status.steer_wheel_pos;
    new_status.steering_wheel_speed = vehicle_status.steering_wheel_sp;
    
    int factor = 1; // default is positive, -1 is for negtive value
    
    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        can_f = (struct can_frame *)pkt;

        if (swap_byte_order(can_f->can_id) == ECU_CANID)
        {
            union {
                uint32_t value;
                uint8_t bytes[4];
            }value_cast;

            if ((can_f->can_dlc > 0) && (can_f->can_dlc % 4 == 0))
            {
                memset(value_cast.bytes, 0, 4);

                /* recover value */
                value_cast.bytes[3] = 0x0;
                
                if(can_f->data[1] & 0x80)
                {
                    /* value is negative */
                    value_cast.bytes[2] = can_f->data[1] & 0x7f;
                    factor = -1;
                }
                else{
                    value_cast.bytes[2] = can_f->data[1];
                    factor = 1;
                }

                value_cast.bytes[1] = can_f->data[2];
                value_cast.bytes[0] = can_f->data[3];
                
                //value_cast.value *= factor;
                /* recovery completed */
                
                std::cout << "received: " << std::hex << can_f->can_id << ", ";
                std::cout << "0x" << std::hex << (int)can_f->data[0];
                std::cout << " 0x" << std::hex << (int)can_f->data[1];
                std::cout << " 0x" << std::hex << (int)can_f->data[2];
                std::cout << " 0x" << std::hex << (int)can_f->data[3];
                std::cout << ", value is "<< std::dec << value_cast.value << std::endl;
                
                // geometry_msgs::TwistStamped new_msg;
                switch(can_f->data[0])
                {
                    case SPEED_CMD_ID:
                        new_msg.header.stamp = ros::Time::now();
                        vehicle_status.linear_x = factor * (double)(value_cast.value) / 100;
                        new_msg.twist.linear.x = vehicle_status.linear_x;
                        twist_pub.publish(new_msg);
                        break;
                    case AXIS_Z_ID:
                        new_msg.header.stamp = ros::Time::now();
                        vehicle_status.angular_z = factor * (double)(value_cast.value) / 100;
                        new_msg.twist.angular.z = vehicle_status.angular_z;
                        twist_pub.publish(new_msg);
                        break;
                    case ACCEL_CMD_ID:
                        vehicle_status.accel_pedal_pos = factor * (double)(value_cast.value) / 100;
                        new_status.accel_pedal_position = vehicle_status.accel_pedal_pos;
                        status_pub.publish(new_status);
                        break;
                    case BRAKE_POS_ID:
                        vehicle_status.brake_pos = factor * (double)(value_cast.value) / 100;
                        new_status.brake_position = vehicle_status.brake_pos;
                        status_pub.publish(new_status);
                        break;
                    case STEER_SPEED_ID:
                        vehicle_status.steer_wheel_pos = factor *(double)(value_cast.value) / 100;
                        new_status.steering_wheel_position = vehicle_status.steer_wheel_pos;
                        status_pub.publish(new_status);
                        break;
                    case STEER_ANGLE_ID:
                        vehicle_status.steering_wheel_sp = factor * (double)(value_cast.value) / 100;
                        new_status.steering_wheel_speed = vehicle_status.steering_wheel_sp;
                        status_pub.publish(new_status);
                        break;
                    default:
                        // do nothing
                        ;
                }
                // new_msg.header.stamp = ros::Time::now();

                // twist_pub.publish(new_msg);
            }
        }
    }
}

int create_can_socket(void)
{
    struct sockaddr_can addr_can;
    struct ifreq ifr_can;

    /* create a raw socket for CAN */
    if((socket_can = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        printf("Error happened in creating CAN socket!\n");
        return -1;
    }

    /* find and return CAN interface, saved in ifr_can */
    //ifr_can.ifr_name = {'/0'};
    strncpy(ifr_can.ifr_name, CAN_DEV.c_str(), IFNAMSIZ-1);
    ifr_can.ifr_name[IFNAMSIZ-1] = '\0';
    //ifr_can.ifr_ifindex = if_nametoindex(ifr_can.ifr_name);
    if(ioctl(socket_can, SIOCGIFINDEX, &ifr_can) < 0)
    {
        printf("ioctl Error! errid: %d, if_nametoindex %d\n", errno, if_nametoindex(ifr_can.ifr_name));
        return -1;
    }

    addr_can.can_family = AF_CAN;
    addr_can.can_ifindex = ifr_can.ifr_ifindex;

    /* bind socket to the CAN interface */
    if(bind(socket_can, (struct sockaddr *)&addr_can, sizeof(addr_can)) < 0)
    {
        printf("bind error! id %d\n", errno);
        return -1;
    }

    return 0;
}

int main(int argc, char **argv)
{
    ros::init(argc ,argv, "can_receiver") ;
    ros::NodeHandle nh;

    bool can_simulation = true;
    if (nh.hasParam("can_simulation"))
    {
        nh.getParam("/can_simulation", can_simulation);
    }

    if(!can_simulation)
    {
        CAN_DEV.assign(CAN_DEV_REAL);
    }
    else
    {
        CAN_DEV.assign(CAN_DEV_VIRTUAL);
    }
    std::cout << "Use CAN interface " <<  CAN_DEV << std::endl;

    // twist_pub = nh.advertise<geometry_msgs::TwistStamped>("/can_infos", 100);
    twist_pub = nh.advertise<geometry_msgs::TwistStamped>("/prius/twist_cmd", 100);
    status_pub = nh.advertise<acronis_common::VehicleStatus>("/coms/telemetry", 100);
  
    if (create_can_socket() < 0)
    {
        std::cout << "Socket CAN error, exiting..." << std::endl;
        exit(-1);
    }
  
    std::cout << "Vehicle CAN receiver" << std::endl;
  
    command_data.reset();
    vehicle_status.reset();
  
    receive_from_can();
  
    ros::spin();
    return 0;
}
