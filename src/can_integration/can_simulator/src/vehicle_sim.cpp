/* Used to generate CAN frames according to control cmd topics
 * 
 */
#include <iostream>
#include <string>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <pthread.h>
#include <linux/can.h>
#include <net/if.h>

#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>
/* Headers from Autoware 1.5.1 */
#include <tablet_socket_msgs/mode_cmd.h>
#include <tablet_socket_msgs/gear_cmd.h>
/*
#include <runtime_manager/accel_cmd.h>
#include <runtime_manager/brake_cmd.h>
#include <runtime_manager/steer_cmd.h>

#include <waypoint_follower/ControlCommandStamped.h>
*/
#include <autoware_msgs/accel_cmd.h>
#include <autoware_msgs/brake_cmd.h>
#include <autoware_msgs/steer_cmd.h>
#include <autoware_msgs/ControlCommandStamped.h>

/* defined headers */
//#include <can_simulator/vehicle.h>
/* use headers in monitor to avoid circular dependency */
#include <monitor/vehicle.h>

static int socket_can;
static CommandData command_data;

static int send_single_frame(const struct can_frame frame)
{
    //socket_lock.lock();
    int nbytes = write(socket_can, &frame, sizeof(frame));
    //packet_sent++;
    /*
    if((packet_sent % 100) == 0)
    {
        printf("%d packets sent.\n", packet_sent);
    }
    socket_lock.unlock();
    */
    if (nbytes < 0)
    {
        //std::cout << boost::this_thread::get_id() << '\n';
        printf("socket sending error!\n");
        return -1;
    }
    
    //std::cout << boost::this_thread::get_id() << ": " << nbytes << " bytes sent.\n";
    return 0;
}

static int send_can_frame(const canid_t frame_id)
{
    uint8_t i = 0;
    uint8_t *bytes;
    struct can_frame frame;
    
    frame.can_dlc = MAX_DLC;
    frame.can_id = frame_id;
    
    switch(frame_id)
    {
    case TWIST_ID:
        /* 1. send linear.x */
        bytes = reinterpret_cast<uint8_t *>(&command_data.linear_x);
        memcpy(frame.data, bytes, sizeof(bytes));
        send_single_frame(frame);

        /* send angular.z */
        bytes = reinterpret_cast<uint8_t *>(&command_data.angular_z);
        memcpy(frame.data, bytes, sizeof(bytes));
        send_single_frame(frame);
        
        break;
    case MODE_ID:        
        for(i = 0; i < sizeof(command_data.modeValue); i++)
        {
            frame.data[i] = command_data.modeValue >> (i*8);
        }
        for (i = sizeof(command_data.modeValue); i < 8; i++)
        {
            frame.data[i] = 0;
        }

        send_single_frame(frame);
        break;
    case GEAR_ID:
        for(i = 0; i < sizeof(command_data.gearValue); i++)
        {
            frame.data[i] = command_data.gearValue >> (i*8);
        }
        for (i = sizeof(command_data.gearValue); i < 8; i++)
        {
            frame.data[i] = 0;
        }

        send_single_frame(frame);
        break;
    case ACCEL_ID:
        for(i = 0; i < sizeof(command_data.accellValue); i++)
        {
            frame.data[i] = command_data.accellValue >> (i*8);
        }
        for (i = sizeof(command_data.accellValue); i < 8; i++)
        {
            frame.data[i] = 0;
        }

        send_single_frame(frame);
        break;
    case STEER_ID:
        for(i = 0; i < sizeof(command_data.steerValue); i++)
        {
            frame.data[i] = command_data.steerValue >> (i*8);
        }
        for (i = sizeof(command_data.steerValue); i < 8; i++)
        {
            frame.data[i] = 0;
        }

        send_single_frame(frame);
        break;
    case BRAKE_ID:
        for(i = 0; i < sizeof(command_data.brakeValue); i++)
        {
            frame.data[i] = command_data.brakeValue >> (i*8);
        }
        for (i = sizeof(command_data.brakeValue); i < 8; i++)
        {
            frame.data[i] = 0;
        }

        send_single_frame(frame);
        break;
    case CTRL_ID:
        bytes = reinterpret_cast<uint8_t *>(&command_data.linear_velocity);
        memcpy(frame.data, bytes, sizeof(bytes));
        send_single_frame(frame);
        
        bytes = reinterpret_cast<uint8_t *>(&command_data.steering_angle);
        memcpy(frame.data, bytes, sizeof(bytes));
        send_single_frame(frame);

        break;
    default:
        break;
    }

    return 0;
}

static void twistCMDCallback(const geometry_msgs::TwistStamped& msg)
{
  command_data.linear_x = msg.twist.linear.x;
  command_data.angular_z = msg.twist.angular.z;

  send_can_frame(TWIST_ID);
}

static void modeCMDCallback(const tablet_socket_msgs::mode_cmd& mode)
{
  if(mode.mode == -1 || mode.mode == 0){
    command_data.reset();
  }

  command_data.modeValue = mode.mode;
  send_can_frame(MODE_ID);
}

static void gearCMDCallback(const tablet_socket_msgs::gear_cmd& gear)
{
  command_data.gearValue = gear.gear;

  send_can_frame(GEAR_ID);
}

static void accellCMDCallback(const autoware_msgs::accel_cmd& accell)
{
  command_data.accellValue = accell.accel;

  send_can_frame(ACCEL_ID);
}

static void steerCMDCallback(const autoware_msgs::steer_cmd& steer)
{
  command_data.steerValue = steer.steer;

  send_can_frame(STEER_ID);
}

static void brakeCMDCallback(const autoware_msgs::brake_cmd &brake)
{
  command_data.brakeValue = brake.brake;

  send_can_frame(BRAKE_ID);
}

static void ctrlCMDCallback(const autoware_msgs::ControlCommandStamped& msg)
{
  command_data.linear_velocity = msg.cmd.linear_velocity;
  command_data.steering_angle = msg.cmd.steering_angle;
  send_can_frame(CTRL_ID);

  /* generate some steer cmd */
  command_data.steerValue = command_data.steering_angle;

  if (command_data.linear_velocity > 0)
  {
      send_can_frame(ACCEL_ID);
  }
  else if (command_data.linear_velocity < 0)
  {
      send_can_frame(BRAKE_ID);
  }

  if (command_data.steerValue !=0)
  {
      send_can_frame(STEER_ID);
  }  
}

int create_can_socket(void)
{
    struct sockaddr_can addr_can;
    struct ifreq ifr_can;

    /* create a raw socket for CAN */
    if((socket_can = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        printf("Error happened in creating CAN socket!\n");
        return -1;
    }

    /* find and return CAN interface, saved in ifr_can */
    //ifr_can.ifr_name = {'/0'};
    strncpy(ifr_can.ifr_name, CAN_DEV, IFNAMSIZ-1);
    ifr_can.ifr_name[IFNAMSIZ-1] = '\0';
    //ifr_can.ifr_ifindex = if_nametoindex(ifr_can.ifr_name);
    if(ioctl(socket_can, SIOCGIFINDEX, &ifr_can) < 0)
    {
        printf("ioctl Error! errid: %d, if_nametoindex %d\n", errno, if_nametoindex(ifr_can.ifr_name));
        return -1;
    }

    addr_can.can_family = AF_CAN;
    addr_can.can_ifindex = ifr_can.ifr_ifindex;

    /* bind socket to the CAN interface */
    if(bind(socket_can, (struct sockaddr *)&addr_can, sizeof(addr_can)) < 0)
    {
        printf("bind error! id %d\n", errno);
        return -1;
    }

    return 0;
}

int main(int argc, char **argv)
{
  ros::init(argc ,argv, "vehicle_simulator") ;
  ros::NodeHandle nh;

  if (create_can_socket() < 0)
  {
      std::cout << "Socket CAN error, exiting..." << std::endl;
      exit(-1);
  }
  
  std::cout << "vehicle sender" << std::endl;
  ros::Subscriber sub[7];
  sub[0] = nh.subscribe("/twist_cmd", 1, twistCMDCallback);
  sub[1] = nh.subscribe("/mode_cmd",  1, modeCMDCallback);
  sub[2] = nh.subscribe("/gear_cmd",  1, gearCMDCallback);
  sub[3] = nh.subscribe("/accel_cmd", 1, accellCMDCallback);
  sub[4] = nh.subscribe("/steer_cmd", 1, steerCMDCallback);
  sub[5] = nh.subscribe("/brake_cmd", 1, brakeCMDCallback);
  sub[6] = nh.subscribe("/ctrl_cmd", 1, ctrlCMDCallback);

  command_data.reset();
  /*
  pthread_t th;
  if(pthread_create(&th, nullptr, receiverCaller, nullptr) != 0){
    std::perror("pthread_create");
    std::exit(1);
  }

  if (pthread_detach(th) != 0){
    std::perror("pthread_detach");
    std::exit(1);
  }
  */
  ros::spin();
  return 0;
}
