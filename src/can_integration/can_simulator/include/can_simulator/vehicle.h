/* define frame ids */
#define TWIST_ID 0x40
#define MODE_ID  0x41
#define GEAR_ID  0x42
#define ACCEL_ID 0x43
#define STEER_ID 0x44
#define BRAKE_ID 0x45
#define CTRL_ID  0x46

/* define maximum frame data length */
#define MAX_DLC  0x8

/* define cmmd data struct */
struct CommandData {
  double linear_x;
  double angular_z;
  int modeValue;
  int gearValue;
  int accellValue;
  int brakeValue;
  int steerValue;
  double linear_velocity;
  double steering_angle;

  void reset();
};

void CommandData::reset()
{
  linear_x    = 0;
  angular_z   = 0;
  modeValue   = 0;
  gearValue   = 0;
  accellValue = 0;
  brakeValue  = 0;
  steerValue  = 0;
  linear_velocity = -1;
  steering_angle = 0;
}
