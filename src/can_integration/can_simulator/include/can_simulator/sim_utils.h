#include <iostream>
#include <cmath>
#include <linux/can.h>
#include <monitor/vehicle.h>

static int send_single_frame(const struct can_frame frame);

/* big edian to little endian 
 * or reverse
 */
static uint32_t swap_byte_order(const uint32_t id)
{
    return (id >> 24) | ((id << 8) & 0x00FF0000) |
        ((id >> 8) & 0x0000FF00) | (id << 24);
}

template <class T> static int send_can_frame(const uint8_t frame_type, const uint8_t frame_id, const T &value)
{
    //uint8_t i = 0;
    //uint8_t *bytes;
    struct can_frame frame;

    union {
        uint32_t real_value;
        uint8_t bytes[sizeof(uint32_t)];
    }value_cast;
    
    
    frame.can_dlc = MAX_SP_DLC;
    if ((frame_type & VEHICLE_CANID) || (frame_type & ECU_CANID))
    {
        frame.can_id = frame_type;
    }
    
    memset(frame.data, 0, sizeof(frame.data));
    memset(value_cast.bytes, 0, sizeof(uint32_t));
    //bytes = reinterpret_cast<uint8_t *>(value);
    
    //value_cast.real_value = (int)(std::round(value) * 100);
    frame.data[0] = (uint8_t)frame_id;
    if (value < 0)
    {
        /* by multipling with 100 we could keep some decimal parts */
        T tmp = value * (-1);
        value_cast.real_value = (uint32_t)std::round(tmp * 100);
        frame.data[1] = value_cast.bytes[2] | 0x80;
        /* frame.data[2] = value_cast.bytes[1]; */
        /* frame.data[3] = value_cast.bytes[0]; */
    }
    else
    {
        value_cast.real_value = (uint32_t)std::round(value * 100);
        frame.data[1] = value_cast.bytes[2];
        /* frame.data[2] = value_cast.bytes[1]; */
        /* frame.data[3] = value_cast.bytes[0]; */
    }
    
    frame.data[2] = value_cast.bytes[1];
    frame.data[3] = value_cast.bytes[0];

    std::cout << "value is " << value << std::endl;
    std::cout << "bytes: ";
    std::cout << "0x" << std::hex << (int)value_cast.bytes[3];
    std::cout << " 0x" << std::hex << (int)value_cast.bytes[2];
    std::cout << " 0x" << std::hex << (int)value_cast.bytes[1];
    std::cout << " 0x" << std::hex << (int)value_cast.bytes[0] << std::endl;

    /* frame.data[1] = value_cast.bytes[2]; */
    /* frame.data[2] = value_cast.bytes[1]; */
    /* frame.data[3] = value_cast.bytes[0]; */
    std::cout << "frame packed." << std::endl;

    send_single_frame(frame);

    return 0;
}
