#ifndef TEST1_TEST1_H
#define TEST1_TEST1_H
#endif //TEST1_TEST1_H

#include <iostream>
#include <vector>
#include <map>
#include <chrono>
#include <string>
#include <stdlib.h>
#include <tuple>
#include <linux/can.h>

#define MAX_CAN_DATA 8

typedef std::chrono::duration<double> duration_t;
typedef std::chrono::system_clock::time_point time_stamp_t;
typedef std::vector<uint16_t>::size_type vec_size_type;

typedef struct tMsgStructure {
    canid_t canID;
    time_stamp_t time_stamp;
} MsgStructure;

struct frame_desc{
    canid_t id;
    uint8_t dlc;
    uint32_t speed;
    std::vector<uint16_t> data;
    uint8_t __paddings[3];
};

struct frame_count {
    std::map<canid_t, uint32_t> data; //can_id and its number
};

//static uint32_t swap_byte_order(const uint32_t id);

typedef struct tHistory 
{
    char propositions;
    uint32_t msgID;
    //double timestamp;
    time_stamp_t time_stamp;
} History;

/* extern int LogicDroid_Module_initializeMonitor(History * hist0, History *hist1); */

/* int LogicDroid_Module_checkEvent(uint32_t currentMsg, double timestamp, History *hist1, uint32_t checkmsg, int lowbound, int upbound); */

bool checkmsg(uint32_t msg, uint32_t * MsgRepository,std::size_t len);

History* History_Constructor(time_stamp_t timestamp);

void Current_Constructor(uint32_t msg, time_stamp_t timestamp, History *hist1);

void History_Reset(History *h);
//void History_insertEvent(History *h, int rel, int idx);
char History_Process(History *next, uint32_t CheckMsg, int lowbound, int upboudn);
//void History_Dispose(History *h);

std::tuple<MsgStructure*, MsgStructure*> ReadMsg(const u_char *pkt, time_stamp_t curr_time);
duration_t compute_duration(time_stamp_t starTime, time_stamp_t endTime);

std::vector<uint32_t> construct_msg_repository(std::string filename);

void writ_to_file(std::map<uint32_t, struct frame_count> time_logs);

void add_to_log(double secs_passed, canid_t can_ID, std::map<uint32_t, struct frame_count> *time_logs,double time_window);
