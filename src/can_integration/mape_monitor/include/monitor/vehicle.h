#include <string>
/* define frame ids (cache) */
#define TWIST_ID 0x40
#define MODE_ID  0x41
#define GEAR_ID  0x42
#define ACCEL_ID 0x43
#define STEER_ID 0x44
#define BRAKE_ID 0x45
#define CTRL_ID  0x46

/* Singpilot */
#define VEHICLE_CANID 0x21
#define ECU_CANID 0x22
#define MAX_SP_DLC 0x4

#define SPEED_CMD_ID 0x61
#define AXIS_Z_ID 0x62
#define STEER_SPEED_ID 0x63
#define STEER_ANGLE_ID 0x64
#define ACCEL_CMD_ID 0x65
#define BRAKE_POS_ID 0x66

/* socketcan */
//#define SIMULATOR true

//#ifdef SIMULATOR
#define CAN_DEV_VIRTUAL "vcan0"
//#else
#define CAN_DEV_REAL "can0"
//#endif

static std::string CAN_DEV;

/* define maximum frame data length */
#define MAX_DLC  0x8

/* define cmmd data struct */
struct CommandData {
    double linear_x;
    double angular_z;
    int modeValue;
    int gearValue;
    int accellValue;
    int brakeValue;
    int steerValue;
    double linear_velocity;
    double steering_angle;

    void reset();
};

struct VehicleStatus {
    double linear_x;
    double angular_z;
    double accel_pedal_pos; // for accel_pedal_postition
    double brake_pos; // for break_position
    double steer_wheel_pos; // for steering_wheel_position
    double steering_wheel_sp; // for steering_wheel_speed

    void reset();
};

void CommandData::reset()
{
  linear_x    = 0;
  angular_z   = 0;
  modeValue   = 0;
  gearValue   = 0;
  accellValue = 0;
  brakeValue  = 0;
  steerValue  = 0;
  linear_velocity = -1;
  steering_angle = 0;
}

void VehicleStatus::reset()
{
    linear_x = 0;
    angular_z = 0;
    accel_pedal_pos = 0;
    brake_pos = 0;
    steer_wheel_pos = 0;
    steering_wheel_sp = 0;
}
