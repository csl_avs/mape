#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>
#include <inttypes.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <map>

#include <acronis_common/Processes.h>
#include <acronis_common/Uint32_Long64.h>

#include <libvmi/libvmi.h>

typedef std::map<uint32_t, uint64_t> proclist_t;

int get_proc_list(vmi_instance_t &vmi, unsigned long &tasks_offset,
                   unsigned long &name_offset, unsigned long &pid_offset,
                   proclist_t &proc_list)
{
    //vmi_instance_t vmi;
    addr_t list_head = 0, cur_list_entry = 0, next_list_entry = 0;
    addr_t current_process = 0;
    char *procname = NULL;
    vmi_pid_t pid = 0;
    //unsigned long tasks_offset = 0, pid_offset = 0, name_offset = 0;
    status_t status;

    /* pause the vm for consistent memory access */
    if (vmi_pause_vm(vmi) != VMI_SUCCESS) {
        printf("Failed to pause VM\n");
        return -1;
    } // if

    /* demonstrate name and id accessors */
    char *name2 = vmi_get_name(vmi);
    vmi_mode_t mode;

    if (VMI_FAILURE == vmi_get_access_mode(vmi, NULL, 0, NULL, &mode))
        return -1;

    if ( VMI_FILE != mode ) {
        uint64_t id = vmi_get_vmid(vmi);

        printf( "Process listing for VM %s (id=%"PRIu64")\n", name2, id);
    } else {
        printf( "Process listing for file %s\n", name2);
    }
    free(name2);

    os_t os = vmi_get_ostype(vmi);

    /* get the head of the list */
    if (VMI_OS_LINUX == os) {
        /* Begin at PID 0, the 'swapper' task. It's not typically shown by OS
         *  utilities, but it is indeed part of the task list and useful to
         *  display as such.
         */
        if ( VMI_FAILURE == vmi_translate_ksym2v(vmi, "init_task", &list_head) )
            return -1;

        list_head += tasks_offset;
    }

    cur_list_entry = list_head;
    if (VMI_FAILURE == vmi_read_addr_va(vmi, cur_list_entry, 0, &next_list_entry)) {
        printf("Failed to read next pointer in loop at %"PRIx64"\n", cur_list_entry);
        return -1;
    }

    /* walk the task list */
    while (1) {

        current_process = cur_list_entry - tasks_offset;

        /* Note: the task_struct that we are looking at has a lot of
         * information.  However, the process name and id are burried
         * nice and deep. 
		 * Jumping to the location with the info that we want.  
		 * See include/linux/sched.h for mode details 
		 */

        /* get 32-bit pid */
        vmi_read_32_va(vmi, current_process + pid_offset, 0, (uint32_t*)&pid);

        procname = vmi_read_str_va(vmi, current_process + name_offset, 0);

        if (!procname) {
            printf("Failed to find procname\n");
            return -1;
        }
        /* put current process into our dict */
        proc_list[pid] = (uint64_t)current_process;

        /* debug: print out the process name */
        printf("[%5d] %s (struct addr:%"PRIx64")\n", pid, procname, current_process);
        if (procname) {
            free(procname);
            procname = NULL;
        }
        
		/* follow the next pointer */
        cur_list_entry = next_list_entry;
        status = vmi_read_addr_va(vmi, cur_list_entry, 0, &next_list_entry);
        if (status == VMI_FAILURE) {
            printf("Failed to read next pointer in loop at %"PRIx64"\n", cur_list_entry);
            return -1;
        }

        if (VMI_OS_LINUX == os && cur_list_entry == list_head) {
            break;
        }
    };

    /* resume the vm */
    vmi_resume_vm(vmi);
    return 0;
    /* cleanup any memory associated with the LibVMI instance */
    // vmi_destroy(vmi);
}


int monitor_proc_list(const char *partition, const ros::Publisher &pb)
{
    /* store process info */
    proclist_t proc_list;

    /* accessing libvmi */
    vmi_instance_t vmi;
    unsigned long tasks_offset = 0, pid_offset = 0, name_offset = 0;

    char *name = strdup(partition);
    
    /* initialize the libvmi library */
    if (VMI_FAILURE ==
            vmi_init_complete(&vmi, name, VMI_INIT_DOMAINNAME, NULL,
                              VMI_CONFIG_GLOBAL_FILE_ENTRY, NULL, NULL)) {
        printf("Failed to init LibVMI library.\n");
        return 1;
    }
    free(name);

    /* init the offset values */
    if (VMI_OS_LINUX == vmi_get_ostype(vmi)) {
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_tasks", &tasks_offset) )
            goto err_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_name", &name_offset) )
            goto err_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_pid", &pid_offset) )
            goto err_exit;
    }

    while(ros::ok())
    {
        /* get proc_list updated */
        if (get_proc_list(vmi, tasks_offset, name_offset, pid_offset, proc_list) < 0)
        {
            goto err_exit;
        }

        if (!proc_list.empty())
        {
            /* TODO: 1. publish process list */
            acronis_common::Processes msg;
            for (auto iter = proc_list.begin(); iter != proc_list.end(); iter++)
            {
                acronis_common::Uint32_Long64 item;
                item.pid = iter->first;
                item.addr = iter->second;

                msg.list.push_back(item);
            }
            msg.header.frame_id = std::string(partition);
            msg.header.stamp = ros::Time::now();
            pb.publish(msg);
            
            /* 2. clear its content */
            proc_list.clear();
            msg.list.clear();
        }

        /* sleep for 10s */
        ros::Duration(10).sleep();
        // std::this_thread::sleep_for(std::chrono::seconds(10));
    }
    
err_exit:
    vmi_resume_vm(vmi);
    vmi_destroy(vmi);
    return 0;
}

int main (int argc, char **argv)
{
    /* ROS */
    /* use remapping in roslaunch to change node name */
    ros::init(argc, argv, "proc_monitor");
    ros::NodeHandle nh;

    if (!nh.hasParam("partition"))
    {
        ROS_INFO("Partition name should be provided!");
        return 1;
    }
    std::string partition;
    nh.getParam("partition", partition);

    ros::Publisher pl_pub = nh.advertise<acronis_common::Processes>("partition_process"+partition, 1);

    monitor_proc_list(partition.c_str(), pl_pub);
    
    return 0;
}
