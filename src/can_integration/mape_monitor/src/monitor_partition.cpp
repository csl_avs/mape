#include <ros/ros.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include <iostream>
#include <sstream>
#include <acronis_common/PartitionStats.h>
//#include <string>

#ifndef DEBUG
#define DEBUG
#endif


/* [in]: data
 * [out]: ps
 * parsing data into ps
 */
void unserialize_data(const char *data, acronis_common::PartitionStats &ps)
{
    std::string input(data);
    std::istringstream ss(input);
    std::string item;

    while(std::getline(ss, item, ','))
    {
        size_t pos = item.find(':');
        std::string key = item.substr(0, pos);
        std::string value(item.substr(pos+1));
        
        if(key.find("num_processes") != std::string::npos){
            ps.num_processes = (uint32_t)stoul(value);
        }else if (key.find("mem_total") != std::string::npos){
            ps.mem_total = stod(value);
        }else if (key.find("mem_available") != std::string::npos){
            ps.mem_avalable = stod(value);
        }else if (key.find("partition") != std::string::npos){
            value.erase(0, 2);// remove " \""
            value.erase(value.size() - 1); // remove "\""
            ps.partition_addr = value;
        }else if (key.find("cpu_percent") != std::string::npos){
            ps.cpu_usage = stof(value);
        }else{
            std::cout << "unknown key!" << std::endl;
        }
        
        
    }
    std::cout<< ps << std::endl;
}

int main(int argc, char *argv[])
{
    int sock;
    int addr_len, bytes_read;
    char recv_data[1024];
    struct sockaddr_in server_addr , client_addr;
    std::string ip_addr("155.69.149.174");

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("Socket error\n");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(5566);
    //server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_addr.s_addr = inet_addr("155.69.149.174");
    bzero(&(server_addr.sin_zero),8);


    if (bind(sock,(struct sockaddr *)&server_addr,
             sizeof(struct sockaddr)) == -1)
    {
        perror("Bind error\n");
        exit(1);
    }

    addr_len = sizeof(struct sockaddr);

    ros::init(argc, argv, "MAPE_m_partition");
    ros::NodeHandle nh;

    ros::Publisher ps_pub = nh.advertise<acronis_common::PartitionStats>("parition_status", 1);
    
#ifdef DEBUG
	printf("UDPServer Waiting for client on port 5566");
    fflush(stdout);
#endif

	while (ros::ok())
	{
        bytes_read = recvfrom(sock,recv_data,1024,0,
                              (struct sockaddr *)&client_addr, (socklen_t *)&addr_len);
	  
        recv_data[bytes_read] = '\0';
        
#ifdef DEBUG
        printf("(%s , %d) said : ",inet_ntoa(client_addr.sin_addr),
               ntohs(client_addr.sin_port));
        printf("%s\n", recv_data);
        fflush(stdout);
#endif
        acronis_common::PartitionStats ps;
        unserialize_data(recv_data, ps);

        ps_pub.publish(ps);

        ros::spinOnce();
    }
    
    return 0;
}
