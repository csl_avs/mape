
#include <iostream>
#include <math.h>
#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>
/* #include <Eigen/Eigen> is deprecated */
#include <eigen3/Eigen/Eigen>
#include <std_msgs/String.h>

// #include <mape_msg/BadNodes.h>
#include <acronis_common/BadNodes.h>

#ifndef __DIS_LIMIT__
#define __DIS_LIMIT__  0.2
#endif

#define GPS_NODE "nmea2fix" 
#define MAX_SUSPECT_COUNT 50
#define MAX_POINT_COUNT 100 
static unsigned int suspect_count;
static unsigned int point_count;
static geometry_msgs::Point old_pose;

static ros::Publisher gnss_pub;
static ros::Time old_imu_stamp;
static ros::Time old_fcu_stamp;

static bool is_new(const geometry_msgs::Point pose)
{
    if (pose.x != old_pose.x || pose.y != old_pose.y
        || pose.z != old_pose.z)
    {
        return true;
    }
    
    return false;
}

int *imu_callback(const sensor_msgs::ImuConstPtr &msg)
{
    if (msg->header.stamp < old_imu_stamp)
    {
        ROS_DEBUG("Recieved old imu msg");
        return ~0
    }
    
    old_imu_stamp = msg->header.stamp;
    
    /* calculate position */
    Eigen::Quaternion orientation(msg->orientation.w, msg->orientation.x, msg->orientation.y,
                                  msg->orientation.z);

    orientation *= 
}

int *fcu_gps_callback(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg)
{
    if (msg->header.stamp < old_fcu_stamp)
    {
        ROS_DEBUG("REcieved old fcu_gps_pose msg");
        return ~0;
    }

    old_fcu_stamp = msg->header.stamp;

    int pose_x, pose_y, pose_z;

    pose_x = msg->pose.pose.position.x;
    pose_y = msg->pose.pose.position.y;
    pose_z = msg->pose.pose.position.z;

    
}

int *gnss_pose_monitor(const geometry_msgs::PoseStampedConstPtr &msg)
{
    geometry_msgs::Point new_pose;
    
    new_pose.x = msg->pose.position.x;
    new_pose.y = msg->pose.position.y;
    new_pose.z = msg->pose.position.z;
    
    float distance = 0;
    if is_new(new_pose)
    {
        /* calculate distance in 2D */
        distance = pow((new_pose.x - old_pose.x), 2)
            + pow((new_pose.y - old_pose.y), 2);
        point_count++;
        /* record new point */
        old_pose.x = new_pose.x;
        old_pose.y = new_pose.y;
        old_pose.z = new_pose.z;
    }

    if (distance > __DIS_LIMIT__){
        /* distance changes too quickly? */
        suspect_count++;
    }
    
    if (point_count >= 100)
    {
        if (suspect_count >= MAX_SUSPECT_COUNT)
        {
            /* TODO: report gps error */
            // mape_msg::BadNodes msg;
            acronis_common::BadNodes msg;
            std_msgs::String node_name;
            
            /* publish msg */
            if (gnss_pub.getNumSubscribers() > 0)
            {
                msg.patients.push_back(GPS_NODE);
                gnss_pub.publish(msg);
            }
            
            /* clear statistics */
            suspect_count = 0;
            point_count = 0;
        }
    }
}

int main(int args, char **argv)
{
    ros::init(args, argv, "gps_monitor", ros::init_options::AnonymousName);
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe("/gnss_pose", 1000, gnss_pose_monitor);
    ros::Subscriber imu_sub  = nh.subscribe("imu", 1, imu_callback);
    ros::Subscriber fcu_sub = nh.subscribe("fcu/gps_pose", 1, fcu_gps_callback);
    // gnss_pub = nh.advertise<mape_msg::BadNodes>("/monitor_gps", 100);
    gnss_pub = nh.advertise<acronis_common::BadNodes>("/monitor_gps", 100);

    ros::spin();
}
