/* monitor control cmd to get running status of the vehicle */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <vector>
#include <deque>
#include <map>
#include <chrono>
#include <cstdint>
#include <mutex>

#include <boost/thread.hpp>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include <ros/ros.h>

#include <autoware_msgs/accel_cmd.h>
#include <autoware_msgs/brake_cmd.h>
#include <autoware_msgs/steer_cmd.h>
#include <autoware_msgs/ControlCommandStamped.h>
#include <geometry_msgs/TwistStamped.h>

#include <monitor/vehicle.h>
// #include <mape_msg/SWMsg.h>
#include <acronis_common/SWMsg.h>

#ifndef __NOW
#define __NOW std::chrono::system_clock::now()
#endif

typedef struct
{
    uint32_t can_id;
    std::chrono::system_clock::time_point time_stamp;
}frame_desc_t;

static std::deque<frame_desc_t> cc_cache;
static boost::mutex cc_lock;
//static std::mutex cc_lock;

/* get a ctrl cmd and push it to the cache */
static void ctrl_cmd_callback(const autoware_msgs::ControlCommandConstPtr &ptr)
{
    frame_desc_t new_frame;
    new_frame.can_id = CTRL_ID;
    new_frame.time_stamp = __NOW;

    cc_lock.lock();
    cc_cache.push_back(new_frame);
    
    if (ptr->linear_velocity > 0)
    {
        new_frame.can_id = ACCEL_ID;    
    }
    else
    {
        new_frame.can_id = BRAKE_ID;
    }
    cc_cache.push_back(new_frame);

    if (ptr->steering_angle !=0)
    {
        new_frame.can_id = STEER_ID;
    }

    //cc_lock.lock();
    cc_cache.push_back(new_frame);
    
    cc_lock.unlock();
}

static void accel_cmd_callback(const autoware_msgs::accel_cmdConstPtr &ptr)
{
    frame_desc_t new_frame;
    new_frame.can_id = ACCEL_ID;
    new_frame.time_stamp == __NOW;

    cc_lock.lock();
    cc_cache.push_back(new_frame);
    cc_lock.unlock();
}

static void brake_cmd_callback(const autoware_msgs::brake_cmdConstPtr &ptr)
{
    frame_desc_t new_frame;
    new_frame.can_id = BRAKE_ID;
    new_frame.time_stamp == __NOW;

    cc_lock.lock();
    cc_cache.push_back(new_frame);
    cc_lock.unlock();
}

static void steer_cmd_callback(const autoware_msgs::accel_cmdConstPtr &ptr)
{
    frame_desc_t new_frame;
    new_frame.can_id = STEER_ID;
    new_frame.time_stamp == __NOW;

    cc_lock.lock();
    cc_cache.push_back(new_frame);
    cc_lock.unlock();
}

static void twist_cmd_callback(const geometry_msgs::TwistStampedPtr &ptr)
{
    frame_desc_t new_frame;
    new_frame.can_id = SPEED_CMD_ID;
    new_frame.time_stamp == __NOW;

    cc_lock.lock();
    cc_cache.push_back(new_frame);

    new_frame.can_id = AXIS_Z_ID;
    cc_cache.push_back(new_frame);

    cc_lock.unlock();

    std::cout << "cache size: " << cc_cache.size() << std::endl;
}

// static void can_filter_callback(const mape_msg::SWMsgConstPtr &ptr)
// {
//     cc_lock.lock();

//     if (cc_cache.empty())
//     {
//         ROS_INFO("[Alert!] CAN frame cache is empty!\n");
//     }

//     if (cc_cache.front().can_id == (uint32_t)ptr->msg_id)
//     {
//         cc_cache.pop_front();
//     }
//     else
//     {
//         // if we cannot find a match, then something might be wrong
//         ROS_INFO("[Suspicious!] Cannot locate the corresponding CAN frame in the cache.\n");
//     }
    
//     cc_lock.unlock();
// }

static uint32_t swap_byte_order(const uint32_t id)
{
    return (id >> 24) | ((id << 8) & 0x00FF0000) |
        ((id >> 8) & 0x0000FF00) | (id << 24);
}

static void check_cc_can(void)
{
    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler
    //float window_size;

    /* open can bus interface for frame capturing */
    //first_packet = false;
    hl = pcap_open_live(CAN_DEV.c_str(), BUFSIZ, 1, 1000, err_buff);
    if (hl == NULL)
    {
        /* open failed */
        ROS_DEBUG("Couldn't open device %s!\n", CAN_DEV.c_str());
        return;
    }
    
    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        /* data is not a socketCAN frame */
        ROS_DEBUG("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return;
    }

    std::cout << CAN_DEV << " opened successfully!" << std::endl;

    struct can_frame *can_f;
    frame_desc_t new_frame;
    new_frame.can_id = 0;
    
    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        //cur_time = __NOW;
        can_f = (struct can_frame *)pkt;

        if (swap_byte_order(can_f->can_id) == VEHICLE_CANID)
        {
            //cc_lock.lock();
            //std::cout << "cc_cache size: " << cc_cache.size() << std::endl; 
            if (cc_cache.empty())
            {
                ROS_INFO("[Alert!] CAN frame cache is empty!\n");
            }
            else
            {
                //cc_lock.lock();
                /* front() is thread-safe? */
                new_frame = cc_cache.front();
                //cc_lock.unlock();

                if (new_frame.can_id == can_f->data[0])
                {
                    cc_lock.lock();
                    cc_cache.pop_front();
                    std::cout << "cc_cache size: " << cc_cache.size() << std::endl;
                    cc_lock.unlock();
                }
                else{
                    ROS_INFO("[Suspicious!] Cannot locate the corresponding CAN frame in the cache.\n");
                }
            }
        }
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "CtrlCmd_monitor");
    ros::NodeHandle nh;

    bool can_simulation = true;
    if (nh.hasParam("can_simulation"))
    {
        nh.getParam("/can_simulation", can_simulation);
    }

    if(!can_simulation)
    {
        CAN_DEV.assign(CAN_DEV_REAL);
    }
    else
    {
        CAN_DEV.assign(CAN_DEV_VIRTUAL);
    }
    std::cout << "Use CAN interface " <<  CAN_DEV << std::endl;

    // ros::Subscriber nh_accel = nh.subscribe("/accel_cmd", 1, accel_cmd_callback);
    // ros::Subscriber nh_brake = nh.subscribe("/brake_cmd", 1, brake_cmd_callback);
    // ros::Subscriber nh_steer = nh.subscribe("/steer_cmd", 1, steer_cmd_callback);
    // ros::Subscriber nh_ctrl = nh.subscribe("/ctrl_cmd", 1, ctrl_cmd_callback);

    /* listen to twist_cmd */
    ros::Subscriber nh_twist = nh.subscribe("/twist_cmd", 1, twist_cmd_callback);
    
    //ros::Subscriber nh_can = nh.subscribe("/CAN_filter", 1, can_filter_callback);

    /* use a dedicated thread to check cc_cache */
    boost::thread *checker = new boost::thread(check_cc_can);
    //checker->join();
    //checker->start_thread();
    
    ros::spin();
    return 0;
}
