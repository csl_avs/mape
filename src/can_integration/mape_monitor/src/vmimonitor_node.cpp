#include <string>
#include <thread>
#include <ros/ros.h>
#include <monitor/libvmimonitor.h>
#include <monitor/syscalls.h>
// message
#include <acronis_common/VMIMonitor.h>

void publish_res(ros::Publisher *pub, std::string dom_name)
{
    syscall_info_t res;
    while(!interrupted)
    {
        if(pop(&res))
        {
            std::cout << "pop...";
        }
        
        acronis_common::VMIMonitor item;
        item.header.stamp = ros::Time::now();
        //item.header.frame_id = "vmimonitor";
        item.header.frame_id = dom_name;

        item.time.sec = res.timestamp.tv_sec;
        item.time.nsec = res.timestamp.tv_usec * 1000;
        item.vcpu = res.vcpu;
        item.cr3 = res.cr3;
        item.proc_name = std::string(res.proc_name);
        item.pid = res.pid;
        item.syscall = std::string(res.syscall);
        // item.nr_syscall = (uint32_t)res.nr_syscall;
        // printf("%ld\n", res.nr_syscall);
        // item.nr_syscall = res.nr_syscall;
        /* in case of new syscall handling scheme using struct pt_regs,
         * now we refer to a syscall table to find the syscall_NR
         */
#ifdef NEW_GUEST_KERNEL
        item.nr_syscall = syscall_name_nr(item.syscall);
#else
        item.nr_syscall = (uint32_t)res.nr_syscall;
#endif

        item.ppid = res.ppid;
        item.base_addr = res.base_addr;
        
        pub->publish(item);
    }    
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "vmimonitor_node");
    ros::NodeHandle nh;
    ros::NodeHandle p_nh("~");
    std::string dom_name;
    
    if(p_nh.hasParam("domain_name"))
    {
        p_nh.getParam("domain_name", dom_name);
    }
    else
    {
        ROS_INFO("No partition name specified!\n");
        exit(1);
    }

    ros::Publisher nh_pub1 = nh.advertise<acronis_common::VMIMonitor>("VMI_monitor", 1000);

    std::thread vmipub_thd(publish_res, &nh_pub1, dom_name);
    vmipub_thd.detach();

    start_vmimonitor(dom_name.c_str());

    /* no need to spin */
    return 0;
}
