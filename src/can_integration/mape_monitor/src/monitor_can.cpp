/* monitor CAN bus communication */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <vector>
#include <deque>
#include <map>
#include <tuple>
#include <chrono>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <ros/ros.h>
/*
#include <mape_msg/CANFreq.h>
#include <mape_msg/SWCache.h>
#include <mape_msg/SWMsg.h>
*/
#include <acronis_common/CANFreq.h>
#include <acronis_common/SWCache.h>
#include <acronis_common/SWMsg.h>

#include <monitor/monitor.h>
#include <monitor/vehicle.h>

// #define DEBUG true
// #define SIMULATOR true

// #ifdef SIMULATOR
// #define CAN_DEV "vcan0"
// #else
// #define CAN_DEV "can0"
// #endif

#define MAX_PERIOD_PACKET 200
#define DEFAULT_SW_SIZE 10 // 10 seconds

// define sliding window type sw_t
typedef struct
{
    time_stamp_t begin;
    time_stamp_t end;
    uint32_t counts;
}sw_t;

typedef std::map<canid_t, double> freq_t;
typedef std::map<canid_t, uint> can_num_t;
//typedef std::chrono::duration<double> duration_t;
//typedef std::chrono::microseconds duration_t;
//typedef std::chrono::system_clock::time_point time_stamp_t;
//static std::map<canid_t, unsigned int> freq_history;

/* frame cache */
static std::deque<MsgStructure> sw_cache;

/* capture data log */
static can_num_t pkt_history, pkt_period;
static freq_t freq_history, freq_period;
static duration_t time_passed, period_time;
static time_stamp_t time_start, period_start;
static bool first_packet;

static int default_sw_size;

/* for ROS communication */
static ros::Publisher nh_pub1, nh_pub2, nh_pub3, nh_pub4;

/* Description:
 * print can frame id in hex little edian format
 */
void print_can_id(const canid_t can_id)
{
    for (int i = 0; i <= 24; i+=8)
    {
        if(((can_id >> i) & 0xFF) != 0)
        {
            printf("%02X", (can_id >> i) & 0xFF);
        }
    }
    printf("h");
}

/* big edian to little endian 
 * or little to big 
 */
static uint32_t swap_byte_order(const uint32_t id)
{
    return (id >> 24) | ((id << 8) & 0x00FF0000) |
        ((id >> 8) & 0x0000FF00) | (id << 24);
}

/* Description:
 * update and publish frequency count of history statics
 */
static void update_freq_history(void)
{
    /* 
     * define a iterator to parse frame count 
     * key: first, value: second
     */
    std::map<canid_t, uint>::iterator iter;
    // mape_msg::CANFreq msg;
    // mape_msg::Uint32_Float64 item;
    acronis_common::CANFreq msg;
    acronis_common::Uint32_Float64 item;
  
#ifdef DEBUG
    //printf("Dump history frequency\n");
    std::cout << "Dump history frequency" << std::endl;
#endif
    uint32_t id;
    for(iter = pkt_history.begin(); iter != pkt_history.end(); iter++)
    {
        // id = swap_byte_order(iter->first);
        // swap has been done when it was added.
        id = iter->first;
        freq_history[id] = ( iter->second / time_passed.count());
        /* build a new message */
        item.key = id;
        item.value = freq_history[id];
        
        msg.data.push_back(item);
        
#ifdef DEBUG
        printf("ID: ");
        print_can_id(iter->first);
        printf(", FREQ: %lf\n", freq_history[iter->first]);
#endif
    }

    /* publish message */
    ROS_DEBUG("history subscribers: %d", nh_pub1.getNumSubscribers());
    nh_pub1.publish(msg);
}

/* Description:
 * This function update and publish frequency count of current period
 */
static void update_freq_period(void)
{
    std::map<canid_t, uint>::iterator iter;
    // mape_msg::CANFreq msg;
    // mape_msg::Uint32_Float64 item;
    acronis_common::CANFreq msg;
    acronis_common::Uint32_Float64 item;
 
#ifdef DEBUG
    printf("Dump period frequency\n");
#endif
    uint32_t id = 0;
    for (iter = pkt_period.begin(); iter != pkt_period.end(); iter++)
    {
        /* update frequency for period count */
        // id = swap_byte_order(iter->first);
        // swap has been done when it was added.
        id = iter->first;
        freq_period[id] = ( iter->second / period_time.count());
        /* build message */
        item.key = id;
        item.value = freq_period[id];
        msg.data.push_back(item);
        
#ifdef DEBUG
        printf("ID: ");
        print_can_id(iter->first);
        printf(", FREQ: %lf\n", freq_period[iter->first]);
#endif
    }

    /* publish message */
    ROS_DEBUG("period subscribers: %d", nh_pub2.getNumSubscribers());
    nh_pub2.publish(msg);
    /* clear current period data */
    pkt_period.clear();
}

static void publish_sw_cache(time_stamp_t sw_begin, duration_t sw_size)
{
    // mape_msg::SWCache data;
    acronis_common::SWCache data;
    std::map<canid_t, uint32_t> id_stats;
    auto iter = sw_cache.begin();

    /* statistical analysis of frames in the current sliding window */
    while (iter != sw_cache.end())
    {
        
        if (id_stats.find(iter->canID) == id_stats.end())
        {
            id_stats[iter->canID] = 0;
        }
        else
        {
            id_stats[iter->canID]++;
        }

        if (iter->time_stamp >= (sw_begin + sw_size))
        {
            break;
        }
        
        iter++;
    }

    /* pack frame status into messages for publishing */
    for(auto iter1 = id_stats.begin(); iter1 != id_stats.end(); iter1++)
    {
        //mape_msg::SWMsg new_msg;
        acronis_common::SWMsg new_msg;
        new_msg.msg_id = iter1->first;
        new_msg.msg_num = iter1->second;
        data.can_frames.push_back(new_msg);
        //data.push_back(new_msg);
    }

    nh_pub3.publish(data);
}

static void check_can_id(const canid_t can_id)
{
    if (can_id & CAN_ERR_FLAG)
    {
        //printf("ID: %d ", can_f->can_id & (CAN_ERR_MASK|CAN_ERR_FLAG));
        print_can_id(can_id & (CAN_ERR_MASK|CAN_ERR_FLAG));
    }
    else if (can_id & CAN_EFF_FLAG)
    {
        //printf("ID %d ", can_f->can_id & CAN_EFF_MASK);
        print_can_id(can_id & CAN_EFF_MASK);
    }
    else if (can_id & CAN_RTR_FLAG)
    {
        print_can_id(can_id);
    }
    else
    {
        //printf ("ID %d ", can_f->can_id & CAN_SFF_MASK);
        print_can_id(can_id & CAN_SFF_MASK);
    }
}

/* [DEPRECATED] NEED FIXING!
 * filter can frames to be checked by topic cache
 * for behavior monitoring
 * see can_filter_simple.cpp
 */
static void capture_filter_can(std::tuple<MsgStructure*, MsgStructure*> msg)
{
    switch(std::get<0>(msg)->canID)
    {
        case VEHICLE_CANID:
        {
            acronis_common::SWMsg new_msg;
            new_msg.msg_id = VEHICLE_CANID;
            new_msg.msg_num = 0;
            nh_pub4.publish(new_msg);
            break;
        }
        default:
            ;
    }
    
    switch(std::get<1>(msg)->canID)
    {
        case ACCEL_ID:
        case STEER_ID:
        case BRAKE_ID:
        case TWIST_ID:
        case CTRL_ID:
        {
            // mape_msg::SWMsg new_msg;
            acronis_common::SWMsg new_msg;
            new_msg.msg_id = CTRL_ID;
            new_msg.msg_num = 0;
            nh_pub4.publish(new_msg);
            break;
        }
        default:
            break;
    }

}

/* Description:
 * capture can frames with libpcap and update data loggings
 */
void can_frame_capturer(void)
{
    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler
    //float window_size;

    /* open can bus interface for frame capturing */
    first_packet = false;
    hl = pcap_open_live(CAN_DEV.c_str(), BUFSIZ, 1, 1000, err_buff);
    if (hl == NULL)
    {
        /* open failed */
        ROS_DEBUG("Couldn't open device %s!\n", CAN_DEV.c_str());
        return;
    }
    
    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        /* data is not a socketCAN frame */
        ROS_DEBUG("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return;
    }

    std::cout << CAN_DEV << " opened successfully!" << std::endl;

    struct can_frame *can_f;
    int dlc;
    int num_pkt = 0;
    //MsgStructure* cur_msg = (MsgStructure*)malloc(sizeof(MsgStructure));
    time_stamp_t cur_time;
    // std::vector<canid_t> id_list;
    // std::map<canid_t, sw_t> sw_frames;

    /* define a 10-second sliding window */
    // std::chrono::duration<int, std::ratio<1,1000>> sw_size(10);
    std::cout << "windows size is " << default_sw_size <<"-second" << std::endl;
    std::chrono::milliseconds sw_size(default_sw_size * 1000); 
    time_stamp_t sw_begin, sw_end;
    //double sw_begin, sw_end; //relative time stamps
    
    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        cur_time = std::chrono::system_clock::now();
        
        /* record the time stamp when we received the first frame */
        if (num_pkt == 0) // period start?
        {
            period_start = cur_time;
        }
        if (!first_packet) // capture start?
        {
            time_start = cur_time;
            sw_begin = time_start;
            first_packet = true;
        }
        can_f = (struct can_frame *)pkt;

        auto cur_msg = ReadMsg(pkt, cur_time);
        capture_filter_can(cur_msg);
        sw_end = cur_time;
        
        sw_cache.push_back(*(std::get<0>(cur_msg)));
        sw_cache.push_back(*(std::get<1>(cur_msg)));
        
        // if (sw_frames.find(can_f->can_id) != sw_frames.end())
        // {
        //     //sw_frames[can_f->can_id] = cur_time;
        //     sw_frames[can_f->can_id].counts++;
        // if ((sw_end - sw_begin) < sw_size)
        // {
        //     //sw_frames[can_f->can_id].end = cur_time;
        //     //sw_cache.push_back(cur_msg);
        //     sw_end = cur_time;
        // }
        if ((sw_end - sw_begin) >= sw_size)
        {
            /* send data in the current sliding window first */
#ifdef DEBUG
            std::cout << "sw begin: " << std::chrono::system_clock::to_time_t(sw_begin) << ", end: " << std::chrono::system_clock::to_time_t(sw_end) << std::endl;
#endif
            publish_sw_cache(sw_begin, sw_size);
            
            /* then clear the old for new accounting */
            sw_cache.pop_front();
            /* update the first frame of the current sw */
            sw_begin = sw_cache.front().time_stamp;
        }
            
            //}
        /* count how many frames received */
        num_pkt++;
        /* check can frame type 
        check_can_id(can_f->can_id);
        */
        
        /* update can_id's frame count */
        pkt_history[swap_byte_order(can_f->can_id)] += 1;
        pkt_period[swap_byte_order(can_f->can_id)] += 1;
        // include sub id
        // sprintf("can_f->data[0] is %", )
        // std::cout << "data[0] is " << (uint32_t)can_f->data[0] << std::endl;
        pkt_history[(uint32_t)can_f->data[0]] += 1;
        pkt_period[(uint32_t)can_f->data[0]] += 1;
        
        
#ifdef DEBUG
        print_can_id(can_f->can_id);

        /* get data length and print data segment */
        dlc = (can_f->can_dlc > 8)?8: can_f->can_dlc;
        ROS_DEBUG(", DATA ");
        for (int i = 0; i < dlc; i++)
        {
            ROS_DEBUG("%02X ", can_f->data[i]);
        }
        ROS_DEBUG(", Count %d\r\n", pkt_history[can_f->can_id]);
#endif
        
        /* if MAX_PERIOD_PACKET frames are received then update the history frequency */
        if (num_pkt == MAX_PERIOD_PACKET)
        {
            num_pkt = 0;
            // time_passed = std::chrono::system_clock::now() - time_start;
            // period_time = std::chrono::system_clock::now() - period_start;
            time_passed = cur_time - time_start;
            period_time = cur_time - period_start;
            
            update_freq_history();
            update_freq_period();
        }
    }
}

int main(int argc, char **argv)
{
    /* initial CAN bus monitor node */
    ros::init(argc, argv, "CAN_monitor");
    ros::NodeHandle nh;

    bool can_simulation = true;
    default_sw_size = DEFAULT_SW_SIZE; 
    if (nh.hasParam("can_simulation"))
    {
        nh.getParam("/can_simulation", can_simulation);
    }

    if (nh.hasParam("sw_size"))
    {
        nh.getParam("/sw_size", default_sw_size);
    }

    if(!can_simulation)
    {
        CAN_DEV.assign(CAN_DEV_REAL);
    }
    else
    {
        CAN_DEV.assign(CAN_DEV_VIRTUAL);
    }
    std::cout << "Use CAN interface " <<  CAN_DEV << std::endl;
    // nh_pub1 = nh.advertise<mape_msg::CANFreq>("CAN_Freq_history", 1000);
    // nh_pub2 = nh.advertise<mape_msg::CANFreq>("CAN_Freq_period", 1000);
    // nh_pub3 = nh.advertise<mape_msg::SWCache>("CAN_SW", 1000);
    // nh_pub4 = nh.advertise<mape_msg::SWMsg>("CAN_filter", 1000);
    nh_pub1 = nh.advertise<acronis_common::CANFreq>("CAN_Freq_history", 1000);
    nh_pub2 = nh.advertise<acronis_common::CANFreq>("CAN_Freq_period", 1000);
    nh_pub3 = nh.advertise<acronis_common::SWCache>("CAN_SW", 1000);
    nh_pub4 = nh.advertise<acronis_common::SWMsg>("CAN_filter", 1000);

    /* start capturing */
    can_frame_capturer();
    
    /* maybe spin() is not necessary as there's an infinite loop? */
    ros::spin();
    return 0;
}
