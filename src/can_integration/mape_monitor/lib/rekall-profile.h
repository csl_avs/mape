#ifdef __cplusplus
extern "C" {
#endif 

//#ifndef WIN_SYMBOLS_H_
//#define WIN_SYMBOLS_H_

#include <libvmi/libvmi.h>
#include "vmimonitor.h"

bool
rekall_lookup(
    const char* rekall_profile,
    const char* symbol,
    const char* subsymbol,
    addr_t* address,
    addr_t* size);

os_t rekall_get_os_type(const char* rekall_profile);

symbols_t* vmimonitor_get_symbols_from_rekall(const char* profile);

void vmimonitor_free_symbols(symbols_t* symbols);

bool vmimonitor_get_function_rva(const char* rekall_profile,
		const char* function,
		addr_t* rva);

bool vmimonitor_get_constant_rva(const char* rekall_profile,
		const char* constant,
		addr_t* rva);

bool vmimonitor_get_struct_size(const char* rekall_profile,
		const char* struct_name,
		size_t* size);

bool vmimonitor_get_struct_member_rva(const char* rekall_profile,
		const char* struct_name,
		const char* symbol,
		addr_t* rva);


//#endif /* WIN_SYMBOLS_H_ */

#ifdef __cplusplus
}
#endif
