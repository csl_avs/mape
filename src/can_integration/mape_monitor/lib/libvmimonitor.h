#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <glib.h>
//#include "vmimonitor.h"

// defined in vmimonitor.c
extern int interrupted; // keep sync with vmimonitor->interrupted;
struct syscall_info
{
    GTimeVal timestamp;
    uint32_t vcpu;
    uint64_t cr3;
    char *proc_name;
    int32_t pid;
    char *syscall;
    uint64_t nr_syscall;
};

typedef struct syscall_info syscall_info_t;

// defined in vmimonitor.c
// extern syscall_info_t queue[100];
int pop(syscall_info_t *res);
int start_vmimonitor(const char *dom_name);

void *p_start_vmimonitor(void *name);

#ifdef __cplusplus
}
#endif
