#ifdef __cplusplus
extern "C" {
#endif

#ifndef LINUX_H
#define LINUX_H

#include <libvmi/libvmi.h>
#include "rekall-profile.h"

enum linux_offsets
{
	CURRENT_TASK,
	TASK_STRUCT_COMM,
	TASK_STRUCT_CRED,
	TASK_STRUCT_PID,
	TASK_STRUCT_TGID,
	TASK_STRUCT_REALPARENT,
	TASK_STRUCT_PARENT,
	CRED_UID,

	__LINUX_OFFSETS_MAX
};

static const char* linux_offset_names[__LINUX_OFFSETS_MAX][2] =
{
	[CURRENT_TASK] = {"current_task", NULL},
	[TASK_STRUCT_COMM] = {"task_struct", "comm"},
	[TASK_STRUCT_CRED] = {"task_struct", "cred"},
	[TASK_STRUCT_PID] = {"task_struct", "pid"},
	[TASK_STRUCT_TGID] = {"task_struct", "tgid"},
	[TASK_STRUCT_REALPARENT] = {"task_struct", "real_parent"},
	[TASK_STRUCT_PARENT] = {"task_struct", "parent"},
	[CRED_UID] = {"cred", "uid"},
};

addr_t linux_get_current_thread(vmimonitor_t vmimonitor, uint64_t vcpu_id);

addr_t linux_get_current_process(vmimonitor_t vmimonitor, uint64_t vcpu_id);

char* linux_get_process_name(vmimonitor_t vmimonitor, addr_t process_base);

status_t linux_get_process_pid(vmimonitor_t vmimonitor, addr_t process_base, vmi_pid_t* pid);

char* linux_get_current_process_name(vmimonitor_t vmimonitor, uint64_t vcpu_id);

int64_t linux_get_process_userid(vmimonitor_t vmimonitor, addr_t process_base);

int64_t linux_get_current_process_userid(vmimonitor_t vmimonitor, uint64_t vcpu_id);

bool linux_get_current_thread_id(vmimonitor_t vmimonitor, uint64_t vcpu_id, uint32_t* thread_id);

status_t linux_get_process_ppid( vmimonitor_t vmimonitor, addr_t process_base, vmi_pid_t* ppid );

bool linux_get_current_process_data( vmimonitor_t vmimonitor, uint64_t vcpu_id, proc_data_t* proc_data );

bool linux_fill_offset_from_rekall(vmimonitor_t vmimonitor, size_t size, const char* names [][2]);
#endif

#ifdef __cplusplus
}
#endif
