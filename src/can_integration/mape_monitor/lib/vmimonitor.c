#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <signal.h>
#include <glib.h>

#include <libvmi/libvmi.h>
#include <libvmi/events.h>
#include <libvmi/peparse.h>

//#include "vmimonitor.h" included in linux-process.h
//#include "xen_helper.h" included in vmimonitor.h
#include "linux-process.h"
//#include "libvmimonitor.h"
#include <monitor/libvmimonitor.h>

uint8_t bp = INT3_TRAP;
// output verbose debugging info
bool debug = false;
int interrupted = 0;

#define QUEUE_SIZE 10000
syscall_info_t queue[QUEUE_SIZE];
volatile int head = 0, tail = 0;

static vmimonitor_t vmimonitor;

int pop(syscall_info_t *res)
{
    if(!queue)
    {
        return 1;
    }
    int i;
    for (i = 0; i < QUEUE_SIZE; i++)
    {
        while(tail == head)
        {
            sleep(0);
        }
        *res = queue[tail];
        tail = (tail+1) % QUEUE_SIZE;
        return 0;
    }

    return 1;
}

int push(syscall_info_t *item)
{
    if(!queue)
    {
        return 1;
    }

    int i;
    for(i = 0; i < QUEUE_SIZE; i++)
    {
        while((head+1) % QUEUE_SIZE == tail)
        {
            sleep(0);
        }
        queue[head] = *item;
        head = (head + 1) % QUEUE_SIZE;
        return 0;
    }
    return 1;
}

static void close_handler(int sig)
{
    vmimonitor->interrupted = sig;
    interrupted = sig;
}

event_response_t vmi_reset_trap(vmi_instance_t vmi, vmi_event_t *event)
{
    vmimonitor_t vmimonitor = event->data;
    event->slat_id = vmimonitor->altp2m_idx;
    // turn off singlestep
    return VMI_EVENT_RESPONSE_TOGGLE_SINGLESTEP | VMI_EVENT_RESPONSE_VMM_PAGETABLE_ID;
}

void remove_trap(vmimonitor_t vmimonitor,
                 const vmimonitor_trap_t* trap)
{
    vmi_instance_t vmi = vmimonitor->vmi;

    switch (trap->type)
    {
        case BREAKPOINT:
        {
            struct wrapper* container =
                g_hash_table_lookup(vmimonitor->breakpoint_lookup_trap, &trap);

            if ( !container )
                return;

            xen_pfn_t current_gfn = container->breakpoint.pa >> 12;
            uint64_t _current_gfn = current_gfn;

            PRINT_DEBUG("Removing breakpoint trap from 0x%lx.\n",
                        container->breakpoint.pa);

            g_hash_table_remove(vmimonitor->breakpoint_lookup_trap, &trap);
            container->traps = g_slist_remove(container->traps, trap);

            /* Update list of traps on this gfn */
            GSList* traps_on_gfn =
                g_hash_table_lookup(vmimonitor->breakpoint_lookup_gfn, &_current_gfn);
            traps_on_gfn = g_slist_remove(traps_on_gfn, &container->breakpoint.pa);
            g_hash_table_remove(vmimonitor->breakpoint_lookup_gfn, &_current_gfn);

            if ( traps_on_gfn )
            {
                // the list head may change so we force a reinsert
                g_hash_table_insert(vmimonitor->breakpoint_lookup_gfn, g_memdup(&_current_gfn, sizeof(uint64_t)), traps_on_gfn);
            }

            if (!container->traps)
            {

                struct remapped_gfn* remapped_gfn = g_hash_table_lookup(vmimonitor->remapped_gfns, &current_gfn);
                uint8_t backup;

                if ( VMI_FAILURE == vmi_read_8_pa(vmimonitor->vmi, container->breakpoint.pa, &backup) )
                {
                    PRINT_DEBUG( "Critical error in removing int3\n");
                    vmimonitor->interrupted = -1;
                    break;
                }

                if ( VMI_FAILURE == vmi_write_8_pa(vmimonitor->vmi,
                                                   (remapped_gfn->r << 12) + (container->breakpoint.pa & VMI_BIT_MASK(0,11)),
                                                   &backup) )
                {
                    PRINT_DEBUG( "Critical error in removing int3\n");
                    vmimonitor->interrupted = -1;
                    break;
                }

                remove_trap(vmimonitor, &container->breakpoint.guard);
                remove_trap(vmimonitor, &container->breakpoint.guard2);

                g_hash_table_remove(vmimonitor->breakpoint_lookup_pa, &container->breakpoint.pa);
            }

            break;
        }
        case MEMACCESS:
        {
            struct wrapper* container =
                g_hash_table_lookup(vmimonitor->memaccess_lookup_trap, &trap);
            status_t ret;

            if ( !container )
            {
                return;
            }

            container->traps = g_slist_remove(container->traps, trap);

            if (!container->traps)
            {
                if ( VMI_SUCCESS == vmi_set_mem_event(vmi, container->memaccess.gfn, VMI_MEMACCESS_N, vmimonitor->altp2m_idx) )
                {
                    PRINT_DEBUG("Removed memtrap for GFN 0x%lx in altp2m view %u\n",
                                container->memaccess.gfn, vmimonitor->altp2m_idx);
                    g_hash_table_remove(vmimonitor->memaccess_lookup_trap, &trap);
                    g_hash_table_remove(vmimonitor->memaccess_lookup_gfn, &container->memaccess.gfn);
                }
                return;
            }

            /*
             * If more subscriber are present make sure we only set the required access settings.
             */
            GSList* loop = container->traps;
            vmi_mem_access_t update_access = 0;

            while (loop)
            {
                vmimonitor_trap_t* trap = loop->data;
                update_access |= trap->memaccess.access;
                loop=loop->next;
            }

            ret = vmi_set_mem_event(vmi, container->memaccess.gfn, update_access, vmimonitor->altp2m_idx);
            if (VMI_SUCCESS == ret)
            {
                PRINT_DEBUG("Successfully set access to %c%c%c on GFN 0x%lx!\n",
                            (update_access & VMI_MEMACCESS_R) ? 'r' : '-',
                            (update_access & VMI_MEMACCESS_W) ? 'w' : '-',
                            (update_access & VMI_MEMACCESS_X) ? 'x' : '-',
                            container->memaccess.gfn
                           );
                container->memaccess.access = update_access;
            }
            else
                PRINT_DEBUG("Failed to update memaccess trap settings on GFN 0x%lx!\n", container->memaccess.gfn);

            break;
        }
        default:
            break;
    }
}

static void process_free_requests(vmimonitor_t vmimonitor)
{
    GHashTableIter iter;
    addr_t *key = NULL;

    struct free_trap_wrapper *free_wrapper = NULL;
    g_hash_table_iter_init(&iter, vmimonitor->remove_traps);
    while(g_hash_table_iter_next(&iter, (void**)&key, (void**)&free_wrapper))
    {
        remove_trap(vmimonitor, free_wrapper->trap);
        if(free_wrapper->free_routine)
        {
            free_wrapper->free_routine(free_wrapper->trap);
        }
        g_free(free_wrapper);
    }

    // free remove_traps
    g_hash_table_destroy(vmimonitor->remove_traps);
    vmimonitor->remove_traps = g_hash_table_new_full(g_int64_hash, g_int64_equal, free, NULL);
}

event_response_t post_mem_cb(vmi_instance_t vmi, vmi_event_t* event)
{
    event_response_t rsp = 0;
    struct memcb_pass *pass = event->data;
    vmimonitor_t vmimonitor = pass->vmimonitor;
    vmimonitor->regs[event->vcpu_id] = event->x86_regs;

    struct wrapper *s = g_hash_table_lookup(vmimonitor->memaccess_lookup_gfn, &pass->gfn);
    if(!s)
    {
        PRINT_DEBUG( "post_mem_cb @ 0x%lx cleared \n", pass->gfn);
        goto post_out;
    }

    vmimonitor->in_callback = 1;
    GSList *loop = s->traps;
    
    while(loop)
    {
        vmimonitor_trap_t *trap = loop->data;
        if(trap->cb && trap->memaccess.type == POST &&
                (trap->memaccess.access & pass->access))
        {
            vmimonitor_trap_info_t trap_info = 
            {
                .vcpu = event->vcpu_id,
                .proc_data.name      = pass->proc_data.name,
                .proc_data.pid       = pass->proc_data.pid,
                .proc_data.ppid      = pass->proc_data.ppid,
                .proc_data.base_addr = pass->proc_data.base_addr,
                .proc_data.userid    = pass->proc_data.userid,
                .trap_pa = pass->pa,
                .regs = event->x86_regs,
                .vcpu = event->vcpu_id
            };
        
            g_get_current_time(&trap_info.timestamp);

            rsp |= trap->cb(vmimonitor, &trap_info);
        }

        loop = loop->next;
    }

    vmimonitor->in_callback = 0;
    process_free_requests(vmimonitor);

    // copy the page to remapped gfn and reinject traps
    if(pass->traps)
    {
        uint8_t page[VMI_PS_4KB] = {0};
        if(vmi_read_pa(vmimonitor->vmi, pass->remapped_gfn->o << 12, VMI_PS_4KB, &page, NULL)
                == VMI_FAILURE)
        {
            vmimonitor->interrupted = -1;
            PRINT_DEBUG( "psot_mem_cb FAILED to read to-be-remapped gfn\n");
            return 0;
        }

        if(vmi_write_pa(vmimonitor->vmi, pass->remapped_gfn->r << 12, VMI_PS_4KB, &page, NULL))
        {
            vmimonitor->interrupted = -1;
            PRINT_DEBUG( "post_mem_cb FAILED to write remapped gfn");
            return 0;
        }

        GSList *loop = pass->traps;

        while(loop)
        {
            addr_t *pa = loop->data;
            uint8_t test = 0;
            struct wrapper *s = g_hash_table_lookup(vmimonitor->breakpoint_lookup_pa, pa);

            /* test if pa is already breakpointed */
            if(vmi_read_8_pa(vmimonitor->vmi, *pa, &test) == VMI_FAILURE)
            {
                vmimonitor->interrupted = -1;
                PRINT_DEBUG( "post_mem_cb FAILED to write remapped gfn");
                return 0;
            }
            if (test == bp)
            {
                s->breakpoint.doubletrap = 1;
            }
            else
            {
                s->breakpoint.doubletrap = 0;
                if ( vmi_write_8_pa(vmimonitor->vmi, (pass->remapped_gfn->r << 12) + (*pa & VMI_BIT_MASK(0, 11)), 
                            &bp) == VMI_FAILURE)
                {
                    vmimonitor->interrupted = -1;
                    PRINT_DEBUG( "post_mem_cb FAILED to inject bps \n");
                    return 0;
                }
            }

            loop = loop->next;
        }
    }

post_out:
    g_free( (gpointer)pass->proc_data.name );
    g_free(pass);

    event->slat_id = vmimonitor->altp2m_idx;
    vmimonitor->step_event[event->vcpu_id]->callback = vmi_reset_trap;
    vmimonitor->step_event[event->vcpu_id]->data = vmimonitor;

    return rsp | VMI_EVENT_RESPONSE_TOGGLE_SINGLESTEP |
        VMI_EVENT_RESPONSE_VMM_PAGETABLE_ID;
}

event_response_t pre_mem_cb(vmi_instance_t vmi, vmi_event_t* event)
{
    event_response_t rsp = 0;
    vmimonitor_t vmimonitor = event->data;
    vmimonitor->regs[event->vcpu_id] = event->x86_regs;

    struct wrapper *s = g_hash_table_lookup(vmimonitor->memaccess_lookup_gfn, &event->mem_event.gfn);

    if(!s)
    {
        PRINT_DEBUG( "MEM event cleared for GFN 0x%lx but can be found in view %u\n",
                event->mem_event.gfn, event->slat_id);
        return 0;
    }

    addr_t pa = (event->mem_event.gfn << 12) + event->mem_event.offset;

    proc_data_t proc_data = {0};
    linux_get_current_process_data(vmimonitor, event->vcpu_id, &proc_data);

    GSList *loop = s->traps;
    vmimonitor->in_callback = 1;

    while(loop)
    {
        vmimonitor_trap_t *trap = loop->data;
        if(trap->cb && trap->memaccess.type == PRE &&
                (trap->memaccess.access & event->mem_event.out_access))
        {
            vmimonitor_trap_info_t trap_info = 
            {
                .vcpu = event->vcpu_id,
                .proc_data.name = proc_data.name,
                .proc_data.pid = proc_data.pid,
                .proc_data.ppid = proc_data.ppid,
                .proc_data.base_addr = proc_data.base_addr,
                .proc_data.userid = proc_data.userid,
                .trap_pa = pa,
                .regs = event->x86_regs,
                .vcpu = event->vcpu_id
            };
        
            g_get_current_time(&trap_info.timestamp);

            rsp |= trap->cb(vmimonitor, &trap_info);
        }

        loop = loop->next;
    }

    /* register all breakpoint handlers */
    if (event->mem_event.out_access & VMI_MEMACCESS_X)
    {
        struct wrapper *sbp = g_hash_table_lookup(vmimonitor->breakpoint_lookup_pa, &pa);
        loop = sbp->traps;
        while (loop)
        {
            vmimonitor_trap_t *trap = loop->data;
            vmimonitor_trap_info_t trap_info = 
            {
                .vcpu = event->vcpu_id,
                .proc_data.name = proc_data.name,
                .proc_data.pid = proc_data.pid,
                .proc_data.ppid = proc_data.ppid,
                .proc_data.base_addr = proc_data.base_addr,
                .proc_data.userid = proc_data.userid,
                .trap_pa = pa,
                .regs = event->x86_regs,
                .vcpu = event->vcpu_id
            };
        
            g_get_current_time(&trap_info.timestamp);

            loop = loop->next;
            rsp |= trap->cb(vmimonitor, &trap_info);
        }
    }
    
    vmimonitor->in_callback = 0;
    process_free_requests(vmimonitor);

    /* check if there're traps active on gfn */
    s = g_hash_table_lookup(vmimonitor->memaccess_lookup_gfn, &event->mem_event.gfn);
    if (s)
    {
        struct memcb_pass *pass = g_malloc0(sizeof(struct memcb_pass));
        pass->vmimonitor = vmimonitor;
        pass->gfn = event->mem_event.gfn;
        pass->pa = pa;
        pass->access = event->mem_event.out_access;
        pass->proc_data.base_addr = proc_data.base_addr;
        pass->proc_data.name      = proc_data.name;
        pass->proc_data.pid       = proc_data.pid;
        pass->proc_data.ppid      = proc_data.ppid;
        pass->proc_data.userid    = proc_data.userid;

        if(!s->memaccess.guard2)
        {
            event->slat_id = 0;

            if(event->mem_event.out_access & VMI_MEMACCESS_W)
            {
                pass->traps = g_hash_table_lookup(vmimonitor->breakpoint_lookup_gfn, &pass->gfn);
                if (pass->traps)
                {
                    pass->remapped_gfn = g_hash_table_lookup(vmimonitor->remapped_gfns, &pass->gfn);
                }
            }
        }
        else
        {
            event->slat_id = vmimonitor->altp2m_idr;
        }

        vmimonitor->step_event[event->vcpu_id]->callback = post_mem_cb;
        vmimonitor->step_event[event->vcpu_id]->data = pass;

        return rsp | VMI_EVENT_RESPONSE_TOGGLE_SINGLESTEP |
            VMI_EVENT_RESPONSE_VMM_PAGETABLE_ID;
    }
    g_free((gpointer)proc_data.name);
    return rsp;
}

event_response_t int3_cb(vmi_instance_t vmi, vmi_event_t *event)
{
    event_response_t rsp = 0;
    vmimonitor_t vmimonitor = event->data;
    vmimonitor->regs[event->vcpu_id] = event->x86_regs;

    // an INT3 has been triggered, let's find the exact address first
    addr_t pa = (event->interrupt_event.gfn << 12)
        + event->interrupt_event.offset + event->interrupt_event.insn_length - 1;

    struct wrapper *s = g_hash_table_lookup(vmimonitor->breakpoint_lookup_pa, &pa);

    if(!s)
    {
        /* no register trap found in the hashtable
         * so it may be caused by a lately removed one
         */

        uint8_t test = 0;
        if(VMI_FAILURE == vmi_read_8_pa(vmi, pa, &test))
        {
            PRINT_DEBUG( "[Critical] int3_cb cannot read page.\n");
            vmimonitor->interrupted = -1;
            return 0;
        }

        if(test == bp)
        {
            /* we found bp(0xcc) here, which there's already a trap */
            event->interrupt_event.reinject = 1;
            PRINT_DEBUG( "Reinjecting breakpoint to domain.\n");
        }
        else
        {
            event->interrupt_event.reinject = 0;
            PRINT_DEBUG( "Ingore old breakpoint event just found.\n");
        }

        return 0;
    }

    if(s->breakpoint.doubletrap)
    {
        event->interrupt_event.reinject = 1;
    }
    else
    {
        event->interrupt_event.reinject = 0;
    }

    proc_data_t proc_data = {0};

    linux_get_current_process_data(vmimonitor, event->vcpu_id, &proc_data);
    
    vmimonitor->in_callback = 1;

    GSList *loop = s->traps;

    while(loop)
    {
        vmimonitor_trap_t *trap = loop->data;
        vmimonitor_trap_info_t trap_info = 
        {
            .trap = trap,
            .proc_data.name = proc_data.name,
            .proc_data.pid = proc_data.pid,
            .proc_data.ppid = proc_data.ppid,
            .proc_data.base_addr = proc_data.base_addr,
            .proc_data.userid = proc_data.userid,
            .trap_pa = pa,
            .regs = event->x86_regs,
            .vcpu = event->vcpu_id
        };
        
        g_get_current_time(&trap_info.timestamp);

        loop = loop->next;
        // call event callback, such as 'linux_syscall_cb'
        rsp |= trap->cb(vmimonitor, &trap_info);
    }

    vmimonitor->in_callback = 0;
    g_free((gpointer)proc_data.name);

    process_free_requests(vmimonitor);

    if (g_hash_table_lookup(vmimonitor->breakpoint_lookup_pa, &pa))
    {
        PRINT_DEBUG( "Switching altp2m and to singlestep on vcpu %u\n", event->vcpu_id);

        event->slat_id = 0;
        vmimonitor->step_event[event->vcpu_id]->callback = vmi_reset_trap;
        vmimonitor->step_event[event->vcpu_id]->data = vmimonitor;

        return rsp | 
            VMI_EVENT_RESPONSE_TOGGLE_SINGLESTEP | 
            VMI_EVENT_RESPONSE_VMM_PAGETABLE_ID;
    }

    return rsp;
}

bool init_vmi(vmimonitor_t vmimonitor)
{
    int rc;
    uint64_t flags = 0;
    
    // initialize libvmi
    if(vmi_init(&vmimonitor->vmi, VMI_XEN, &vmimonitor->dom_id, 
                VMI_INIT_DOMAINID | VMI_INIT_EVENTS, NULL, NULL) == VMI_FAILURE)
    {
        printf("Failed to initialize libvmi vmi.\n");
        return false;
    }

    //GHashTable *config = g_hash_table_new(g_str_hash, g_str_equal);
    //vmimonitor->rekall_profile = vmi_get_rekall_path(vmimonitor->vmi);
    //g_hash_table_insert(config, "rekall_profile", vmimonitor->rekall_profile);
    //g_hash_table_insert(config, "os_type", "Linux");

    // init paging mode
    if(vmi_init_paging(vmimonitor->vmi, flags) == VMI_PM_UNKNOWN)
    {
        printf("Failed to initialize libvmi paging.\n");
        //g_hash_table_destroy(config);
        return false;
    }

    // use libvmi global config in /home/$USER/etc/libvmi.conf
    // so we don't need to provide config
    vmimonitor->os_type = vmi_init_os(vmimonitor->vmi, VMI_CONFIG_GLOBAL_FILE_ENTRY, NULL, NULL);

    //g_hash_table_destroy(config);
    
    vmimonitor->pm = vmi_get_page_mode(vmimonitor->vmi, 0);
    vmimonitor->vcpus = vmi_get_num_vcpus(vmimonitor->vmi);
    // addr_t == uint64_t
    vmimonitor->init_memsize = (uint64_t)vmi_get_max_physical_address(vmimonitor->vmi);
    /* call strdup to duplicate rekall_profile path
     * or vmi_destory() may corrupt it and cause double free issue
     */  
    vmimonitor->rekall_profile = strdup(vmi_get_rekall_path(vmimonitor->vmi));

    if(xc_domain_maximum_gpfn(vmimonitor->xen->xc, vmimonitor->dom_id, &vmimonitor->max_gpfn) < 0)
    {
        return false;
    }

    // Crete tables to lookup breakpoints
    vmimonitor->breakpoint_lookup_pa =
        g_hash_table_new_full(g_int64_hash, g_int64_equal, free, free);
     vmimonitor->breakpoint_lookup_gfn =
        g_hash_table_new_full(g_int64_hash, g_int64_equal, free, NULL);
     vmimonitor->breakpoint_lookup_trap =
        g_hash_table_new_full(g_int64_hash, g_int64_equal, free, NULL);
     vmimonitor->memaccess_lookup_gfn =
        g_hash_table_new_full(g_int64_hash, g_int64_equal, free, free);
     vmimonitor->memaccess_lookup_trap =
        g_hash_table_new_full(g_int64_hash, g_int64_equal, free, NULL);
     vmimonitor->remapped_gfns =
        g_hash_table_new_full(g_int64_hash, g_int64_equal, NULL, free);
     vmimonitor->remove_traps =
        g_hash_table_new_full(g_int64_hash, g_int64_equal, free, NULL);

    /* initial step_event */
    unsigned int i;
    /* maximum vcpus of a domainU is 16 */
    for (i = 0; i < vmimonitor->vcpus && i < 16; i++)
    {
        vmimonitor->step_event[i] = g_malloc0(sizeof(vmi_event_t));
        
        SETUP_SINGLESTEP_EVENT(vmimonitor->step_event[i], 1u << i, vmi_reset_trap, 0);
        vmimonitor->step_event[i]->data = vmimonitor;
        
        if(vmi_register_event(vmimonitor->vmi, vmimonitor->step_event[i]) == VMI_FAILURE)
        {
            PRINT_DEBUG( "init_vmi FAILED to register step_event for vcpu %u\n", i);
            return false;
        }
    }

    rc = xc_domain_setmaxmem(vmimonitor->xen->xc, vmimonitor->dom_id, ~0);  
    if(rc < 0)
    {
        PRINT_DEBUG( "xc_domain_setmaxmem failed.\n");
        return false;
    }

    vmimonitor->zero_page_gfn = ++(vmimonitor->max_gpfn);

    rc = xc_domain_populate_physmap_exact(vmimonitor->xen->xc, vmimonitor->dom_id, 1, 0, 0, &vmimonitor->zero_page_gfn);
    if(rc < 0)
    {
        PRINT_DEBUG( "xc_domain_populate_physmap_exact failed.\n");
        return false;
    }

    /* create altp2m */
    rc = xc_altp2m_set_domain_state(vmimonitor->xen->xc, vmimonitor->dom_id, 1);
    if(rc < 0)
    {
        PRINT_DEBUG( "xc_altp2m_set_domain_state failed.\n");
        return false;
    }
    
    // create altp2m idx 
    rc = xc_altp2m_create_view(vmimonitor->xen->xc, vmimonitor->dom_id, 0, &vmimonitor->altp2m_idx);
    if (rc < 0)
    {
        PRINT_DEBUG( "xc_altp2m_create_view idx failed.\n");
        return false;
    }

    // create altp2m idr
    rc = xc_altp2m_create_view(vmimonitor->xen->xc, vmimonitor->dom_id, 0, &vmimonitor->altp2m_idr);
    if (rc < 0)
    {
        PRINT_DEBUG( "xc_altp2m_create_view idr failed.\n");
        return false;
    }

    /* prepare libvmi event */
    SETUP_INTERRUPT_EVENT(&vmimonitor->interrupt_event, 0, int3_cb);
    vmimonitor->interrupt_event.data = vmimonitor;
    // register event
    if(vmi_register_event(vmimonitor->vmi, &vmimonitor->interrupt_event) == VMI_FAILURE)
    {
        PRINT_DEBUG( "Failed to register interrupt event.\n");
        return false;
    }

    SETUP_MEM_EVENT(&vmimonitor->mem_event, ~0ULL, VMI_MEMACCESS_RWX, pre_mem_cb, 1);
    vmimonitor->mem_event.data = vmimonitor;
    if(vmi_register_event(vmimonitor->vmi, &vmimonitor->mem_event) == VMI_FAILURE)
    {
        PRINT_DEBUG( "Failed to register mem event.\n");
        return false;
    }

    // switch to idx view
    rc = xc_altp2m_switch_to_view(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idx);
    if(rc < 0)
    {
        PRINT_DEBUG( "xc_altp2m_switch_to_view idx failed.\n");
        return false;
    }

    return true;
}

bool init_vmimonitor(vmimonitor_t *vmimonitor, const char *domain)
{
    if (!domain)
        return false;

    *vmimonitor = g_malloc0(sizeof(struct vmimonitor));
    (*vmimonitor)->dom_name = g_strdup(domain);
    //(*vmimonitor)->rekall_profile = g_strdup(rekall_profile);
    //(*vmimonitor)->os_type = rekall_get_os_type(rekall_profile);
    (*vmimonitor)->interrupted = 0;

    g_mutex_init(&(*vmimonitor)->vmi_lock);
    // init xen_helper and libvmi
    if(!xen_init_interface(&(*vmimonitor)->xen))
        goto err;
    // get xen domain name and id
    get_dom_info((*vmimonitor)->xen, domain, &(*vmimonitor)->dom_id, &(*vmimonitor)->dom_name);
    if((*vmimonitor)->dom_id == INVALID_DOMID)
        goto err;

    // [is it a must for creating altp2m view?] pause xen domainU
    xen_pause((*vmimonitor)->xen, (*vmimonitor)->dom_id);
    if (!init_vmi(*vmimonitor))
    {
        // resume domain's execution
        xen_resume((*vmimonitor)->xen, (*vmimonitor)->dom_id);
    }

    if ((*vmimonitor)->os_type != VMI_OS_LINUX)
    {
        PRINT_DEBUG( "Domain is not linux.\n");
        goto err;
    }

    if (VMI_FAILURE == vmi_translate_ksym2v((*vmimonitor)->vmi, "_text", &(*vmimonitor)->kernel_base))
    {
        PRINT_DEBUG( "Failed to locate kernel base address\n");
        goto err;
    }

    if (!linux_fill_offset_from_rekall(*vmimonitor, __LINUX_OFFSETS_MAX, linux_offset_names))
    {
        PRINT_DEBUG( "Failed to fill linux offset names \b");
        goto err;
    }

    return true;

err:
    close_vmimonitor(*vmimonitor, true);
    *vmimonitor = NULL;

    return false;
}

void close_vmimonitor(vmimonitor_t vmimonitor, const bool pause)
{
    interrupted = -1;
    if (!vmimonitor)
    {
        return;
    }

    if(vmimonitor->vmi)
    {
        close_vmi(vmimonitor);
    }

    if(vmimonitor->xen)
    {
        if(!pause)
        {
            xen_force_resume(vmimonitor->xen, vmimonitor->dom_id);
        }
        xen_free_interface(vmimonitor->xen);
    }

    if(vmimonitor->offsets)
    {
        g_free(vmimonitor->offsets);
        vmimonitor->offsets = NULL;
    }
    if(vmimonitor->sizes)
    {
        g_free(vmimonitor->sizes);
        vmimonitor->sizes = NULL;
    }
    if(&vmimonitor->vmi_lock)
    {
        g_mutex_clear(&vmimonitor->vmi_lock);
        //vmimonitor->vmi_lock = NULL;
    }
    if(vmimonitor->dom_name)
    {
        g_free(vmimonitor->dom_name);
        vmimonitor->dom_name = NULL;
    }
    if(vmimonitor->rekall_profile)
    {
        g_free(vmimonitor->rekall_profile);
        vmimonitor->rekall_profile = NULL;
    }
    if(vmimonitor)
    {
        g_free(vmimonitor);
        vmimonitor = NULL;
    }
}

void close_vmi(vmimonitor_t vmimonitor)
{
    xen_pause(vmimonitor->xen, vmimonitor->dom_id);

    if(vmimonitor->memaccess_lookup_gfn)
    {
        GHashTableIter iter;
        addr_t *key = NULL;
        struct wrapper *s = NULL;

        g_hash_table_iter_init(&iter, vmimonitor->memaccess_lookup_gfn);
        while(g_hash_table_iter_next(&iter, (void**)&key,(void**)&s))
        {
            vmi_set_mem_event(vmimonitor->vmi, s->memaccess.gfn, VMI_MEMACCESS_N, vmimonitor->altp2m_idx);
            xc_altp2m_change_gfn(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idx, s->memaccess.gfn, ~0);
            
            g_slist_free(s->traps);
            s->traps = NULL;
        }
        //g_hash_table_destroy(vmimonitor->memaccess_lookup_gfn);
    }

    if(vmimonitor->vmi)
    {
        vmi_destroy(vmimonitor->vmi);
        vmimonitor->vmi = NULL;
    }

    if(vmimonitor->breakpoint_lookup_gfn)
    {
        GHashTableIter iter;
        addr_t *key = NULL;
        GSList *list = NULL;

        g_hash_table_iter_init(&iter, vmimonitor->breakpoint_lookup_gfn);
        while(g_hash_table_iter_next(&iter, (void**)&key,(void**)&list))
        {
            g_slist_free(list);
        }
        g_hash_table_destroy(vmimonitor->breakpoint_lookup_gfn);
    }
    
    if(vmimonitor->breakpoint_lookup_pa)
    {
        GHashTableIter iter;
        addr_t *key = NULL;
        struct wrapper *s = NULL;

        g_hash_table_iter_init(&iter, vmimonitor->breakpoint_lookup_pa);
        while(g_hash_table_iter_next(&iter, (void**)&key,(void**)&s))
        {
            g_slist_free(s->traps);
        }
        g_hash_table_destroy(vmimonitor->breakpoint_lookup_pa);
    }

    if(vmimonitor->remapped_gfns)
    {
        GHashTableIter iter;
        xen_pfn_t *key = NULL;
        struct remapped_gfn *rgfn = NULL;

        g_hash_table_iter_init(&iter, vmimonitor->remapped_gfns);
        while(g_hash_table_iter_next(&iter, (void**)&key,(void**)&rgfn))
        {
            xc_altp2m_change_gfn(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idx, rgfn->o, ~0);
            xc_altp2m_change_gfn(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idr, rgfn->r, ~0);
            xc_domain_decrease_reservation_exact(vmimonitor->xen->xc, vmimonitor->dom_id, 1, 0, &rgfn->r);
        }
        g_hash_table_destroy(vmimonitor->remapped_gfns);
    }

    if(vmimonitor->memaccess_lookup_gfn)
        g_hash_table_destroy(vmimonitor->memaccess_lookup_gfn);
    if(vmimonitor->memaccess_lookup_trap)
        g_hash_table_destroy(vmimonitor->memaccess_lookup_trap);
    if(vmimonitor->breakpoint_lookup_trap)
        g_hash_table_destroy(vmimonitor->breakpoint_lookup_trap);
    if(vmimonitor->remove_traps)
        g_hash_table_destroy(vmimonitor->remove_traps);

    unsigned int i;
    for(i = 0; i < vmimonitor->vcpus; i++)
    {
        if(vmimonitor->step_event[i] && vmimonitor->step_event[i]->data != vmimonitor)
            g_free(vmimonitor->step_event[i]->data);
        g_free(vmimonitor->step_event[i]);
    }

    xc_altp2m_switch_to_view(vmimonitor->xen->xc, vmimonitor->dom_id, 0);
    if(vmimonitor->altp2m_idx)
        xc_altp2m_destroy_view(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idx);
    if(vmimonitor->altp2m_idr)
        xc_altp2m_destroy_view(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idr);
    xc_altp2m_set_domain_state(vmimonitor->xen->xc, vmimonitor->dom_id, 0);

    if(vmimonitor->zero_page_gfn)
        xc_domain_decrease_reservation_exact(vmimonitor->xen->xc, vmimonitor->dom_id, 1, 0, &vmimonitor->zero_page_gfn);
    // restore init mem size 
    xc_domain_setmaxmem(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->init_memsize);

    //resume domainU
    xen_resume(vmimonitor->xen, vmimonitor->dom_id);
}

static event_response_t linux_syscall_cb(vmimonitor_t vmimonitor, vmimonitor_trap_info_t *info)
{
    GTimeVal t;
    //syscall_info_t item;

    g_get_current_time(&t);
    /* debugging
     * in recent kernels RAX registers maybe embedded in struct pt_regs
     * so now in monitor node we do a table lookup
    printf("[SYSCALL] TIME: %" PRId64 ".%06" PRId64 ", VCPU:%" PRIu32 ", CR3:0x%" PRIx64 ",\"%s\", %s:%" PRIi64", %s, %s,%lu\n",
            t.tv_sec, t.tv_usec, info->vcpu, info->regs->cr3, info->proc_data.name, "UID", info->proc_data.userid,
            info->trap->breakpoint.module, info->trap->name, info->regs->rax);
    */
    // printf("rbx: %lu, rax:%lu, %s\n", info->regs->rbx, info->regs->rax, info->trap->name);

    /* adding ppid, base_addr for process info */ 
    syscall_info_t item = 
    {
        .timestamp = t,
        .vcpu = (uint32_t)info->vcpu,
        .cr3 = (uint64_t)info->regs->cr3,
        .proc_name = strdup(info->proc_data.name),
        .pid = (int32_t)info->proc_data.pid,
        .syscall = strdup(info->trap->name),
        .nr_syscall = (uint64_t)info->regs->rax,
        .ppid = (int32_t)info->proc_data.ppid,
        .base_addr = (uint64_t)info->proc_data.base_addr
    };

    push(&item);

    return 0;
}


static GSList* create_trap_config(vmimonitor_t vmimonitor, symbols_t *symbols)
{
    GSList *ret = NULL;
    addr_t rva = 0;
    uint64_t i;

    if(!vmimonitor_get_constant_rva(vmimonitor->rekall_profile, "_text", &rva))
        return NULL;

    addr_t kaslr = vmimonitor->kernel_base - rva;

    for (i = 0; i < symbols->count; i++)
    {
        symbol_t *symbol = &symbols->symbols[i];

        if (strncmp(symbol->name, "sys_", 4))
            continue;

        if (!strcmp(symbol->name, "sys_call_table"))
            continue;

        // add trap for each "sys_function"
        vmimonitor_trap_t *trap = (vmimonitor_trap_t *)g_malloc0(sizeof(vmimonitor_trap_t));
        trap->breakpoint.lookup_type = LOOKUP_PID;
        trap->breakpoint.pid = 0;
        trap->breakpoint.addr_type = ADDR_VA;
        trap->breakpoint.addr = symbol->rva + kaslr;
        trap->name = g_strdup(symbol->name);
        trap->breakpoint.module = "linux";
        trap->type = BREAKPOINT;
        trap->cb = linux_syscall_cb;
        /*
        struct wrapper *s = g_malloc0(sizeof(struct wrapper));
        s->type = BREAKPOINT;
        s->vmimonitor = vmimonitor;
        s->traps = g_slist_prepend(s->traps, trap);
        s->breakpoint = trap->breakpoint;
        trap->data = s;
        */
        trap->data = "NULL";
        ret = g_slist_prepend(ret, trap);
    }

    return ret;
}

bool inject_trap_memaccess(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap, bool guard2)
{
    /* check if trap is already registered */
    struct wrapper *s = g_hash_table_lookup(vmimonitor->memaccess_lookup_gfn, &trap->memaccess.gfn);

    if(s)
    {
        vmimonitor_trap_t *oldtrap = s->traps->data;
        if(oldtrap->type != trap->type)
        {
            PRINT_DEBUG( "Failed to add memaccess trap as the page is already trapped\n");
            return false;
        }

        // check if guard2 is used to protect remapped_gfn
        s->memaccess.guard2 = guard2;

        if(s->memaccess.access != trap->memaccess.access)
        {
            //merge access rights
            vmi_mem_access_t new_access = s->memaccess.access | trap->memaccess.access;
            if( VMI_FAILURE == vmi_set_mem_event(vmimonitor->vmi, trap->memaccess.gfn, 
                        new_access, vmimonitor->altp2m_idx))
            {
                PRINT_DEBUG( "Failed to update memory trap @ %lu \n", trap->memaccess.gfn);
                return false;
            }
            s->memaccess.access = new_access;
        }

        s->traps = g_slist_prepend(s->traps, trap);
        g_hash_table_insert(vmimonitor->memaccess_lookup_trap, g_memdup(&trap, sizeof(void*)), s);

        return true;
    }
    else
    {
        xen_unshare_gfn(vmimonitor->xen, vmimonitor->dom_id, trap->memaccess.gfn);
        s = g_malloc0(sizeof(struct wrapper));
        s->vmimonitor = vmimonitor;
        s->traps = g_slist_prepend(s->traps, trap);
        s->memaccess.gfn = trap->memaccess.gfn;
        s->memaccess.access = trap->memaccess.access;

        s->memaccess.guard2 = guard2;
        if( VMI_FAILURE == vmi_set_mem_event(vmimonitor->vmi, trap->memaccess.gfn, 
                        trap->memaccess.access, vmimonitor->altp2m_idx))
        {
            PRINT_DEBUG( "Failed to add memory trap @ %lu \n", trap->memaccess.gfn);
            g_slist_free(s->traps);
            return false;
        }
        
        g_hash_table_insert(vmimonitor->memaccess_lookup_gfn, g_memdup(&s->memaccess.gfn, sizeof(addr_t)), s);
        g_hash_table_insert(vmimonitor->memaccess_lookup_trap, g_memdup(&trap, sizeof(void*)), s);
    }

    return true;

}

bool inject_trap_pa(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap, addr_t pa)
{
    /* first, check if there's already a trap at pa */
    xen_pfn_t cur_gfn = pa >> 12;
    struct wrapper *container = g_hash_table_lookup(vmimonitor->breakpoint_lookup_pa, &pa);

    if(container)
    {
        g_hash_table_insert(vmimonitor->breakpoint_lookup_trap, g_memdup(&trap, sizeof(void*)), container);
        container->traps = g_slist_prepend(container->traps, trap);

        GSList *traps = g_hash_table_lookup(vmimonitor->breakpoint_lookup_gfn, &cur_gfn);
        traps = g_slist_append(traps, &container->breakpoint.pa);

        return true;
    }

    /* then, check if there're mem traps on this page (frame) */
    struct wrapper *s = g_hash_table_lookup(vmimonitor->memaccess_lookup_gfn, &cur_gfn);
    vmi_mem_access_t old_access = VMI_MEMACCESS_INVALID;

    if(s)
        old_access = s->memaccess.access;

    container = g_malloc0(sizeof(struct wrapper));
    if(!container)
        return false;

    container->vmimonitor = vmimonitor;
    container->traps = g_slist_prepend(container->traps, trap);
    container->breakpoint.pa = pa;
    
    /* check if there is already a shadow copy of this page(gfn) */
    struct remapped_gfn *remap_gfn = g_hash_table_lookup(vmimonitor->remapped_gfns, &cur_gfn);

    // cannot find an existing copy, create one
    if(!remap_gfn)
    {
        remap_gfn = g_malloc0(sizeof(struct remapped_gfn));

        remap_gfn->o = cur_gfn;

        int rc;

        remap_gfn->r = ++(vmimonitor->max_gpfn);
        rc = xc_domain_populate_physmap_exact(vmimonitor->xen->xc, vmimonitor->dom_id, 1, 0, 0, &remap_gfn->r);
        if(rc < 0)
            goto err_bp;

        // update remapped_gfns
        g_hash_table_insert(vmimonitor->remapped_gfns, &remap_gfn->o, remap_gfn);
    }

    //trap on cur_gfn is not activated, so cur_gfn has to be backuped
    if(!g_hash_table_lookup(vmimonitor->breakpoint_lookup_gfn, &remap_gfn->o))
    {
        uint8_t page[VMI_PS_4KB] = {0};
        // read page(pa & 0xfffff000) info into page[]
        if(vmi_read_pa(vmimonitor->vmi, cur_gfn << 12, VMI_PS_4KB, &page, NULL) == VMI_FAILURE)
        {
            PRINT_DEBUG( "Dump trapped page FAILED.\n");
            goto err_bp;
        }
        // copy page[] to the shadow page
        if(vmi_write_pa(vmimonitor->vmi, remap_gfn->r << 12, VMI_PS_4KB, &page, NULL) == VMI_FAILURE)
        {
            PRINT_DEBUG( "Copying trapped page to new place FAILED.\n");
            goto err_bp;
        }
    }

    if(!remap_gfn->active)
    {
        remap_gfn->active = 1;
        xc_altp2m_change_gfn(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idx, cur_gfn, remap_gfn->r);
        xc_altp2m_change_gfn(vmimonitor->xen->xc, vmimonitor->dom_id, vmimonitor->altp2m_idx, remap_gfn->r, vmimonitor->zero_page_gfn);
    }

    // set mem access 
    container->breakpoint.guard.type = MEMACCESS;
    container->breakpoint.guard.memaccess.access = VMI_MEMACCESS_RW | old_access;
    container->breakpoint.guard.memaccess.type = PRE;
    container->breakpoint.guard.memaccess.gfn = cur_gfn;

    container->breakpoint.guard2.type = MEMACCESS;
    container->breakpoint.guard2.memaccess.access = VMI_MEMACCESS_RWX;
    container->breakpoint.guard2.memaccess.type = PRE;
    container->breakpoint.guard2.memaccess.gfn = remap_gfn->r;

    if(!inject_trap_memaccess(vmimonitor, &container->breakpoint.guard, 0))
    {
        PRINT_DEBUG( "Failed to set memaccess guard for beakpoint.\n");
        goto err_bp;
    }

    if(!inject_trap_memaccess(vmimonitor, &container->breakpoint.guard2, 1))
    {
        PRINT_DEBUG( "Failed to set memaccess guard2 for beakpoint.\n");
        goto err_bp;
    }

    // remapped pa [31:12] ==  remap_gfn, rpa[11:0] == pa[11:0]
    addr_t rpa = (remap_gfn->r << 12) + (container->breakpoint.pa & VMI_BIT_MASK(0,11));

    uint8_t test;
    if(VMI_FAILURE == vmi_read_8_pa(vmimonitor->vmi, pa, &test))
    {
        PRINT_DEBUG( "Failed to test @ 0x%lx\n", container->breakpoint.pa);
        goto err_bp;
    }

    if(test == bp)
    {
        //there is already a trap at pa
        container->breakpoint.doubletrap = 1;
        PRINT_DEBUG( "Double trap @ 0x%lx\n", pa);
    }
    else
    {
        container->breakpoint.doubletrap = 0;
        // inject INT3
        if(vmi_write_8_pa(vmimonitor->vmi, rpa, &bp) == VMI_FAILURE)
        {
            PRINT_DEBUG( "Inject bp FAILED @ 0x%lx\n", rpa);
            goto err_bp;
        }
    }

    // update hash tables to record trap
    GSList *traps = g_hash_table_lookup(vmimonitor->breakpoint_lookup_gfn, &cur_gfn);
    traps = g_slist_append(traps, &container->breakpoint.pa);
    // save traps
    uint64_t _cur_gfn = cur_gfn;
    g_hash_table_insert(vmimonitor->breakpoint_lookup_gfn, g_memdup(&_cur_gfn, sizeof(uint64_t)), traps);
    g_hash_table_insert(vmimonitor->breakpoint_lookup_pa, g_memdup(&container->breakpoint.pa, sizeof(addr_t)), container);
    g_hash_table_insert(vmimonitor->breakpoint_lookup_trap, g_memdup(&trap, sizeof(void*)), container);

    return true;

err_bp:
    g_free(container);
    g_free(remap_gfn);
    return false;
}

bool inject_trap_breakpoint(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap)
{
    if (trap->breakpoint.lookup_type == LOOKUP_NONE)
        return inject_trap_pa(vmimonitor, trap, trap->breakpoint.addr);

    if (trap->breakpoint.lookup_type == LOOKUP_PID || trap->breakpoint.lookup_type == LOOKUP_NAME)
    {
        if (trap->breakpoint.addr_type == ADDR_VA)
        {
            addr_t dtb, trap_pa;

            // find (page) directory table base (address)
            if( vmi_pid_to_dtb(vmimonitor->vmi, trap->breakpoint.pid, &dtb) == VMI_FAILURE)
            {
                PRINT_DEBUG( "Cannot find DTB for pid %i.\n", trap->breakpoint.pid);
                return false;
            }

            // find physicall address
            if(vmi_pagetable_lookup(vmimonitor->vmi, dtb, trap->breakpoint.addr, &trap_pa) == VMI_FAILURE)
            {
                PRINT_DEBUG( "Failed to find PA for breakpoint VA addr 0x%lx in DTB 0x%lx\n", trap->breakpoint.addr, dtb);
                return false;
            }

            return inject_trap_pa(vmimonitor, trap, trap_pa);
        }
    }

    return false;
}

bool vmimonitor_add_trap(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap)
{
    bool ret;

    if(!trap || !trap->cb)
        return false;

    if(g_hash_table_lookup(vmimonitor->remove_traps, &trap))
    {
        g_hash_table_remove(vmimonitor->remove_traps, &trap);
        return true;
    }

    xen_pause(vmimonitor->xen, vmimonitor->dom_id);
    
    switch(trap->type)
    {
        case BREAKPOINT:
            ret = inject_trap_breakpoint(vmimonitor, trap);
            break;
        case MEMACCESS:
            ret = inject_trap_memaccess(vmimonitor, trap, 0);
        default:
            ret = false;
    }

    xen_resume(vmimonitor->xen, vmimonitor->dom_id);
    return ret; 
}
    
bool vmimonitor_syscall(vmimonitor_t vmimonitor)
{
    symbols_t *symbols = vmimonitor_get_symbols_from_rekall(vmimonitor->rekall_profile);
    GSList* traps;
    uint8_t reg_size;

    if(!symbols)
    {
        PRINT_DEBUG( "syscall: Failed to get symbols from rekall profile %s\n", vmimonitor->rekall_profile);
        return false;
    }

    traps = create_trap_config(vmimonitor, symbols);
    if(!traps)
    {
        PRINT_DEBUG( "syscall: Failed to create trap config.\n");
        vmimonitor_free_symbols(symbols);
        return false;
    }

    g_mutex_lock(&vmimonitor->vmi_lock);
    reg_size = vmi_get_address_width(vmimonitor->vmi);
    g_mutex_unlock(&vmimonitor->vmi_lock);

    vmimonitor_free_symbols(symbols);

    GSList *cur_trap = traps;
    while(cur_trap)
    {
        vmimonitor_trap_t *trap = (vmimonitor_trap_t *)cur_trap->data;
        if(!vmimonitor_add_trap(vmimonitor, trap))
            return false;

        cur_trap = cur_trap->next;
    }

    return true;
}

// for creating pthread
void *p_start_vmimonitor(void *name)
{
    start_vmimonitor((char *)name);

    return NULL;
}

int start_vmimonitor(const char *dom_name)
{
    status_t status = VMI_SUCCESS;
    int rc = 1, c;
    struct sigaction act;
    //char *dom_name;
    //char *name = NULL;
    /*
    if (argc < 3) {
        PRINT_DEBUG( "Usage: %s -d <name of VM> \n", argv[0]);
        exit(1);
    }
    
    while((c = getopt(argc, argv, "d:")) != -1)
    {
        switch (c)
        {
            case 'd':
                name = optarg;
                break;
            default:
                PRINT_DEBUG( "Unrecognized option: %c\n", c);
                return 1;
        }
    }
    */

    // Arg 1 is the VM name.
    if(!dom_name)
    {
        PRINT_DEBUG( "No domain name specified.\n");
        return 1;
    }
    /*
    if(!rekall_profile)
    {
        PRINT_DEBUG( "No rekall profile specified.\n");
        return 1;
    }
    */

    if(!init_vmimonitor(&vmimonitor, dom_name))
    {
        PRINT_DEBUG( "Failed to initial vmimonitor.\n");
        return 1;
    }

    /* for a clean exit */
    act.sa_handler = close_handler;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGHUP,  &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGINT,  &act, NULL);
    sigaction(SIGALRM, &act, NULL);

    // init syscall queue
    //queue = g_malloc0(sizeof(syscall_info_t));
 
    /* trap all syscalls */
    if(!vmimonitor_syscall(vmimonitor))
    {
        PRINT_DEBUG( "Failed to create traps for syscalls \n");
        goto err_exit;
    }

    xen_force_resume(vmimonitor->xen, vmimonitor->dom_id);
    /* start listening to events */
    while (!vmimonitor->interrupted) {
        //printf("listening...\n");
        status = vmi_events_listen(vmimonitor->vmi, 1000);
        if (status != VMI_SUCCESS) {
            printf("Error waiting for events, quitting...\n");
            vmimonitor->interrupted = -1;
        }
    }
    printf("Finished with test.\n");

    rc = 0;

err_exit:
    //close_vmi(vmimonitor->vmi);
    close_vmimonitor(vmimonitor, 0);
    return rc;
}
