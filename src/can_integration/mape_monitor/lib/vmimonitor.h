#ifdef __cplusplus
extern "C" {
#endif

//#pragma GCC visibility push(defaut)

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <glib.h>
#include <xenctrl.h>
#include <libxl_utils.h>
#include <libvmi/libvmi.h>
#include <libvmi/events.h>

#include "xen_helper.h"
	
#define INT3_TRAP 0xCC

typedef enum trap_type
{
	__INVALID_TRAP_TYPE,	
	BREAKPOINT,
	MEMACCESS
	//REGISTER,
	//DEBUG,
	//CPUID
} trap_type_t;

typedef enum addr_type
{
	__INVALID_ADDR_TYPE,
	ADDR_RVA,
	ADDR_VA,
	ADDR_PA
}addr_type_t;

typedef enum memaccess_type
{
	__INVALID_MEMACCESS_TYPE,
	PRE,
	POST
}memaccess_type_t;

typedef enum lookup_type
{
	__INVALID_LOOKUP_TYPE,
	LOOKUP_NONE,
	LOOKUP_DTB,
	LOOKUP_PID,
	LOOKUP_NAME
}lookup_type_t;

typedef struct process_data
{
	// process name
	const char *name;
	// process id
	vmi_pid_t pid;
	//parent process id
	vmi_pid_t ppid;
	// process base address
	addr_t base_addr;
	//process UID
	int64_t userid;
}proc_data_t;

struct vmimonitor{
	char *dom_name;
	domid_t dom_id;
	os_t os_type;

	char *rekall_profile;
	uint16_t altp2m_idx, altp2m_idr;

	xen_interface_t* xen;
	xen_pfn_t zero_page_gfn, max_gpfn;
	
	page_mode_t pm; //paging mode
	GMutex vmi_lock;
   	vmi_instance_t vmi;

	vmi_event_t interrupt_event;
	vmi_event_t mem_event;
	// Xen: max 16 vcpus
	vmi_event_t *step_event[16];
	
	size_t *offsets, *sizes;

	bool in_callback;
	// save all to-be-removed traps and remove them after 
	// all callbacks finish
	GHashTable* remove_traps;

	int interrupted;
	uint32_t vcpus;
	uint64_t init_memsize;

	addr_t kernel_base;
	// kernel (page) directory table base;
	addr_t kdtb;

	x86_registers_t* regs[16];
	addr_t kpcr[16];
	
	// <guest frame number(gfn), remapped gfn>
	GHashTable *remapped_gfns;
	// <pa of trap, struct wrapper>
	GHashTable *breakpoint_lookup_pa;
	// <gfn (uint64_t), GSList of addr_t* for trap physical address>
	GHashTable *breakpoint_lookup_gfn;
	// <trap pointer, struct wrapper>
	GHashTable* breakpoint_lookup_trap;

	// <gfn of trap, struct wrapper>
	GHashTable* memaccess_lookup_gfn;
	// <trap pointer, struct wrapper>
	GHashTable* memaccess_lookup_trap;

	GList *cr0, *cr3, *cr4, *debug, *cpuid;
};

typedef struct vmimonitor *vmimonitor_t;

struct vmimonitor_trap;
typedef struct vmimonitor_trap vmimonitor_trap_t;

typedef struct vmimonitor_trap_info
{
	GTimeVal timestamp;
	uint32_t vcpu;
	uint16_t altp2m_idx;
	proc_data_t proc_data;
	addr_t trap_pa;
	x86_registers_t* regs;
	vmimonitor_trap_t *trap;

	union
	{
		const cpuid_event_t* cpuid;
		const debug_event_t* debug;
	};
}vmimonitor_trap_info_t;

typedef event_response_t (*trap_cb)(vmimonitor_t, vmimonitor_trap_info_t*);

struct vmimonitor_trap
{
	trap_type_t type;
	trap_cb cb;
	const char *name;
	void *data;

	union
	{
		struct
		{
			lookup_type_t lookup_type;
			union
			{
				vmi_pid_t pid;
				const char *proc;
				addr_t dtb;
			};

			const char *module;
			
			addr_type_t addr_type;

			union
			{
				addr_t rva;
				addr_t addr;
			};
		}breakpoint;

		struct
		{
			addr_t gfn;
			vmi_mem_access_t access;
			memaccess_type_t type;
		}memaccess;

		register_t reg;
	};
};

struct breakpoint
{
	addr_t pa;
	vmimonitor_trap_t guard, guard2;
	bool doubletrap;
};

struct memaccess
{
	addr_t gfn;
	bool guard2;
	vmi_mem_access_t access;
};

struct wrapper
{
	trap_type_t type;
	vmimonitor_t vmimonitor;
	GSList *traps;

	union
	{
		struct memaccess memaccess;
		struct breakpoint breakpoint;
	};
};

typedef void (*trap_free_t)(vmimonitor_trap_t *trap);

struct free_trap_wrapper
{
    unsigned int counter;
    vmimonitor_trap_t* trap;
    trap_free_t free_routine;
};

typedef struct symbol
{
	const char *name;
	addr_t rva;
	uint8_t type;
	int inputs;
} __attribute__ ((packed)) symbol_t;

typedef struct symbols
{
	const char *name;
	symbol_t *symbols;// array's size is count
	uint64_t count;
}symbols_t;

struct remapped_gfn
{
    xen_pfn_t o;
    xen_pfn_t r;
    bool active;
};

struct memcb_pass
{
	vmimonitor_t vmimonitor;
	uint64_t gfn;
	addr_t pa;
	proc_data_t proc_data;
	struct remapped_gfn* remapped_gfn;
	vmi_mem_access_t access;
	GSList *traps;
};

/* Functions */
extern bool debug;

#ifndef PRINT_DEBUG
#define PRINT_DEBUG(...) \
	if(debug) \
		fprintf(stderr, __VA_ARGS__);
#endif

// in vmimonitor.c
event_response_t vmi_reset_trap(vmi_instance_t vmi, vmi_event_t *event);

void remove_trap(vmimonitor_t vmimonitor, const vmimonitor_trap_t* trap);

event_response_t post_mem_cb(vmi_instance_t vmi, vmi_event_t* event);

event_response_t pre_mem_cb(vmi_instance_t vmi, vmi_event_t* event);

event_response_t int3_cb(vmi_instance_t vmi, vmi_event_t *event);

bool init_vmi(vmimonitor_t vmimonitor);

void close_vmi(vmimonitor_t vmimonitor);

void close_vmimonitor(vmimonitor_t vmimonitor, const bool pause);

bool init_vmimonitor(vmimonitor_t *vmimonitor, const char *domain);

bool inject_trap_memaccess(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap, bool guard2);

bool inject_trap_pa(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap, addr_t pa);

bool inject_trap_breakpoint(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap);

bool vmimonitor_add_trap(vmimonitor_t vmimonitor, vmimonitor_trap_t *trap);

bool vmimonitor_syscall(vmimonitor_t vmimonitor);

#ifdef __cplusplus
}
#endif
