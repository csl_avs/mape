#include <iostream>
#include <thread>
//#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <cinttypes>

#include "libvmimonitor.h"
/*
pthread_t monitor_thd, printer_thd;

static void close_handler(int sig)
{
    int ret;
    
    if(monitor_thd)
    {
        ret = pthread_kill(monitor_thd, SIGINT);
        if (ret < 0)
            std::cout << "kill vmimonitor thread failed." << std::endl;
    }
    
    if(printer_thd)
    {
        ret = pthread_kill(printer_thd, SIGINT);
        if (ret < 0 )
            std::cout << "Kill priter thread failed." << std::endl;
    }
    
    exit(0);
}
*/
void *printer(void *param)
{
    syscall_info_t res;
    //while(pop(&res))
    while(1)
    {
        if(pop(&res))
        {
            std::cout << "pop..."; //<< std::endl;
            //return NULL;
        }
        printf("[SYSCALL] TIME: %" PRId64 ".%06" PRId64 ", VCPU:%" PRIu32 ", CR3:0x%" PRIx64 ",\"%s\", %d, %s, %d\n",
                res.timestamp.tv_sec, res.timestamp.tv_usec, res.vcpu, res.cr3, res.proc_name, 
                res.pid, res.syscall, (int)res.nr_syscall);
    }
    //return NULL;
}

void printer_fn(void)
{
    syscall_info_t res;
    while(!interrupted)
    {
        if(pop(&res))
        {
            std::cout << "Poping...";
        }
        printf("[SYSCALL] TIME: %" PRId64 ".%06" PRId64 ", VCPU:%" PRIu32 ", CR3:0x%" PRIx64 ",\"%s\", %d, %s, %d\n",
                res.timestamp.tv_sec, res.timestamp.tv_usec, res.vcpu, res.cr3, res.proc_name, 
                res.pid, res.syscall, (int)res.nr_syscall);
    }
}

int main(int argc, char *argv[])
{
    int c;
    char *dom_name = NULL;
    struct sigaction act;

    if(argc < 3)
    {
        std::cout << "Usage: " << argv[0] << 
            " -d <name of VM> " << std::endl;

        exit(1);
    }

    while((c = getopt(argc, argv, "d:")) != -1)
    {
        switch (c)
        {
            case 'd':
                dom_name = optarg;
                break;
            default:
                std::cout << "Unrecognized option: " << c << std::endl;
                exit(1);
        }
    }

    if (!dom_name)
    {
        std::cout << "No domain name specified" << std::endl;
        exit(1);
    }
    /*
    std::thread monitor(start_vmimonitor, dom_name);
    */
    std::thread printer_thd(printer_fn);
    //printer_thd.join();
    
    //pthread_t monitor_thd, printer_thd;
    /*
    c = pthread_create(&monitor_thd, NULL, p_start_vmimonitor, (void *)strdup(dom_name));
    if(c)
    {
        std::cout << "Unable to create vmimonitor thread, " << c << std::endl;
        exit(1);
    }
    
    c = pthread_create(&printer_thd, NULL, printer, NULL);
    if(c)
    {
        std::cout << "Unable to create printer thread, " << c << std::endl;
        exit(1);
    }
    
    act.sa_handler = close_handler;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGHUP,  &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGINT,  &act, NULL);
    sigaction(SIGALRM, &act, NULL);
    */
    //int *monitor_stat;
    //int *printer_stat;
    /*
    c = pthread_join(monitor_thd, (void **)&monitor_stat);
    if (c)
    {
        std::cout << "Unable to join() vmimonitor thread, " << c << std::endl;
        pthread_kill(monitor_thd, SIGINT);
        exit(1);
    }
    
    c = pthread_join(printer_thd, NULL);
    if (c)
    {
        std::cout << "Unable to join() printer thread, " << c << std::endl;
        pthread_kill(printer_thd, SIGINT);
        exit(1);
    }
    */
    c = start_vmimonitor(dom_name);
    if(c)
    {
        std::cout<< "Failed to start vmi monitor, exiting." << std::endl;
        exit(1);
    }

    /* TODO: catch SIGINT and send it to kill child threads */

    /*
    monitor.join();
    printer_thd.join();
    */
    printer_thd.join();
    return c;
}
