#include <libvmi/libvmi.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>
#include <glib.h>
#include <limits.h>

//#include "vmimonitor.h"
//#include "linux-offsets.h"
#include "linux-process.h"
//#include "rekall-profile.h"

#define STACK_SIZE_8K  0x1fff
#define STACK_SIZE_16K 0x3fff
#define MIN_KERNEL_BOUNDARY 0x80000000

addr_t linux_get_current_process(vmimonitor_t vmimonitor, uint64_t vcpu_id)
{
    addr_t process = 0;
    vmi_instance_t vmi = vmimonitor->vmi;

    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_DTB,
        .dtb = vmimonitor->regs[vcpu_id]->cr3,
        .addr = vmimonitor->regs[vcpu_id]->gs_base + vmimonitor->offsets[CURRENT_TASK],
    };

    if ( VMI_FAILURE == vmi_read_addr(vmi, &ctx, &process) || process < MIN_KERNEL_BOUNDARY )
    {
        /*
         * The kernel stack also has a structure called thread_info that points
         * to a task_struct but it doesn't seem to always agree with current_task.
         * However, when current_task obviously is wrong (for example during a CPUID)
         * we can fall back to it to find the correct process.
         * On most newer kernels the kernel stack size is 16K. This is just a guess
         * so for older kernels this may not work as well if the VA happens to map
         * something that resembles a kernel-address.
         * See https://www.cs.columbia.edu/~smb/classes/s06-4118/l06.pdf for more info.
         */
        ctx.addr = vmimonitor->kpcr[vcpu_id] & ~STACK_SIZE_16K;
        if ( VMI_FAILURE == vmi_read_addr(vmi, &ctx, &process) || process < MIN_KERNEL_BOUNDARY )
        {
            ctx.addr = vmimonitor->kpcr[vcpu_id] & ~STACK_SIZE_8K;
            if ( VMI_FAILURE == vmi_read_addr(vmi, &ctx, &process) || process < MIN_KERNEL_BOUNDARY )
                process = 0;
        }
    }

    return process;
}

/*
 * Threads are really just processes on Linux.
 */
addr_t linux_get_current_thread(vmimonitor_t vmimonitor, uint64_t vcpu_id)
{
    return linux_get_current_process(vmimonitor, vcpu_id);
}

char* linux_get_process_name(vmimonitor_t vmimonitor, addr_t process_base)
{
    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_PID,
        .pid = 0,
        .addr = process_base + vmimonitor->offsets[TASK_STRUCT_COMM]
    };

    return vmi_read_str(vmimonitor->vmi, &ctx);
}

status_t linux_get_process_pid(vmimonitor_t vmimonitor, addr_t process_base, vmi_pid_t* pid )
{
    /*
     * On Linux PID is actually a thread ID, while the TGID (Thread Group-ID) is
     * what getpid() would return. Because THAT makes sense.
     */
    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_PID,
        .pid = 0,
        .addr = process_base + vmimonitor->offsets[TASK_STRUCT_TGID]
    };

    return vmi_read_32(vmimonitor->vmi, &ctx, (uint32_t*)pid);
}

char* linux_get_current_process_name(vmimonitor_t vmimonitor, uint64_t vcpu_id)
{
    addr_t process_base = linux_get_current_process(vmimonitor, vcpu_id);
    if ( !process_base )
        return NULL;

    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_DTB,
        .dtb = vmimonitor->regs[vcpu_id]->cr3,
        .addr = process_base + vmimonitor->offsets[TASK_STRUCT_COMM]
    };

    return vmi_read_str(vmimonitor->vmi, &ctx);
}

int64_t linux_get_process_userid(vmimonitor_t vmimonitor, addr_t process_base)
{
    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_PID,
        .pid = 0,
        .addr = process_base + vmimonitor->offsets[TASK_STRUCT_CRED]
    };

    addr_t cred;
    if ( VMI_FAILURE == vmi_read_addr(vmimonitor->vmi, &ctx, &cred) )
        return -1;

    uint32_t uid;
    ctx.addr = cred + vmimonitor->offsets[CRED_UID];
    if ( VMI_FAILURE == vmi_read_32(vmimonitor->vmi, &ctx, &uid) )
        return -1;

    return uid;
};

int64_t linux_get_current_process_userid(vmimonitor_t vmimonitor, uint64_t vcpu_id)
{
    addr_t process_base = linux_get_current_process(vmimonitor, vcpu_id);
    if ( !process_base )
        return -1;

    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_DTB,
        .dtb = vmimonitor->regs[vcpu_id]->cr3,
        .addr = process_base + vmimonitor->offsets[TASK_STRUCT_CRED]
    };

    addr_t cred;
    if ( VMI_FAILURE == vmi_read_addr(vmimonitor->vmi, &ctx, &cred) )
        return -1;

    uint32_t uid;
    ctx.addr = cred + vmimonitor->offsets[CRED_UID];
    if ( VMI_FAILURE == vmi_read_32(vmimonitor->vmi, &ctx, &uid) )
        return -1;

    return uid;
}

bool linux_get_current_thread_id( vmimonitor_t vmimonitor, uint64_t vcpu_id, uint32_t* thread_id )
{
    /*
     * On Linux PID is actually the thread ID....... ... ...
     */
    addr_t process_base = linux_get_current_process(vmimonitor, vcpu_id);
    if ( !process_base )
        return false;

    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_DTB,
        .dtb = vmimonitor->regs[vcpu_id]->cr3,
        .addr = process_base + vmimonitor->offsets[TASK_STRUCT_PID]
    };
    uint32_t _thread_id;

    if ( VMI_FAILURE == vmi_read_32(vmimonitor->vmi, &ctx, &_thread_id) )
        return false;

    *thread_id = _thread_id;

    return true;
}

status_t linux_get_process_ppid( vmimonitor_t vmimonitor, addr_t process_base, vmi_pid_t* ppid )
{
    status_t ret ;
    addr_t parent_proc_base = 0 ;
    access_context_t ctx =
    {
        .translate_mechanism = VMI_TM_PROCESS_PID,
        .pid = 0,
        .addr = process_base + vmimonitor->offsets[TASK_STRUCT_REALPARENT]
    };

    ret = vmi_read_addr( vmimonitor->vmi, &ctx, &parent_proc_base );

    /* If we were unable to get the "proc->real_parent *" get "proc->parent *"... */
    /* Assuming a parent_proc_base == 0 is a fail... */
    if ( (ret == VMI_FAILURE ) || ! parent_proc_base )
    {
        ctx.addr = process_base + vmimonitor->offsets[TASK_STRUCT_PARENT];
        ret = vmi_read_addr( vmimonitor->vmi, &ctx, &parent_proc_base );
    }

    /* Get pid from parent/real_parent...*/
    if ( ( ret == VMI_SUCCESS ) && parent_proc_base )
    {
        ctx.addr = parent_proc_base + vmimonitor->offsets[TASK_STRUCT_TGID];
        return vmi_read_32( vmimonitor->vmi, &ctx, (uint32_t*)ppid );
    }

    return VMI_FAILURE ;
}

bool linux_get_current_process_data( vmimonitor_t vmimonitor, uint64_t vcpu_id, proc_data_t* proc_data )
{
    proc_data->base_addr = linux_get_current_process( vmimonitor, vcpu_id );

    if ( proc_data->base_addr )
    {
        if ( linux_get_process_pid( vmimonitor, proc_data->base_addr, &proc_data->pid ) == VMI_SUCCESS )
        {
            proc_data->name = linux_get_process_name( vmimonitor, proc_data->base_addr );

            if ( proc_data->name )
            {
                proc_data->userid = linux_get_process_userid( vmimonitor, proc_data->base_addr );
                linux_get_process_ppid( vmimonitor, proc_data->base_addr, &proc_data->ppid );

                return true ;
            }
        }
    }

    return false ;
}

bool linux_fill_offset_from_rekall(vmimonitor_t vmimonitor, size_t size, const char* names [][2])
{
	unsigned int i;

	vmimonitor->offsets = g_malloc0(sizeof(addr_t) * size);
	if(!vmimonitor->offsets)
		return false;

	for(i = 0; i < size; i++)
	{
		if(!rekall_lookup(vmimonitor->rekall_profile, names[i][0],
					names[i][1], &vmimonitor->offsets[i], NULL))
		{
			fprintf(stderr, "Failed to find offset for %s:%s\n",
					names[i][0], names[i][1]);
		}
	}

	return true;
}


