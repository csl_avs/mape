import psutil
from datetime import date


# Provide methods to retrieve most hardware statistics, i.e.
# CPUs, memory, disks, networks
# TODO: PID monitoring (if necessary)
class HWStats:
    def __init__(self):
        # self.meminfo = open('/proc/meminfo', 'r')
        # self.memdict = dict((i.split()[0].rstrip(':'),int(i.split()[1])) for i in open('/proc/meminfo').readlines())
        pass
    
    def GetCpuTimes(self):
        return psutil.cpu_times()

    def GetNumProcesses(self):
        return len(psutil.pids());

    def GetCpuTimesUser(self):
        return psutil.cpu_times().user

    def GetCpuTimesSys(self):
        return psutil.cpu_times().system

    def GetCputimeIdle(self):
        return psutil.cpu_times().idle

    def GetCpuPercent(self):
        return psutil.cpu_percent()

    def GetCpuPercentEach(self):
        return psutil.cpu_percent(percpu=True)

    def GetCpuPercentInterval(self, period):
        # Give cpu percentage in the last 'period' seconds
        if type(period) is str:
            raise TypeError('period must be a number')

        return psutil.cpu_percent(interval=period)

    def GetCpuCtx(self):
        return psutil.cpu_stats().ctx_switches

    def GetIrqs(self):
        return psutil.cpu_stats().interrupts

    def GetSoftIrqs(self):
        return psutil.cpu_stats().soft_interrupts

    def GetSyscalls(self):
        return psutil.cpu_stats().syscalls

    def GetCpufreq(self):
        return psutil.cpu_freq()

    def GetMemTotal(self):
        return psutil.virtual_memory().total

    def GetMemTotalNew(self):
        #in KB
        memdict = dict((i.split()[0].rstrip(':'),int(i.split()[1])) for i in open('/proc/meminfo').readlines())
        return int(memdict['MemTotal'])

    def GetMemAvailable(self):
        return psutil.virtual_memory().available

    def GetMemAvailableNew(self):
        #in KB
        memdict = dict((i.split()[0].rstrip(':'),int(i.split()[1])) for i in open('/proc/meminfo').readlines())
        return int(memdict['MemAvailable'])
    
    def GetMemActive(self):
        return psutil.virtual_memory().active

    def GetMemActiveNew(self):
        #in KB
        memdict = dict((i.split()[0].rstrip(':'),int(i.split()[1])) for i in open('/proc/meminfo').readlines())
        return int(memdict['Active'])

    def GetSwapTotal(self):
        return psutil.swap_memory().total

    def GetSwapUsed(self):
        return psutil.swap_memory().used

    def GetDiskUsageAll(self):
        return psutil.disk_usage('/').percent

    def GetDiskUsage(self, path):
        return psutil.disk_usage(path).percent

    def GetNetStats(self):
        return psutil.net_io_counters()

    def GetNetSentBytes(self):
        return psutil.net_io_counters().bytes_sent

    def GetNetSentPackets(self):
        return psutil.net_io_counters().packets_sent

    def GetCurCpuTemprature(self):
        temp = psutil.sensors_temperatures()['coretemp']

        all = 0
        num = 0
        for socket in temp:
            if 'Phisical' in socket.lable:
                all = all + socket.current
                num = num + 1

        return all/num

    def _GetCurCpuTemprature(self, name, core=0):
        if type(core) is not int:
            raise TypeError("Param must be integer")

        if core < 0:
            raise ValueError("Param mustn't be less than 0")

        temp = psutil.sensors_temperatures()['coretemp']
        
        for item in temp:
            if (name % core) in item.label:
                # locate core
                return item.current
                
        return ~0
    
    def GetCurCoreTemprature(self, id_core=0):
        return self._GetCurCpuTemprature('Core %d', id_core)

    def GetCurSockTemprature(self, id_sock=0):
        return self._GetCurCpuTemprature('Physical id %d', id_sock)

    def GetBootTime():
        return psutil.boot_time()

    def GetBootTimeReadable():
        return date.fromtimestamp(psutil.boot_time()). \
            strftime("%Y-%m-%d %H:%M:%S")
