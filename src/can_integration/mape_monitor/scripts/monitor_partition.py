#!/usr/bin/env python
import json
import SocketServer
import rospy
import libvirt
import pylxd
import rospkg
from xmlparse import xmlparse
from acronis_common.msg import PartitionLogs, PartitionStats
from time import sleep
import thread
# import sys


HOST = '155.69.149.174'
PORT = 5566
INTERVAL = 1
DATA_LOG = dict()
# pub = None

stats_lock = thread.allocate_lock()


class UDP_data_handler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        # socket = self.request[1]

        print('Recieve from {0}'.format(self.client_address))
        print(json.loads(data))
        
        # global DATA_LOG

        # update log data to include the current report
        # raw_data = dict()
        raw_data = json.loads(data)

        global stats_lock, DATA_LOG

        partition = raw_data['partition']

        partition_stats = dict()
        partition_stats['num_processes'] = raw_data['num_processes']
        partition_stats['mem_available'] = raw_data['mem_available']
        partition_stats['mem_total'] = raw_data['mem_total']
        partition_stats['cpu_percent'] = raw_data['cpu_percent']

        stats_lock.acquire()

        DATA_LOG[partition] = partition_stats

        stats_lock.release()

        
def find_dom_config(name):
    global configlist
    dom = configlist[name]
    # print("dom is: {}".format(dom))
    if type(dom) is dict:
        return dom


def publish_stats(delay=INTERVAL):
    global stats_lock, DATA_LOG, pub, configlist
    while True:
        # if len(DATA_LOG) > 0:
        msg = PartitionLogs()
            
        stats_lock.acquire()
        for key in configlist:
            log = PartitionStats()
            log.partition_addr = key
            
            if key in DATA_LOG:
                log.num_processes = DATA_LOG[key]['num_processes']
                log.mem_avalable = DATA_LOG[key]['mem_available']
                log.mem_total = DATA_LOG[key]['mem_total']
                log.cpu_usage = DATA_LOG[key]['cpu_percent']
                    
            else:
                log.num_processes = 0
                log.mem_avalable = 0
                log.mem_total = 1
                log.cpu_usage = 0

            global conn, client
            config = find_dom_config(key)
            if config is None:
                continue
            # print('config is: {}'.format(config))
            if config['class'] == 2:
                dom = client.containers.get(key)
                if dom.status == 'Running':
                    log.status = 1
                else:
                    log.status = 0
            elif config['class'] == 1:
                domu = conn.lookupByName(key)
                log.status = domu.state()[0]
                    
            msg.logs.append(log)

        DATA_LOG.clear()
        stats_lock.release()
        if not rospy.is_shutdown():
            pub.publish(msg)
            
        # sleep(delay)


if __name__ == '__main__':
    rospy.init_node('MAPE_m_partition', anonymous=True)
    # global pub
    pub = rospy.Publisher('partition_status', PartitionLogs, queue_size=1)

    global node_config
    if rospy.has_param('~node_config'):
        # search param server for node_config
        node_config = rospy.get_param('~node_config')
        # ros_plan = ROSNodePlanner('nodes.xml')
    else:
        # nodes.xml should be in $(PLANNER_PATH)/nodes.xml
        pkg_handler = rospkg.RosPack()
        node_config = pkg_handler.get_path('mape_monitor')+'/nodes.xml'
        # rospy.loginfo("[debug] config is %s" % node_config)
        rospy.set_param('~node_config', node_config)

    global xmlparser, configlist, conn, client
    xmlparser = xmlparse(node_config)
    nodelist, configlist = xmlparser.GetNodeList()

    conn = libvirt.open(None)
    client = pylxd.Client()
    
    try:
        thread.start_new_thread(publish_stats, (1,))
    except:
        print 'publish thread error'
    
    # global HOST, PORT
    host = HOST
    port = PORT
    if(rospy.has_param('~host')):
        host = rospy.get_param('~host')
    if(rospy.has_param('~port')):
        port = rospy.get_param('~port')

    server = SocketServer.UDPServer((host, port), UDP_data_handler)
    server.serve_forever()
