#!/usr/bin/env python

'''
By Cheng Kun

'''
import json
import socket
import sys
import getopt
import ctypes
from time import sleep
from profiling import HWStats
from ctypes.util import find_library
from random import randint


# HOST = '155.69.149.174'  # host ip address
# PORT = 5566  # host port
INTERVAL = 10  # time interval to retrieve new monitoring data
syscall_enable = False
rogue_mode = False

def report_stats(dest, port):
    # create a UDP socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        print('socket created!')
    except socket.error, msg:
        print('socket creation error: {0}'.format(str(msg[0])))
        sys.exit()

    print('socket initialed!')

    # retrive monitoring statistics
    agent = HWStats()

    while True:
        data = dict()

        data['partition'] = socket.gethostname()
        # reserve only two decimal places for cpu usage
        cpu_percent = float('%.2f' % agent.GetCpuPercent())
        mem_available = agent.GetMemAvailableNew()
        if rogue_mode is True:
            # report fake data to trigger protection, for test purpose only
            cpu_percent *= randint(2, 5)
            if (cpu_percent > 100.0):
                cpu_percent = 99.1
            mem_available /= randint(5,10)

        data['cpu_percent'] = float('%.2f' % agent.GetCpuPercent())
        data['mem_available'] = agent.GetMemAvailableNew()

        data['num_processes'] = agent.GetNumProcesses()  
        data['mem_total'] = agent.GetMemTotalNew()

        # serialize msg
        data_string = json.dumps(data)

        try:
            s.sendto(data_string, (dest, port))
        except socket.error, msg:
            print('data sent error: {0}'.format(str(msg[0])))
            sys.exit()

        if syscall_enable is True:
            # randomly call a syscall
            libc = ctypes.CDLL(find_library('c'), use_errno=True)
            syscall = libc.syscall
            num = randint(1,325)
            syscall(num)
            print('Called {0} syscall.'.format(num))

        sleep(INTERVAL)


if __name__ == '__main__':
    # global HOST = "127.0.0.1", PORT = 5566
    host = "127.0.0.1"
    port = 5566
    
    try:
        optlist, args = getopt.getopt(sys.argv[1:], "hd:p:s:r:", ["help", "dest=", "port=", "syscall=", "rogue="])
    except getopt.GetoptError as err:
        print(str(err))
        print("lxd_agent.py [options]")
        print("-d|--dest= <host_ip_address>")
        print("-p|--port= <host_port>")
        print("CAUTION: Test only options")
        print("-s|--syscall= int (>0 enable)")
        print("-r|--rogue= int (>0 enable)")
        sys.exit(2)
    for opt, arg in optlist:
        if opt in ("-h", "--help"):
            print("lxd_agent.py [options]")
            print("-d|--dest= <host_ip_address>")
            print("-p|--port= <host_port>")
            print("CAUTION: Test only options")
            print("-s|--syscall= int (>0 enable)")
            print("-r|--rogue= int (>0 enable)")
            sys.exit()
        elif opt in ("-d", "--dest"):
            host = arg
        elif opt in ("-p", "--port"):
            port = int(arg)
        elif opt in ("-s", "--syscall"):
            if int(arg) > 0:
                syscall_enable = True
        elif opt in ("-r", "--rogue"):
            if int(arg) > 0:
                rogue_mode = True
        else:
            assert False, "unhandled option"
            
    report_stats(host, port)
