#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <algorithm>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <net/if.h>

#include <boost/thread.hpp>

//#define VIRTUAL_CAN false
/*
#ifdef VIRTUAL_CAN
#define CAN_DEV "vcan0"
#else
*/
#define CAN_DEV "can0"
//#endif

#define MAX_CAN_DATA 8
/* protect CAN socket */
static std::mutex socket_lock;
static uint32_t packet_sent;

struct frame_desc{
        uint16_t id;
        uint16_t dlc;
        uint16_t speed;
        std::vector<uint16_t> data;
};

int send_single_frame(const int can_socket, const struct can_frame frame)
{
    socket_lock.lock();
    int nbytes = write(can_socket, &frame, sizeof(frame));
    packet_sent++;
    if((packet_sent % 100) == 0)
    {
        printf("%d packets sent.\n", packet_sent);
    }
    socket_lock.unlock();
    
    if (nbytes < 0)
    {
        std::cout << boost::this_thread::get_id() << '\n';
        printf("socket sending error!\n");
        return -1;
    }
    
    //std::cout << boost::this_thread::get_id() << ": " << nbytes << " bytes sent.\n";
    return 0;
}

void desc_2_frame(const struct frame_desc desc, struct can_frame &frame)
{
    frame.can_id = desc.id;
    frame.can_dlc = desc.dlc;

    for (int i = 0; i < frame.can_dlc; i++)
    {
        frame.data[i] = (uint8_t)desc.data[i];
    }

}

void send_frame_rand(const int can_socket, const struct frame_desc item)
{
    int min, cycle;
    struct can_frame frame;
    
    frame.can_id = item.id;
    frame.can_dlc = item.dlc;
    for (int i = 0; i < frame.can_dlc; i++)
    {
        frame.data[i] = (uint8_t)item.data[i];
    }
    //frame.data = (uint8_t )&item.data[0];
    
    if (item.speed > 0 && item.speed < 11)
    {
        min = item.speed * 100;
        while (true)
        {
            cycle = rand() % 100 + min;
            send_single_frame(can_socket, frame);
            
            usleep(cycle * 1000);
        }
    }
    else if (item.speed == 0)
    {
        while(true)
        {
            send_single_frame(can_socket, frame);
        }
    }
    else if (item.speed > 10)
    {
        while(true)
        {
            send_single_frame(can_socket, frame);
            usleep(item.speed * 1000);
        }
    }
    else{
        return;
    }
}

int main(int argc, char **argv)
{
    int sock_can;
    struct sockaddr_can addr_can;
    struct ifreq ifr_can;

    /* create a raw socket for CAN */
    if((sock_can = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        printf("Error happened in creating CAN socket!\n");
        exit(-1);
    }

    /* find and return CAN interface, saved in ifr_can */
    //ifr_can.ifr_name = {'/0'};
    strncpy(ifr_can.ifr_name, CAN_DEV, IFNAMSIZ-1);
    ifr_can.ifr_name[IFNAMSIZ-1] = '\0';
    //ifr_can.ifr_ifindex = if_nametoindex(ifr_can.ifr_name);
    if(ioctl(sock_can, SIOCGIFINDEX, &ifr_can) < 0)
    {
        printf("ioctl Error! errid: %d, if_nametoindex %d\n", errno, if_nametoindex(ifr_can.ifr_name));
        exit(-1);
    }

    addr_can.can_family = AF_CAN;
    addr_can.can_ifindex = ifr_can.ifr_ifindex;

    /* bind socket to the CAN interface */
    if(bind(sock_can, (struct sockaddr *)&addr_can, sizeof(addr_can)) < 0)
    {
        printf("bind error! id %d\n", errno);
        exit(-1);
    }

    /******* send CAN frames **********/

    /* open sending frames cfg */
    std::fstream frame_in("frames.cfg");
    if(!frame_in.is_open())
    {
        std::cout << "error open " << "frames.cfg \n";
        exit(-1);
    }

    /* record all sending frames */
    std::vector<struct frame_desc> can_frames;

    char line[64] = {'\0'};

    /* parse frames.cfg */ 
    while(frame_in.getline(line, sizeof(line)))
    {
        /* (c++11) if the current line is empty */
        if(*line == '\0')
        {
            break;
        }
        /* use stringstram to parse line into frame_desc */
        std::stringstream s_stream(line);
        struct frame_desc item;

        /* each frame is represented as: 
         * ID DATA_LENGTH SPEED_FACTOR DATA 
         */
        s_stream >> item.id >> item.dlc >> item.speed;
        item.dlc = (item.dlc > MAX_CAN_DATA)? MAX_CAN_DATA : item.dlc;
        
        for (int i = 0; i < item.dlc; i++)
        {
            uint16_t hex_data;
            s_stream >> hex_data;
            item.data.push_back(hex_data);
        }

        s_stream.clear();
        can_frames.push_back(item);
    }
    /* close() is unnecessary here as RAII will release it after main() exits */
    //frame_in.close();

    /* create thread handler pool */
    std::vector<boost::thread *> pool;
    /* iterate CAN frames */
    std::vector<struct frame_desc>::iterator iter;
    for (iter = can_frames.begin(); iter != can_frames.end(); iter++)
    {
        /*  create a thread for each kind of frame and add it to the pool */
        pool.push_back(new boost::thread(send_frame_rand, sock_can, *iter));
    }
    
    /* start each thread, wait for them to finish and clear memory */
    for(int i = 0; i < pool.size(); i++)
    {
        pool[i]->join();
        delete pool[i];
    }

    /* sending complete, close socket */
    close(sock_can);
    return 0;
}
