//
// Created by yzhou on 5/10/17.
//

#ifndef TEST1_TEST1_H
#define TEST1_TEST1_H

#endif //TEST1_TEST1_H
#include <iostream>
/*
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <time.h>
#include <map>
#include <chrono>
#include <vector>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>
*/

typedef struct tHistory 
{
    char propositions;
    uint32_t msgID;
    long timestamp;
} History;

//int LogicDroid_Module_renewMonitorVariable(int UID);
extern int LogicDroid_Module_initializeMonitor(History * hist0, History *hist1);

int LogicDroid_Module_checkEvent(uint32_t currentMsg, double timestamp, History *hist1, uint32_t checkmsg, int lowbound, int upbound);


bool checkmsg(uint32_t msg, uint32_t * MsgRepository,std::size_t len);

History* History_Constructor(double timestamp);

void Current_Constructor(uint32_t msg, double timestamp, History *hist1);

void History_Reset(History *h);
//void History_insertEvent(History *h, int rel, int idx);
char History_Process(History *next, uint32_t CheckMsg, int lowbound, int upboudn);
//void History_Dispose(History *h);
