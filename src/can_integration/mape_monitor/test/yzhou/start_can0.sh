#!/bin/bash

# ${1} iface name, e.g. can0
# ${2} bit rate, e.g. 500000
sudo ip link set ${1} type can bitrate ${2} triple-sampling on
sudo ifconfig ${1} up
