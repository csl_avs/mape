//
// Created by yzhou on 5/10/17.
//

#include "ReadMsg.h"
#include <algorithm>
#include <math.h>

double compute_duration(time_stamp_t starTime, time_stamp_t endTime){
    std::chrono::duration<double>diff=endTime-starTime;
    double Tduration=diff.count();
    return Tduration;
}


static uint32_t swap_byte_order(const canid_t id)
{
    return (id >> 24) | ((id << 8) & 0x00FF0000) |
           ((id >> 8) & 0x0000FF00) | (id << 24);
}


MsgStructure* ReadMsg(const u_char *pkt, time_stamp_t star_time, time_stamp_t cur_time){

    MsgStructure *msg = (MsgStructure*) malloc(sizeof(MsgStructure));
    struct can_frame *can_f;
    can_f = (struct can_frame *) pkt;
    msg->canID = swap_byte_order(can_f->can_id);
    msg->timestamp=compute_duration(star_time,cur_time);
    return msg;
}

std::vector<uint32_t> construct_msg_repository(std::string filename){
    std::fstream frame_in(filename);
    if(!frame_in.is_open()) {
        std::cout << "error open " << filename <<" \n";
        exit(-1);
    }
    std::vector<struct frame_desc> can_frames;
    std::vector<uint32_t> can_ID;

    char line[sizeof(frame_desc)] = {'\0'};
    while(frame_in.getline(line, sizeof(line)))
    {
        /* (c++11) line is empty */
        if(*line == '\0')
        {
            break;
        }
        /* use stringstram to parse line into frame_desc */
        std::stringstream s_stream(line);
        struct frame_desc item;
        s_stream >> item.id >> item.dlc >> item.speed;
        item.dlc = (item.dlc > MAX_CAN_DATA)? MAX_CAN_DATA : item.dlc;

        for (int i = 0; i < item.dlc; i++)
        {
            uint16_t hex_data;
            s_stream >> hex_data;
            item.data.push_back(hex_data);
        }

        s_stream.clear();
        can_frames.push_back(item);
        std::vector <uint32_t>::iterator it = std::find(can_ID.begin(), can_ID.end(),item.id);
        if (it==can_ID.end()){
            can_ID.push_back(item.id);
        }

    }
    return can_ID;
}


void writ_to_file(std::map<uint32_t, struct frame_count> time_logs){
    time_t now = time(NULL);
    struct tm *aTime = localtime(&now);
    /* get current hours and minutes to prepare logging file */
    std::string file_name = "result_" + std::to_string(aTime->tm_hour) + "_" + std::to_string(aTime->tm_min) + ".txt";

    std::ofstream f_out(file_name);
    /* parse logging dataset to output to the result file */
    std::map<uint32_t, struct frame_count>::iterator iter1;
    for(iter1 = time_logs.begin(); iter1 != time_logs.end(); iter1++)
    {
        f_out << "Time Window " << iter1->first+1<<":";
        std::map<canid_t, uint32_t>::iterator iter2;

        for(iter2 = iter1->second.data.begin(); iter2 != iter1->second.data.end(); iter2++)
        {
            f_out << "can_id=" << iter2->first << " count=" << iter2->second<<";";
        }
        f_out << std::endl;
    }
}


void add_to_log(double timestamp, canid_t can_ID,  std::map<uint32_t, struct frame_count> *time_logs, double time_window){

    std::map <uint32_t, struct frame_count>::iterator iter;
    std::map <uint32_t, struct frame_count>::iterator end_iter;
    uint32_t secs_passed=(timestamp-fmod(timestamp,time_window))/time_window;
    //add new item <secs_passed, new_count>
    if(time_logs->find(secs_passed)==time_logs->end()){
        struct frame_count new_count;
        (*time_logs)[secs_passed] = new_count;
    }

    //updates count of can_ID
    if(((time_logs->find(secs_passed))->second.data.find(can_ID)) == ((time_logs->find(secs_passed))->second.data.end()))
    {
        /* init count for current can_id */
        (*time_logs).find(secs_passed)->second.data[can_ID] = 1;
    }
    else
    {
        /* update count for this can_id */
        (*time_logs).find(secs_passed)->second.data[can_ID] += 1;
    }
}

