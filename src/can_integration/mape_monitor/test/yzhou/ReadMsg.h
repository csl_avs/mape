//
// Created by yzhou on 5/10/17.
//

#ifndef TEST_READMSG_H
#define TEST_READMSG_H

#endif //TEST_READMSG_H

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <time.h>
#include <map>
#include <chrono>
#include <vector>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>


#define MAX_CAN_DATA 8


typedef std::chrono::system_clock::time_point time_stamp_t;


typedef struct tMsgStructure {
    canid_t canID;
    double timestamp;
} MsgStructure;

struct frame_desc{
    canid_t id;
    uint8_t dlc;
    uint32_t speed;
    std::vector<uint16_t> data;
    uint8_t __paddings[3];
};

struct frame_count {
    std::map<canid_t, uint32_t> data; //can_id and its number
};

static uint32_t swap_byte_order(const uint32_t id);


MsgStructure* ReadMsg(const u_char *pkt, time_stamp_t star_time, time_stamp_t curr_time);
double compute_duration(time_stamp_t starTime, time_stamp_t endTime);

std::vector<uint32_t> construct_msg_repository(std::string filename);

void writ_to_file(std::map<uint32_t, struct frame_count> time_logs);

void add_to_log(double secs_passed, canid_t can_ID, std::map<uint32_t, struct frame_count> *time_logs,double time_window);

//TimeWindow is the time duration in second.