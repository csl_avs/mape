//
// Created by yzhou on 5/10/17.
//
#include <iostream>
//#include <pthread.h>
#include <malloc.h>
#include <algorithm>
#include <mutex>

#include "monitor.h"

/* Use std mutex to provide r/w lock/unlock operations */
//pthread_mutex_t count_mutex;
std::mutex rw_lock; 

static int counter = 0;
static char notInitialized = 1;


////////////////////////////////////////////////////////////


History* History_Constructor(double timestamp)
{
    History *retVal = (History*) malloc(sizeof(History));

    retVal->propositions = 0;

    retVal->timestamp = timestamp;

    return retVal;
}

void Current_Constructor(uint32_t msg, double timestamp, History *hist1)
{

    hist1->msgID=msg;
    hist1->timestamp=timestamp;
}

void History_Reset(History *h)
{
   h->propositions=0;
}

char History_Process(History *next, uint32_t CheckMsg, int lowbound, int upbound)
{

    int count = counter;
    if (next->msgID==CheckMsg)
    {
        if (count > upbound || count <lowbound)
        {
            next->propositions = 1;
        }
        else
        {
            counter = ++count;
            if (count > upbound || count <lowbound)
            {
                next->propositions = 1;
            }
            else
            {
                next->propositions = 0;
            }
        }
    }
    return next->propositions;
}

bool checkmsg(uint32_t msg, uint32_t * MsgRepository, std::size_t len)
{
    uint32_t * it=NULL;
    it = std::find(MsgRepository,MsgRepository+len-1,msg);
    if (it==MsgRepository+len-1){
        printf("The monitored message is wrong!\n");
        return false;
    }else
        return true;
}

// Additional function to clean up garbage
/*
void History_Dispose(History *h) {
    int i;

    if (h == NULL) return;
    for (i = 0; i < 1; i++)
    {
        kfree(h->propositions[0][i]);
    }
    kfree(h->propositions[0]);
    kfree(h->propositions);
    kfree(h);
}
 */

/*
int LogicDroid_Module_renewMonitorVariable(int msgID, double timestamp) {//, int varCount, char value, int rel

    if (notInitialized)
    {
        return 0;
    }

    mutex_lock(&lock);
    hist[1]->msgID=msgID;
    hist[1]->timestamp=timestamp;
    mutex_unlock(&lock);
    return 0;
}
 */


int LogicDroid_Module_initializeMonitor(History * hist0, History *hist1)
{

    // int i;
    // pthread_mutex_lock(&count_mutex);
    if (hist0 == NULL || hist1 == NULL)
    {
        return -1;
    }
    
    rw_lock.lock();
    
    hist0 = History_Constructor(0);
    hist1 = History_Constructor(0);
    History_Reset(hist0);
    History_Reset(hist1);

    //LogicDroid_setIDs(0, INTERNET_UID);
    notInitialized = 0;
    counter=0;
    
    rw_lock.unlock();

    //pthread_mutex_unlock(&count_mutex);
    //return module_policyID;
    return 0;
}

int LogicDroid_Module_checkEvent(uint32_t currentMsg, double timestamp, History *hist1, uint32_t checkmsg, int lowbound, int upbound)
{

    char result;

    if (notInitialized)
    {
        return 0;
    }

    //pthread_mutex_lock(&count_mutex);
    rw_lock.lock();

    Current_Constructor(currentMsg,timestamp,hist1);
    result = History_Process(hist1, checkmsg, lowbound, upbound);

    rw_lock.unlock();
    
    if (result)
    {
        //currentHist = !currentHist;
        std::cout << "Warning! Current message with a timestamp " << timestamp << "with respect to the current window is violated." << std::endl;
    }
    //pthread_mutex_unlock(&count_mutex);

    //rw_lock.unlock();
    return result;
}
