//#include "a.h"
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <time.h>
#include <map>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <vector>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include "monitor.h"
#include "ReadMsg.h"

typedef std::chrono::system_clock::time_point time_stamp_t;
typedef std::vector<uint16_t>::size_type vec_size_type;

#define CAN_DEV "can0"
#define MAX_PERIOD 1000

int main(int argc, char **argv)
{
    std::map<uint32_t,struct frame_count> time_log;
    std::string filename = "frames.cfg";
    std::vector<uint32_t> can_ID;
    uint32_t *msg_repository;
    std::size_t len;
    
    can_ID = construct_msg_repository(filename);
    msg_repository = &can_ID[0];
    len = can_ID.size();

    uint32_t check_msg = 0;

    double time_window = 60;
    int low_bound = 0;
    int up_bound = 4;
    double timestamp = 0;

    if (argc == 3)
    {
        std::stringstream input1(argv[1]), input2(argv[2]);
        input1 >> check_msg;
        input2 >> up_bound;

        std::cout << "msg_id: " << check_msg << ", up_bound: " << up_bound << std::endl;
    }

    if(!checkmsg(check_msg,msg_repository,len))
    {
        return 0;
    }
    
    time_stamp_t star_time, curr_time,first_start_time;
    
    History* hist0 = (History*) malloc(sizeof(History));
    History* hist1 = (History*) malloc(sizeof(History));
    
    MsgStructure* curr_msg = (MsgStructure*)malloc(sizeof(MsgStructure));

    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler

    hl = pcap_open_live(CAN_DEV, BUFSIZ, 1, 1000, err_buff);

    if (hl == NULL)
    {
        printf("Couldn't open device %s!\n", CAN_DEV);
        return -1;
    }

    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        printf("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return -1;
    }

    LogicDroid_Module_initializeMonitor(hist0,hist1);
    bool has_first_packet = false;

    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        curr_time=std::chrono::system_clock::now();

        if(has_first_packet== false)
        {
            first_start_time=curr_time;
            star_time=curr_time;
            has_first_packet= true;
            LogicDroid_Module_initializeMonitor(hist0,hist1);
            curr_msg=ReadMsg(pkt,star_time,curr_time);
            Current_Constructor(curr_msg->canID, 0, hist0); //useless, only set hist0;
            std::time_t now_c = std::chrono::system_clock::to_time_t(star_time);
            std::cout << "The star time of the current window is "<<std::put_time(std::localtime(&now_c),"%F %T")<<'\n';
        }
        double total_duration=compute_duration(first_start_time,curr_time);

        if(total_duration>MAX_PERIOD)
        {
            break;
        }

        curr_msg=ReadMsg(pkt,star_time,curr_time);

        if(curr_msg->timestamp > time_window)
        {
            //The current message is the first message for the new window
            star_time=curr_time;
            LogicDroid_Module_initializeMonitor(hist0,hist1);
            
            curr_msg->timestamp=0;
            Current_Constructor(curr_msg->canID, 0, hist0);//useless, only set hist0;
            
            std::time_t now_c = std::chrono::system_clock::to_time_t(star_time);
            
            std::cout << "The star time of the current window is "<<std::put_time(std::localtime(&now_c),"%F %T")<<'\n';
        }
        
        LogicDroid_Module_checkEvent(curr_msg->canID, curr_msg->timestamp, hist1, check_msg, low_bound, up_bound);
        add_to_log(total_duration,curr_msg->canID,&time_log,time_window);
    }

    writ_to_file(time_log);

    return 0;
}
