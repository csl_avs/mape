#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>

//#define DEBUG true
#define MAX_CAN_DATA 8
#define CAN_DEV "can0"

/* count packets sent */
static uint32_t packet_sent;
/* frame descption from input files */
struct frame_desc{
        uint16_t id;
        uint16_t dlc;
        uint16_t speed;
        std::vector<uint16_t> data;
};

/* big edian to little endian 
 * or little to big 
 */
static uint32_t swap_byte_order(const uint32_t id)
{
    return (id >> 24) | ((id << 8) & 0x00FF0000) |
        ((id >> 8) & 0x0000FF00) | (id << 24);
}

/* print can frame id in hex little edian format */
void print_can_id(const canid_t can_id)
{
    for (int i = 0; i <= 24; i+=8)
    {
        if(((can_id >> i) & 0xFF) != 0)
        {
            printf("%02X", (can_id >> i) & 0xFF);
        }
    }
    printf("h");
}

/* construct a can frame from its description */
void desc_2_frame(const struct frame_desc desc, struct can_frame &frame)
{
    frame.can_id = desc.id;
    frame.can_dlc = desc.dlc;

    for (int i = 0; i < frame.can_dlc; i++)
    {
        frame.data[i] = (uint8_t)desc.data[i];
    }
}

/* send a CAN frame over CAN socket */
int send_single_frame(const int can_socket, const struct can_frame frame)
{
    //socket_lock.lock();
    /* no is required as we will use a dedicated socket */
    int nbytes = write(can_socket, &frame, sizeof(frame));
    packet_sent++;
    if((packet_sent % 100) == 0)
    {
        printf("%d packets sent.\n", packet_sent);
    }
    //socket_lock.unlock();
    
    if (nbytes < 0)
    {
        //std::cout << boost::this_thread::get_id() << '\n';
        printf("socket sending error!\n");
        return -1;
    }
    
    //std::cout << boost::this_thread::get_id() << ": " << nbytes << " bytes sent.\n";
    return 0;
}

/* Description:
 * capture can frames with libpcap and update data loggings
 */
void can_frame_respond(const int socket, const std::map<canid_t, struct frame_desc> res_frames)
{
    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler

    /* open can bus interface for frame capturing */
    hl = pcap_open_live(CAN_DEV, BUFSIZ, 1, 1000, err_buff);
    if (hl == NULL)
    {
        /* open failed */
        printf("Couldn't open device %s!\n", CAN_DEV);
        return;
    }
    
    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        /* data is not a socketCAN frame */
        printf("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return;
    }

    struct can_frame *can_f;
    int dlc;
    int num_pkt = 0;
    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        can_f = (struct can_frame *)pkt;

        /* count how many frames received */
        num_pkt++;
        
#ifdef DEBUG
        print_can_id(can_f->can_id);

        /* get data length and print data segment */
        dlc = (can_f->can_dlc > 8)?8: can_f->can_dlc;
        printf(", DATA ");
        for (int i = 0; i < dlc; i++)
        {
            printf("%02X ", can_f->data[i]);
        }
        printf(", Count %d\r\n", pkt_history[can_f->can_id]);
#endif

        /* if the response frame is found */
        uint32_t new_id = swap_byte_order(can_f->can_id);
        if (res_frames.find(new_id) != res_frames.end())
        {
            struct can_frame new_frame;
            
            desc_2_frame(res_frames.find(new_id)->second, new_frame);
            /* send response frame */
            send_single_frame(socket, new_frame);
        }
    }
}

int main(int argc, char **argv)
{
    int sock_can;
    struct sockaddr_can addr_can;
    struct ifreq ifr_can;

    /* create a raw socket for CAN */
    if((sock_can = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        printf("Error happened in creating CAN socket!\n");
        exit(-1);
    }

    /* find and return CAN interface, saved in ifr_can */
    //ifr_can.ifr_name = {'/0'};
    strncpy(ifr_can.ifr_name, CAN_DEV, IFNAMSIZ-1);
    ifr_can.ifr_name[IFNAMSIZ-1] = '\0';
    //ifr_can.ifr_ifindex = if_nametoindex(ifr_can.ifr_name);
    if(ioctl(sock_can, SIOCGIFINDEX, &ifr_can) < 0)
    {
        printf("ioctl Error! errid: %d, if_nametoindex %d\n", errno, if_nametoindex(ifr_can.ifr_name));
        exit(-1);
    }

    addr_can.can_family = AF_CAN;
    addr_can.can_ifindex = ifr_can.ifr_ifindex;
    //addr_can.can_ifindex = 0;

    /* bind socket to the CAN interface */
    if(bind(sock_can, (struct sockaddr *)&addr_can, sizeof(addr_can)) < 0)
    {
        printf("bind error! id %d\n", errno);
        exit(-1);
    }

    /* open response frames cfg */
    std::fstream frame_res("response.cfg");
    if(!frame_res.is_open())
    {
        std::cout << "error open response.cfg" << std::endl;
        exit(-1);
    }

    /* record sedning frames and their corresponding response frame */
    std::map<canid_t, struct frame_desc> res_frames_map;
    char line[64] = {'\0'};
    
    /* parse response.cfg */
    while(frame_res.getline(line, sizeof(line)))
    {
        /* (c++11) if the current line is empty */
        if(*line == '\0')
        {
            break;
        }
        /* use stringstram to parse line into frame_desc */
        std::stringstream s_stream(line);
        struct frame_desc item;
        canid_t id_in;

        /* each res_frame line is represented as: 
         * CAN_ID RES_ID DATA_LENGTH SPEED_FACTOR DATA 
         */
        s_stream >> id_in >> item.id >> item.dlc >> item.speed;
        item.dlc = (item.dlc > MAX_CAN_DATA)? MAX_CAN_DATA : item.dlc;
        
        for (int i = 0; i < item.dlc; i++)
        {
            uint16_t hex_data;
            s_stream >> hex_data;
            item.data.push_back(hex_data);
        }

        s_stream.clear();
        
        if(res_frames_map.find(id_in) != res_frames_map.end())
        {
            std::cout << "Duplicated response frame for CAN_ID %d" << id_in << std::endl;
            std::cout << "Response frame will be overwritten." << std::endl;
        }
        
        res_frames_map[id_in] = item;
    }
    
#define DEBUG
    std::map<canid_t, struct frame_desc>::iterator iter;
    for (iter = res_frames_map.begin(); iter != res_frames_map.end(); iter++)
    {
        std::cout << iter->first << iter->second.id << iter->second.dlc << std::endl;
    }
#endif
    
    can_frame_respond(sock_can, res_frames_map);
    close(sock_can);
    return 0;
}
