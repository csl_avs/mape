#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <map>
#include <chrono>
#include <vector>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#define DEBUG true
#define CAN_DEV "can0"
#define MAX_PERIOD_PACKET 1000
#define MAX_PROBE_NUM 10*1000

typedef std::map<canid_t, double> freq_t;
typedef std::map<canid_t, uint> can_num_t;
typedef std::chrono::duration<double> duration_t;
//typedef std::chrono::microseconds duration_t;
typedef std::chrono::system_clock::time_point time_stamp_t;
//static std::map<canid_t, unsigned int> freq_history;

static can_num_t pkt_history, pkt_period;
static freq_t freq_history, freq_period;
static duration_t time_passed, period_time;
static time_stamp_t time_start, period_start;
static bool first_packet;

struct frame_log
{
    time_stamp_t last_stamp;
    uint32_t lower_bound;
    /* count[i] indicates how manay frames received during
     * [lower_bound + i * 10, lower_bound + (i+1)*10)
     */
    uint32_t count[10];
};

/* big edian to little endian 
 * or little to big 
 */
static uint32_t swap_byte_order(const uint32_t id)
{
    return (id >> 24) | ((id << 8) & 0x00FF0000) |
        ((id >> 8) & 0x0000FF00) | (id << 24);
}

/* print can frame id in hex little edian format */
void print_can_id(const canid_t can_id)
{
    for (int i = 0; i <= 24; i+=8)
    {
        if(((can_id >> i) & 0xFF) != 0)
        {
            printf("%02X", (can_id >> i) & 0xFF);
        }
    }
    printf("h");
}

static void update_freq_history(void)
{
    /* 
     * define a iterator to parse frame count 
     * key: first, value: second
     */
    std::map<canid_t, uint>::iterator iter;
#ifdef DEBUG
    printf("Dump history frequency\n");
#endif
    
    for(iter = pkt_history.begin(); iter != pkt_history.end(); iter++)
    {
        freq_history[iter->first] = ( iter->second / time_passed.count());
#ifdef DEBUG
        printf("ID: ");
        print_can_id(iter->first);
        printf(", FREQ: %lf\n", freq_history[iter->first]);
#endif
    }
}

static void update_freq_period(void)
{
    std::map<canid_t, uint>::iterator iter;
    #ifdef DEBUG
    printf("Dump period frequency\n");
#endif
    
    for(iter = pkt_period.begin(); iter != pkt_period.end(); iter++)
    {
        freq_period[iter->first] = ( iter->second / period_time.count());
#ifdef DEBUG
        printf("ID: ");
        print_can_id(iter->first);
        printf(", FREQ: %lf\n", freq_period[iter->first]);
#endif
    }

    /* clear current period data */
    pkt_period.clear();
}

static void check_can_id(const canid_t can_id)
{
    if (can_id & CAN_ERR_FLAG)
    {
        //printf("ID: %d ", can_f->can_id & (CAN_ERR_MASK|CAN_ERR_FLAG));
        print_can_id(can_id & (CAN_ERR_MASK|CAN_ERR_FLAG));
    }
    else if (can_id & CAN_EFF_FLAG)
    {
        //printf("ID %d ", can_f->can_id & CAN_EFF_MASK);
        print_can_id(can_id & CAN_EFF_MASK);
    }
    else if (can_id & CAN_RTR_FLAG)
    {
        print_can_id(can_id);
    }
    else
    {
        //printf ("ID %d ", can_f->can_id & CAN_SFF_MASK);
        print_can_id(can_id & CAN_SFF_MASK);
    }
}

/* Description:
 * capture can frames with libpcap and update data loggings
 */
void can_frame_capturer(void)
{
    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler

    /* open can bus interface for frame capturing */
    first_packet = false;
    hl = pcap_open_live(CAN_DEV, BUFSIZ, 1, 1000, err_buff);
    if (hl == NULL)
    {
        /* open failed */
        printf("Couldn't open device %s!\n", CAN_DEV);
        return;
    }
    
    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        /* data is not a socketCAN frame */
        printf("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return;
    }

    struct can_frame *can_f;
    int dlc;
    int num_pkt = 0;
    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        /* record the time stamp when we received the first frame */
        if (num_pkt == 0)
        {
            period_start = std::chrono::system_clock::now();
        }
        if (!first_packet)
        {
            time_start = std::chrono::system_clock::now();
            first_packet = true;
        }
        can_f = (struct can_frame *)pkt;

        /* count how many frames received */
        num_pkt++;
        /* check can frame type 
        check_can_id(can_f->can_id);
        */
        
        /* update can_id's frame count */
        pkt_history[can_f->can_id] += 1;
        pkt_period[can_f->can_id] += 1;
        
#ifdef DEBUG
        print_can_id(can_f->can_id);

        /* get data length and print data segment */
        dlc = (can_f->can_dlc > 8)?8: can_f->can_dlc;
        printf(", DATA ");
        for (int i = 0; i < dlc; i++)
        {
            printf("%02X ", can_f->data[i]);
        }
        printf(", Count %d\r\n", pkt_history[can_f->can_id]);
#endif
        
        /* if MAX_PERIOD_PACKET frames are received then update the history frequency */
        if (num_pkt == MAX_PERIOD_PACKET)
        {
            num_pkt = 0;
            time_passed = std::chrono::system_clock::now() - time_start;
            period_time = std::chrono::system_clock::now() - period_start;
            update_freq_history();
            update_freq_period();
        }
    }
}

void init_frame_log(struct frame_log &new_log, const time_stamp_t stamp)
{
    new_log.last_stamp = stamp;
    new_log.lower_bound = 0;
    for (int i = 0; i< 10; i++)
    {
        new_log.count[i] = 0;
    }
}

int main(int argc, char **argv)
{
    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler
    
    first_packet = false;
    hl = pcap_open_live(CAN_DEV, BUFSIZ, 1, 1000, err_buff);
    if (hl == NULL)
    {
        /* open failed */
        printf("Couldn't open device %s!\n", CAN_DEV);
        return -1;
    }
    
    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        /* data is not a socketCAN frame */
        printf("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return -1;
    }

    struct can_frame *can_f;
    int dlc;
    int num_pkt = 0;
    std::map<canid_t, struct frame_log> can_logs;
    
    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        /* reord time stamp */        
        time_stamp_t cur_stamp = std::chrono::system_clock::now();
        can_f = (struct can_frame *)pkt;
        canid_t new_id = swap_byte_order(can_f->can_id);
        
        /* if it's the first time this frame is captured */
        if (can_logs.find(new_id) == can_logs.end())
        {
            struct frame_log new_log;
            init_frame_log(new_log, cur_stamp);

            /* update count */
            new_log.count[0] += 1;

            /* add to the logs */
            can_logs[new_id] = new_log;
        }
        else
        {
            /*
            printf("ID: %d ", new_id);
            std::cout << "last: " << std::chrono::system_clock::to_time_t(can_logs[new_id].last_stamp);
            std::cout << " current: " << std::chrono::system_clock::to_time_t(cur_stamp);
            */
            duration_t interval = cur_stamp - can_logs[new_id].last_stamp;
            //uint32_t hun = interval.count() * 1000 / 100;
            uint32_t hun = interval.count() * 10;
            uint32_t index = (interval.count() * 1000 - (hun * 100)) / 10;
            //printf(", hun:%d\n", hun);
            /* update can_logs */
            if( can_logs[new_id].lower_bound == 0)
            {
                can_logs[new_id].lower_bound = hun * 100;
            }
            else if ( can_logs[new_id].lower_bound != hun * 100)
            {
                std::cout << "id " << new_id << ": lower bound changed!\n";
                std::cout <<"new: " << hun*100 << ", old: " << can_logs[new_id].lower_bound << std::endl;
            }
            can_logs[new_id].count[index] += 1;
            can_logs[new_id].last_stamp = cur_stamp;
        }

        num_pkt++;
        if(!(num_pkt < MAX_PROBE_NUM))
        {
            break;
        }
    }

    std::map<canid_t, struct frame_log>::iterator iter;
    for(iter = can_logs.begin(); iter != can_logs.end(); iter++)
    {
        printf("ID: %d, lower bound: %d ", iter->first, iter->second.lower_bound);
        uint32_t all_nums = 0;
        for(int i = 0; i < 10; i++)
        {
            printf("count[%d]: %d, ", i, iter->second.count[i]);
            all_nums += iter->second.count[i];
        }
        printf("nums: %d\n", all_nums);
    }

    return 0;
}
