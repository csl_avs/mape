#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <time.h>
#include <map>
#include <chrono>
#include <vector>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#define DEBUG true
#define CAN_DEV "can0"
#define MAX_PERIOD 1000
#define MAX_PROBE_NUM 1*1000
#define TIME_UNIT 1

typedef std::chrono::duration<double> duration_t;
typedef std::chrono::system_clock::time_point time_stamp_t;

static duration_t time_passed;
static time_stamp_t time_start, cur_stamp;
static bool first_packet;
static uint32_t MAX_FRAMES;
static double max_period_time;
/* big edian to little endian 
 * or little to big 
 */
static uint32_t swap_byte_order(const uint32_t id)
{
    return (id >> 24) | ((id << 8) & 0x00FF0000) |
        ((id >> 8) & 0x0000FF00) | (id << 24);
}

/* print can frame id in hex little edian format */
void print_can_id(const canid_t can_id)
{
    for (int i = 0; i <= 24; i+=8)
    {
        if(((can_id >> i) & 0xFF) != 0)
        {
            printf("%02X", (can_id >> i) & 0xFF);
        }
    }
    printf("h");
}

static void check_can_id(const canid_t can_id)
{
    if (can_id & CAN_ERR_FLAG)
    {
        //printf("ID: %d ", can_f->can_id & (CAN_ERR_MASK|CAN_ERR_FLAG));
        print_can_id(can_id & (CAN_ERR_MASK|CAN_ERR_FLAG));
    }
    else if (can_id & CAN_EFF_FLAG)
    {
        //printf("ID %d ", can_f->can_id & CAN_EFF_MASK);
        print_can_id(can_id & CAN_EFF_MASK);
    }
    else if (can_id & CAN_RTR_FLAG)
    {
        print_can_id(can_id);
    }
    else
    {
        //printf ("ID %d ", can_f->can_id & CAN_SFF_MASK);
        print_can_id(can_id & CAN_SFF_MASK);
    }
}

int main(int argc, char **argv)
{
    char err_buff[PCAP_ERRBUF_SIZE];
    const u_char *pkt; // packet
    struct pcap_pkthdr header;
    pcap_t *hl; // capture handler

    //MAX_FRAMES = MAX_PROBE_NUM;
    max_period_time = MAX_PERIOD;
    if (argc == 2)
    {
        std::stringstream input(argv[1]);
        input >> max_period_time; // seconds
        printf("***** Test logging limit will be %lf.\n", max_period_time);
    }
    else
    {
        /*
        printf("***** Usage: you can customize upper packet bound with:\n");
        printf("***** ./logger2.o NUM_PACKETS\n");
        printf("***** E.g. ./logger2.o 1000000\n");
        printf("***** Now the limit will be %d\n", MAX_PROBE_NUM);
        */
        printf("***** Usage: you can customize upper logging time with:\n");
        printf("***** ./logger2.o MAX_TIME_SECONDS\n");
        printf("***** E.g. ./logger2.o 100\n");
        printf("***** Now the time limit will be %lf\n", (double)MAX_PERIOD);
    }
    
    first_packet = false;
    hl = pcap_open_live(CAN_DEV, BUFSIZ, 1, 1000, err_buff);
    if (hl == NULL)
    {
        /* open failed */
        printf("Couldn't open device %s!\n", CAN_DEV);
        return -1;
    }
    
    if (pcap_datalink(hl) != DLT_CAN_SOCKETCAN)
    {
        /* data is not a socketCAN frame */
        printf("Device doesn't use socketCAN, it use %d!\n", pcap_datalink(hl));
        return -1;
    }

    struct can_frame *can_f;
    int dlc;
    int num_pkt = 0;

    /* <can_id, frame_number> strcuture */
    struct frame_count
    {
        std::map<canid_t, uint32_t> data;
    };

    /* data log is constructed like:
     * Time_unit_1 --> <can_id_1, frame_num>
     *             --> <can_id_2, frame_num>
     * ...
     * Time_unit_n --> <can_id_1, frame_num>
     *             --> <can_id_2, frame_num>
     */
    /* <time_unit, frame_count> structre */
    std::map<uint32_t, struct frame_count> time_logs;
    std::cout << "waiting..." << std::endl;
    
    while((pkt = pcap_next(hl, &header))!=NULL)
    {
        if (first_packet == false)
        {
            time_start = std::chrono::system_clock::now();
            first_packet = true;
            std::cout << "started" << std::endl;
        }
        /* reord time stamp */
        cur_stamp = std::chrono::system_clock::now();
        can_f = (struct can_frame *)pkt;
        
        canid_t new_id = swap_byte_order(can_f->can_id);

        /* calculate current time_unit */
        time_passed = cur_stamp - time_start;
        uint32_t secs_passed = time_passed.count() / TIME_UNIT;

        
        if(time_logs.find(secs_passed) == time_logs.end())
        {
            /* if there is no log for current time_unit period
             * then create a log item
             */
            struct frame_count new_count;
            time_logs[secs_passed] = new_count;
        }

        /* if there is no count for new_id in current period 
         * then initial the count data
         * or (else) update the count
         */
        if((time_logs.find(secs_passed)->second.data.find(new_id)) == (time_logs.find(secs_passed)->second.data.end()))
        {
            /* init count for current can_id */
            time_logs.find(secs_passed)->second.data[new_id] = 1;
        }
        else
        {
            /* update count for this can_id */
            time_logs.find(secs_passed)->second.data[new_id] += 1;
        }

        /* update total packet count */
        num_pkt++;
        /*
        if(!(num_pkt < MAX_FRAMES))
        {
            break;
        }
        */
        if(!(time_passed.count() < max_period_time))
        {
            break;
        }
    }

    time_t now = time(NULL);
    struct tm *aTime = localtime(&now);
    /* get current hours and minutes to prepare logging file */
    std::string file_name = "result_" + std::to_string(aTime->tm_hour) + "_" + std::to_string(aTime->tm_min) + ".txt";  
    std::ofstream f_out(file_name);

    /* parse logging dataset to output to the result file */
    std::map<uint32_t, struct frame_count>::iterator iter1;
    for(iter1 = time_logs.begin(); iter1 != time_logs.end(); iter1++)
    {
        f_out << "Time " << iter1->first;
        std::map<canid_t, uint32_t>::iterator iter2;
        
        for(iter2 = iter1->second.data.begin(); iter2 != iter1->second.data.end(); iter2++)
        {
            f_out << ", can_id " << iter2->first << " count: " << iter2->second;
        }
        f_out << std::endl;
    }
    
    return 0;
}
