/* define corporation class for local units */
#include <iostream>
#include <cstdlib>
#include <ctype.h>
#include <stdint.h>
#include <vector>
#include <map>
#include <deque>
#include <mutex>

#include <ros/ros.h>

//#include <mape_analyzer/corporate.h>
#include <acronis_common/BadNodes.h>
#include <acronis_common/DisBeacon.h>
#include <acronis_common/FailureReq.h>
#include <acronis_common/CommCH.h>
/* DisBeacon.h defines the message used in corporation to exchange failure descriptions
 * including: upstream nodes' IDs and fault queues
 */

class Unit{
    std::deque<uint32_t> fault_queue;
    std::mutex fq_lock; // lock for fault_queue

    ros::NodeHandle unit_hdl;
    ros::Publisher msg_pb; // ros publisher for sending message 
    ros::Publisher plan_pub; // publish msg for planners
    ros::Publisher req_pub; // publish failure assertion request

    /* <upstream_id, subscriber> */
    std::map<uint32_t, ros::Subscriber> sb_list;
    std::mutex sbl_lock;
    
    bool sick_bit;
    std::mutex sb_lock; // lock for sick_bit
    
    uint32_t self_id;

    std::vector<uint32_t> upstreams, downstreams;
    std::mutex up_lock, down_lock;

    /* we keep a <id, sync_bit> pair for each upstream unit
     * once a local fault is deteted, local unit should ask 
     * upstream units for faiilure status (sync) in case of comm delay
     * this sync_bit is to indicate if at least one reply has been got
     * from each upstream unit
     */
    std::map<uint32_t, bool> sync_map;
    std::mutex sm_lock;

    void send_beacon_msg(void);

    bool is_synched(void);

    void sync_failure_req(void);

    void call_planner(void);

public:
    /* user should provide 
     *  node ID 
     *  and message publishers 
     *  to initialize an instance */
    template<class Container>
    Unit(const uint32_t &self_id, const ros::Publisher &pb1, const ros::Publisher &pb2, const ros::Publisher &pb3, const ros::NodeHandle gnh, const Container &up, const Container &down, const bool flag);

    template<typename T> 
    void update_upstreams(const std::vector<T> &list, const bool flag);

    template<typename T>
    void update_downstreams(const std::vector<T> &list);

    /* operation taken once a local failure is detected */
    void fault_detected(void);

    /* after failure is resolved */
    void fault_clear(void);

    /* update local knowledge about failure path */
    void update_data_hdl(const acronis_common::DisBeaconConstPtr &ptr);

    /* handle failure assertion request */
    void failure_req_hdl(const acronis_common::FailureReqConstPtr &ptr);

    /* listen and handle common_channel */
    void comm_ch_hdl(const acronis_common::CommCHConstPtr &ptr);
};

/* if we put it in seperate .cpp then an "undefined reference" error 
 * will pop up when it's being invoked
 */
template<typename T>
void Unit::update_upstreams(const std::vector<T> &list, const bool flag)
{
    // should check typeid(T).name()
    this->upstreams.assign(list.begin(), list.end());

    if (flag)
    {
        // initialize sync_map
        for (uint i = 0; i < this->upstreams.size(); i++)
        {
            /* <node_id, false> */
            this->sync_map[upstreams[i]] = false;
        }
    }
}

template<typename T>
void Unit::update_downstreams(const std::vector<T> &list)
{
    this->downstreams.assign(list.begin(), list.end());
}

template<class Container>
Unit::Unit(const uint32_t &self_id, const ros::Publisher &pb1, const ros::Publisher &pb2, const ros::Publisher &pb3, const ros::NodeHandle gnh, const Container &up, const Container &down, const bool flag)
{
    this->sick_bit = false;
    this->self_id = self_id;
    this->msg_pb = pb1;
    this->plan_pub = pb2;
    this->req_pub = pb3;
    this->unit_hdl = gnh;

    this->upstreams.assign(up.begin(), up.end());
    if (flag)
    {
        // initialize sync_map
        for (uint i = 0; i < this->upstreams.size(); i++)
        {
            /* <node_id, false> */
            this->sync_map[upstreams[i]] = false;
        }
    }

    this->downstreams.assign(down.begin(), down.end());

    // add beacon subscribers
    for(uint i = 0; i < this->upstreams.size(); i++)
    {
        this->sb_list[this->upstreams[i]] = this->unit_hdl.subscribe("unit_beacon_"+std::to_string(this->upstreams[i]), 1, &Unit::update_data_hdl, this);
    }
    // add sync_req subscribers
    for(uint i = 0; i < this->downstreams.size(); i++)
    {
        this->sb_list[this->downstreams[i]] = this->unit_hdl.subscribe("unit_req_"+std::to_string(this->downstreams[i]), 1, &Unit::failure_req_hdl, this);
    }
}
