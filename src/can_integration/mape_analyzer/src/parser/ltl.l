%option noyywrap
%option header-file="scanner.h"
%{
  #include <stdio.h>
  #include <stdlib.h>
  #include "ltl.tab.hpp"

  #define YYDECL int yylex()
%}

%%
[ \t]                        ;
(0[xX][0-9a-fA-F]+)            {yylval.uint_t = strtol(yytext, NULL, 16); return MSG;}
([0-9]+(\.[0-9]+)?)            {yylval.float64_t = atof(yytext); return NUM;}
"<"                          {return T_SMALLER;}
"-"                          {return T_MINUS;}
"^"                          {return R_AND;}
\n                           {return L_EOF;}
%%

