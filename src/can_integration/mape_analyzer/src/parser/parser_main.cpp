#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "abc.h"
#include "ltl.tab.hpp"
#include "scanner.h"

// extern FILE *yyin;
  
// typedef struct yy_buffer_state * YY_BUFFER_STATE;
// extern YY_BUFFER_STATE yy_scan_string(const char *str); 

// int yylex(void);
// int yyparse();
// void yyerror(const char *);
// int check(const double msg, const double uphold);

char* rule_str;

enum Checker:uint32_t {
    Threshold,
    Compare
};

typedef struct {
    Checker checker_name;
    uint32_t msg_id1;
    uint32_t msg_id2;

    double threshold;
} rule_t;

std::vector<rule_t> rule_list;

int check(const unsigned int msg, const double uphold)
{
    rule_t new_rule;
    new_rule.checker_name = Threshold;
    new_rule.msg_id1 = msg;
    new_rule.threshold = uphold;

    rule_list.push_back(new_rule);

    return 1;
}

int check_diff(const unsigned int msg1, const unsigned int msg2, const double uphold)
{
    rule_t new_rule;
    new_rule.checker_name = Compare;
    new_rule.msg_id1 = msg1;
    new_rule.msg_id2 = msg2;
    new_rule.threshold = uphold;

    rule_list.push_back(new_rule);

    return 1;
}

int check_rule(void)
{
    for(int i = 0; i < rule_list.size(); i++)
    {
        switch(rule_list[i].checker_name)
        {
            case Threshold:
                if (rule_list[i].msg_id1 * 10 < rule_list[i].threshold)
                {
                    std::cout << "rule " << i << " check succeeded!"<< std::endl;
                }
                else{
                    std::cout << "rule " << i << " check failed!"<< std::endl;
                }
                break;
            case Compare:
                if (abs(rule_list[i].msg_id1 * 10 - rule_list[i].msg_id2 * 10) < rule_list[i].threshold)
                {
                    std::cout << "rule " << i << " check succeeded!"<< std::endl;
                }
                else{
                    std::cout << "rule " << i << " check failed!"<< std::endl;
                }
                break;
            default:
                ;    
        }
    }

    return 0;
}

int main(int argc, char **argv)
{
    std::string input;
    //std::cin >> input;
    std::getline(std::cin, input);

    input.append("\n\0\0", 3);
    int length = input.length();
    rule_str = (char *)malloc(length);
    // rule_str[length] = '\n';
    // rule_str[length+1] = '\0';
    // rule_str[length+2] = '\0';
    strncpy(rule_str, input.c_str(), length);
    /*
    std::cout << "input is: " << rule_str << std::endl;
    std::cout << "input length is: " << length << std::endl;
    std::cout << "c_str is: " << input.c_str() << std::endl;
    std::cout << "c_str length is: " << sizeof(input.c_str()) << std::endl;
    */
    yy_scan_string(rule_str);
    yyparse();

    check_rule();

    return 0;
}
