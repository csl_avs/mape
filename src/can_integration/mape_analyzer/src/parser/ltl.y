%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <math.h>
  #include <ctype.h>
  #include <string.h>
  #include "abc.h"

  extern FILE *yyin;
  
  typedef struct yy_buffer_state * YY_BUFFER_STATE;
  extern YY_BUFFER_STATE yy_scan_string(const char *str); 

  int yylex(void);
  int yyparse();
  void yyerror(const char *);
%}

//                      %define api.value.type {double}
//                      %define api.value.type {unsigned int}
%union{
    unsigned int uint_t;
    double float64_t;
}

%token  <float64_t> NUM
%token  <uint_t> MSG
%token T_SMALLER T_MINUS R_AND L_EOF
%left T_SMALLER T_MINUS R_AND

%type   <float64_t>        exp


%%
input:
  %empty
| input line
;

line:
  L_EOF
| exp L_EOF { /* printf("%.10g\n", $1);*/ ; }
;
/*
exp:
  NUM '<' NUM         { $$ = ($1 < $3)?1:0;}
| exp '^' exp { $$ = $1 && $3;}
;
*/

exp:            
  MSG T_SMALLER NUM    { $$ = check($1, $3);}
| MSG T_MINUS MSG T_SMALLER NUM {$$ = check_diff($1, $3, $5); }       
| exp R_AND exp        { $$ = ($1) && ($3);}
;
%%

void yyerror(const char *s)
{
    fprintf(stderr, "%s\n", s);
}
