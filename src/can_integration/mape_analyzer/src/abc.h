/* integration with flex/bison based rule parser 
 */
extern char* rule_str;

int check(const unsigned int msg, const double uphold);
int check_diff(const unsigned int msg1, const unsigned int msg2, const double uphold);
