#include <iostream>
#include <ctype.h>

enum Checker:uint32_t {
    Threshold,
    Compare
};

typedef struct {
    Checker checker_name;
    uint32_t msg_id1;
    uint32_t msg_id2; // reserved

    double threshold;

} rule_t;
