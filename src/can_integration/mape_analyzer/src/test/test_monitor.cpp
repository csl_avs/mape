#include <iostream>
#include <ros/ros.h>
#include <mutex>
#include <thread>
#include <chrono>

#include <boost/thread.hpp>
#include <boost/chrono.hpp>

#include <boost/thread.hpp>

#include <std_msgs/Bool.h>
#include <acronis_common/TestPlan.h>
#include <acronis_common/TestStatus.h>

int unit_id;
int monitor_delay;
ros::Publisher pb;
bool sim_failure;
std::mutex sf_lock;


void unit_failure_cb(const acronis_common::TestPlanConstPtr &ptr)
{
    if (ptr->solved)
    {
        sf_lock.lock();
        sim_failure = false;
        sf_lock.unlock();
    }
}

void inject_failure(ros::Rate &rate)
{
    if (monitor_delay > 0)
    {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(monitor_delay));
    }
    
    while(ros::ok())
    {
       
        //sf_lock.lock();
        // if (sim_failure)
        // {
        acronis_common::TestStatus new_failure;
        new_failure.header.stamp = ros::Time::now();
        new_failure.header.frame_id = "test_monitor_"+std::to_string(unit_id);
        
        sf_lock.lock();
        new_failure.failed = sim_failure;
        sf_lock.unlock();

        pb.publish(new_failure);
        // }

        ros::spinOnce();
        rate.sleep();

        //sleep for 1 seconds
        //std::this_thread::sleep_for(std::chrono::seconds(3));
        }
}

void start_beacon_cb(const std_msgs::BoolConstPtr &ptr)
{
    ros::Rate rate(10);
    
    if(ptr->data)
    {
        inject_failure(rate);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_monitor");

    ros::NodeHandle nh("~"), g_nh;;

    bool flag = false;
    sim_failure = false;

    if(nh.hasParam("unit_id"))
    {
        nh.getParam("unit_id", unit_id);
    }else{
        ROS_WARN("A unique ID should be assigned to different partition analyzer!\n");
    }

    if(nh.hasParam("sim_failure"))
    {
        nh.getParam("sim_failure", sim_failure);
    }

    if(nh.hasParam("monitor_delay"))
    {
        nh.getParam("monitor_delay", monitor_delay);
    }

    if(g_nh.hasParam("listen_beacon"))
    {
        g_nh.getParam("listen_beacon", flag);
        std::cout << "flag is " << flag << std::endl;
    }


    
    pb = g_nh.advertise<acronis_common::TestStatus>("Test_stats_"+std::to_string(unit_id), 10);

    ros::Subscriber sb = g_nh.subscribe("Test_resolve_"+std::to_string(unit_id), 10, unit_failure_cb);

    ros::Rate loop(10);

    ros::Subscriber bsb;

    if(flag)
    {
        // listen to beacon;
        std::cout << "subscribe to /start_beacon" << std::endl;
        bsb = g_nh.subscribe("start_beacon", 1, start_beacon_cb);
    }
    else{
        inject_failure(loop);
    }
    //boost::thread *injector = new boost::thread(inject_failure);
    
    ros::spin();

    // ros::Rate loop_rate(20);

    // while(ros::ok())
    // {
    //     acronis_common::FailureReq new_req;
    //     new_req.header.stamp = ros::Time::now();
    //     new_req.self_id = 5;
    //     new_req.target.push_back(4);

    //     pb.publish(new_req);

    //     ros::spinOnce();

    //     loop_rate.sleep();
        
    // }

    return 0;
}
