#include <iostream>
#include <ros/ros.h>

#include <acronis_common/FailureReq.h>
#include <std_msgs/Bool.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_start");

    ros::NodeHandle nh;

    ros::Publisher pb = nh.advertise<std_msgs::Bool>("start_beacon", 10);

    ros::Rate loop_rate(0.5);

    while(ros::ok())
    {
        // acronis_common::FailureReq new_req;
        // new_req.header.stamp = ros::Time::now();
        // new_req.self_id = 5;
        // new_req.target.push_back(4);
        std_msgs::Bool new_req;
        new_req.data = true;        

        pb.publish(new_req);

        //loop_rate.sleep();

        ros::spinOnce();

        loop_rate.sleep();
        
    }

    return 0;
}
