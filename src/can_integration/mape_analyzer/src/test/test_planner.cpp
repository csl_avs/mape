#include <iostream>
#include <ros/ros.h>

#include <boost/thread.hpp>
#include <boost/chrono.hpp>

#include <acronis_common/TestPlan.h>
#include <acronis_common/BadNodes.h>

#define PLAN_INTERVAL 1500 // ms

int unit_id;
int plan_interval; // in ms
ros::Publisher pb;


void unit_failure_cb(const acronis_common::BadNodesConstPtr &ptr)
{
    boost::this_thread::sleep_for(boost::chrono::milliseconds(plan_interval));
    
    if (std::find(ptr->failures.begin(), ptr->failures.end(), std::to_string(unit_id)) != ptr->failures.end())
    {
        acronis_common::TestPlan new_plan;
        new_plan.header.stamp = ros::Time::now();
        new_plan.header.frame_id = "test_plan_"+std::to_string(unit_id);
        new_plan.solved = true;

        pb.publish(new_plan);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_planner");

    ros::NodeHandle nh("~"), g_nh;;

    if(nh.hasParam("unit_id"))
    {
        nh.getParam("unit_id", unit_id);
    }else{
        ROS_WARN("A unique ID should be assigned to different partition analyzer!\n");
    }

    if(nh.hasParam("plan_interval"))
    {
        nh.getParam("plan_interval", plan_interval);
    }else{
        plan_interval = PLAN_INTERVAL;
    }

    pb = g_nh.advertise<acronis_common::TestPlan>("Test_resolve_"+std::to_string(unit_id), 10);

    ros::Subscriber sb = g_nh.subscribe("unit_failure_"+std::to_string(unit_id), 10, unit_failure_cb);

    ros::spin();

    // ros::Rate loop_rate(20);

    // while(ros::ok())
    // {
    //     acronis_common::FailureReq new_req;
    //     new_req.header.stamp = ros::Time::now();
    //     new_req.self_id = 5;
    //     new_req.target.push_back(4);

    //     pb.publish(new_req);

    //     ros::spinOnce();

    //     loop_rate.sleep();
        
    // }

    return 0;
}
