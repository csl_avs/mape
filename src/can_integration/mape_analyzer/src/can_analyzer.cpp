#include <stdio.h>
#include <iostream>
#include <map>
#include <mutex>
#include <stdlib.h>
#include <string.h>
#include <ros/ros.h>
#include <ros/console.h>
/*
#include <mape_msg/CANFreq.h>
#include <mape_msg/Uint32_Float64.h>
#include <mape_msg/SWMsg.h>
#include <mape_msg/SWCache.h>
*/
#include <acronis_common/CANFreq.h>
#include <acronis_common/Uint32_Float64.h>
#include <acronis_common/SWMsg.h>
#include <acronis_common/SWCache.h>

/* parser */
#include "abc.h"
#include "ltl.tab.hpp"
#include "scanner.h"
#include "parse-util.h"

/* subscribed topics' names */
#define CAN_MONITOR_TOPIC1 "CAN_Freq_history"
#define CAN_MONITOR_TOPIC2 "CAN_Freq_period"
#define CAN_MONITOR_TOPIC3 "CAN_SW"

/* for a given can frame, if its period frequency is more than
 * (FREQ_DIFF-1) times of its history frequency, then
 * it's peobably a spoofing frame
 */
#define FREQ_DIFF 2
/* frequency up limit */
#define FREQ_THRESHOLD 999

/* frame count threshold 
 * a frame count cannot exceed the NUM_UPLIMIT 
 * in a sliding window
 */
#define NUM_UPLIMIT 400

/* can_freq_xxx will be updated and read by different functions
 * use lock to protect the operation
 */
static std::map<uint32_t, double> can_freq_history, can_freq_period;
static std::mutex history_lock, period_lock;

static int MAX_FRAMES_COUNT;

void analyze_can_bus(void);

//void history_callback(const mape_msg::CANFreqConstPtr &msg_ptr)
void history_callback(const acronis_common::CANFreqConstPtr &msg_ptr)
{
    /* lock can_freq_history */
    try{
        history_lock.lock();
    }
    catch (const std::system_error &e){
        std::cout << "error code:" << e.code();
    }
    //history_lock.lock();
    /* update */
    for (uint i = 0; i < msg_ptr->data.size(); i++)
    {
        can_freq_history[msg_ptr->data[i].key] = msg_ptr->data[i].value;
    }
    ROS_INFO("history updated");
    /* unlock */
    history_lock.unlock();
}

//void period_callback(const mape_msg::CANFreqConstPtr &msg_ptr)
void period_callback(const acronis_common::CANFreqConstPtr &msg_ptr)
{
    /* lock can_freq_period */
    try{
        period_lock.lock();
    }
    catch (const std::system_error &e){
        std::cout << "error_code:" << e.code();
    }
    /* update it */
    can_freq_period.clear();

    for (uint i = 0; i < msg_ptr->data.size(); i++)
    {
        //const mape_msg::Uint32_Float64 &item = msg_ptr->data[i];
        const acronis_common::Uint32_Float64 &item = msg_ptr->data[i];
        can_freq_period[item.key] = item.value;
    }
    ROS_INFO("period updated");
    /* unlock */
    period_lock.unlock();

    /* call analyze_can_bus */
    analyze_can_bus();
}

void analyze_can_bus(void)
{
    /* lock both frequency logs */
    try{
        history_lock.lock();
        period_lock.lock();
    }catch (const std::system_error &e){
        std::cout << "error is " << e.code();
    }

    if (can_freq_history.size() > 0)
    {
        std::map<uint32_t, double>::iterator iter;
        /* check for suspicious new present fames */
        for (iter = can_freq_period.begin(); iter != can_freq_period.end(); iter++)
        {
            /* if current frame is not found in can_freq_history
             * then it's new and possiblly a spoofing frame
             */
            if (can_freq_history.find(iter->first) == can_freq_history.end())
            {
                if (iter->second >= FREQ_THRESHOLD)
                {
                    /* A spoofing frame! */
                    ROS_INFO("Found a criminal! ID: %08X @ %lf Hz\n", iter->first, iter->second);
                }
            }
            else
            {
                if (iter->second >= (FREQ_DIFF * can_freq_history[iter->first]))
                {
                    /* current period frequency is greatly increased then it's suspicious */
                    ROS_INFO("[Suspicious] ID: %08X @ %lf Hz now but @ %lf Hz historically\n", iter->first, iter->second, can_freq_history[iter->first]);
                }
            }
        }
    }

    /* unlock */
    period_lock.unlock();
    history_lock.unlock();
}

/* parsing rules */
char *rule_str; // input for the parser, [deprecated]

// store parsed results
std::vector<rule_t> rule_list;

int check(const unsigned int msg, const double uphold)
{
    rule_t new_rule;
    new_rule.checker_name = Threshold;
    new_rule.msg_id1 = msg;
    new_rule.threshold = uphold;

    rule_list.push_back(new_rule);
    
#ifdef DEBUG
    printf("rule size is %d\n", rule_list.size());
#endif
    
    return 1;
}

int check_diff(const unsigned int msg1, const unsigned int msg2, const double uphold)
{
    rule_t new_rule;
    new_rule.checker_name = Compare;
    new_rule.msg_id1 = msg1;
    new_rule.msg_id2 = msg2;
    new_rule.threshold = uphold;

    rule_list.push_back(new_rule);
    
#ifdef DEBUG
    printf("rule size is %d\n", rule_list.size());
#endif
    
    return 1;
}

double get_msg_num(const acronis_common::SWCacheConstPtr &ptr, const uint msg_id)
{
    for (uint i = 0; i < ptr->can_frames.size(); i++)
    {
#ifdef DEBUG
        std::cout << "id: " << ptr->can_frames[i].msg_id;
        std::cout <<" num: " << ptr->can_frames[i].msg_num << std::endl;
#endif
        if (ptr->can_frames[i].msg_id == msg_id)
        {
            return ptr->can_frames[i].msg_num;
        }
    }

    return 0;
}

// check threshold in each sliding window
void sw_callback(const acronis_common::SWCacheConstPtr &ptr)
{
    // default rule
    for (uint i = 0; i < ptr->can_frames.size(); i++)
    {
        if (ptr->can_frames[i].msg_num >= MAX_FRAMES_COUNT)
        {
            ROS_INFO("[Suspicious] CAN Frame %08X's count %d has exceeded max number %d in last check!\n", ptr->can_frames[i].msg_id, ptr->can_frames[i].msg_num, MAX_FRAMES_COUNT);
        }
    }

    // for defined rules
    for (uint i = 0; i < rule_list.size(); i++)
    {
        //std::cout << "in checker" << std::endl;
        switch(rule_list[i].checker_name)
        {
            case Threshold:
                // std::cout << "policy 1 check" << std::endl;
                if (get_msg_num(ptr, rule_list[i].msg_id1) < rule_list[i].threshold)
                {
                    std::cout << "rule " << i << " check succeeded!"<< std::endl;
                                    
                }
                else{
                    std::cout << "rule " << i << " check failed!"<< std::endl;
                }
                break;
            case Compare:
                // std::cout << "policy 2 check" << std::endl;
                if (abs(get_msg_num(ptr, rule_list[i].msg_id1) - get_msg_num(ptr, rule_list[i].msg_id2)) < rule_list[i].threshold)
                {
                    std::cout << "rule " << i << " check succeeded!"<< std::endl;
                                    
                }
                else{
                    std::cout << "rule " << i << " check failed!" << std::endl;
                }
                break;
            default:
                ;
                        
        }
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "CAN_Analyzer");
    ROS_INFO("analyzer started!");
    ros::NodeHandle nh;

    // set default upthreshold
    MAX_FRAMES_COUNT = NUM_UPLIMIT; 

    // get the rule if it exists
    if(ros::param::has("sw_rule"))
    {
        std::string sw_rule;
        ros::param::get("sw_rule", sw_rule);
        sw_rule.append("\n"); // this is a MUST!
#ifdef DEBUG
        std::cout << "rule is: " << sw_rule << std::endl;
#endif
        /* call our parser to process it */
        yy_scan_string(sw_rule.c_str());
        yyparse();
    }

    if(ros::param::has("sw_threshold"))
    {
        int sw_threshold;
        ros::param::get("sw_threshold", sw_threshold);
        
#ifdef DEBUG
        std::cout << "sw threshold is: " << sw_threshold << std::endl;
#endif
        MAX_FRAMES_COUNT = sw_threshold;
    }

    
    
    ros::Subscriber nh_sub1 = nh.subscribe(CAN_MONITOR_TOPIC1, 1000, history_callback);
    ros::Subscriber nh_sub2 = nh.subscribe(CAN_MONITOR_TOPIC2, 1000, period_callback);
    ros::Subscriber nh_sub3 = nh.subscribe(CAN_MONITOR_TOPIC3, 1000, sw_callback);
    
    ros::spin();
    return 0;
}
