#include <iostream>
#include <map>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <ros/ros.h>

/*
#include <mape_msg/PartitionLogs.h>
#include <mape_msg/BadNodes.h>
*/
#include <mape_analyzer/unit.h>
#include <acronis_common/PartitionLogs.h>
#include <acronis_common/BadNodes.h>
#include <acronis_common/DisBeacon.h>
#include <acronis_common/FailureReq.h>
#include <acronis_common/CommCH.h>
#include <acronis_common/IDTuple.h>

#include <acronis_common/TestStatus.h>


#define MAX_UP_DIFF 3
#define MAX_CHECKS 3
#define MAX_COUNTERS 3
#define MAX_PIDS 256
#define MAX_MEM_USAGE 0.8
#define MAX_CPU_USAGE 90


struct stats{
    uint32_t update_tag;
    uint32_t num_processes;
    float cpu_usage;
    double mem_available;
    double mem_total;
};

/* address - stats, parition statistics log */
std::map<std::string, struct stats> partition_logs;
static uint32_t cur_update;
static ros::Publisher nh_pub1;

/* each container has multiple counters */
std::map<std::string, std::array<uint8_t, MAX_COUNTERS>> suspect_counts;
std::vector<std::string> suspects, failures;
/* Unit will be initialized in main() */
Unit *my_unit;

void publish_failures(void)
{
    /* check for failure */
    //std::cout << "size of failures is " << failures.size() << std::endl;
    if (failures.size() > 0)
    {
        my_unit->fault_detected();
    }
}

void test_unit(void)
{
    my_unit->fault_detected();
}

void check_usage_counters(const std::string partition, const int counter_id)
{
    /* update counts first */
    if (suspect_counts.find(partition) == suspect_counts.end())
    {
        /* first appearance, init count value */
        suspect_counts[partition][counter_id] = 1;
    }
    else if (suspect_counts[partition][counter_id] >= MAX_CHECKS)
    {
        std::vector<std::string>::iterator position = std::find(suspects.begin(), suspects.end(), partition);
        /* count value exceeds MAX_CHECK */
        if (position != suspects.end())
        {
            /* Parition has been in the list, then we put it to failure list to be published */
            std::cout << "[failure] " << partition << std::endl;
            failures.push_back(partition);
            /* erase Parition from suspect list */
            // suspects.erase(position);
        }
        else
        {
            /* Partition never appears in suspect list, then we put it in the list */
            std::cout << "[suspect] " << partition << std::endl;
            suspects.push_back(partition);
        }
    }
    else
    {
        suspect_counts[partition][counter_id]++;
    }

    publish_failures();
}


void partition_stats_chk(void)
{
    if (partition_logs.size() == 0)
    {
        return;
    }

    /* check if there're any far behind the current update period */
    std::map<std::string, struct stats>::iterator iter;
    uint32_t up_thd;

    if (cur_update > 3)
    {
        up_thd = cur_update - 3;
    }
    else
    {
        up_thd = cur_update;
    }
    
    for (iter = partition_logs.begin(); iter != partition_logs.end(); iter++)
    {
        std::cout << "[DEBUG] parition: " << iter->first << std::endl;
        std::cout << "process: " << iter->second.num_processes << std::endl;
        std::cout << "cpu usage: " << iter->second.cpu_usage << std::endl;
        std::cout << "mem usage: " << (1 - iter->second.mem_available / iter->second.mem_total) << std::endl;
        std::cout << "tag: " << iter->second.update_tag << std::endl;
        std::cout << "thd: " << up_thd << std::endl;
        /* if a partiton is far behind the update threshold count
         * then that partition is probably offline
         */
        if (iter->second.update_tag < up_thd)
        {
            if ((suspects.size() > 0) && (std::find(suspects.begin(), suspects.end(), iter->first) != suspects.end()) )
            {
                std::cout << "[failure tag] " << iter->first << std::endl;
                /* if current partition is already in the suspect list
                 * then put it in the failure list for further plan action
                 */
                failures.push_back(iter->first);

                publish_failures();
            }
            else
            {
                std::cout << "[suspect tag] " << iter->first << std::endl;
                /* first appearance, put it in the suspect list */
                suspects.push_back(iter->first);
            }
        }
        
        /* check if a partition has abnormal resource usage */
        /* TODO: define rules to check the usage info */
        
        if (iter->second.num_processes >= MAX_PIDS)
        {
            /* possible forkbomb? */
            check_usage_counters(iter->first, 0);
        }
        if ((1 - iter->second.mem_available / iter->second.mem_total) >= MAX_MEM_USAGE)
        {
            /* possible membomb? */
            check_usage_counters(iter->first, 1);
        }
        if(iter->second.cpu_usage >= MAX_CPU_USAGE)
        {
            /* possible cpu DDoS? */
            check_usage_counters(iter->first, 2);
        }
    }
}


//void partition_stats_cb(const mape_msg::PartitionLogsConstPtr &data_ptr)
void partition_stats_cb(const acronis_common::PartitionLogsConstPtr &data_ptr)
{
    cur_update++;
    /* read new statistics and update logs */
    for (uint i = 0; i < data_ptr->logs.size(); i++)
    {
        struct stats new_log;

        /* update the tag */
        new_log.update_tag = cur_update;
        /* update statistics */
        new_log.num_processes = data_ptr->logs[i].num_processes;
        new_log.mem_available = data_ptr->logs[i].mem_avalable;
        new_log.mem_total = data_ptr->logs[i].mem_total;
        new_log.cpu_usage = data_ptr->logs[i].cpu_usage;
        
        partition_logs[data_ptr->logs[i].partition_addr] = new_log;
    }

    /*  check log data for anomaly data*/
    partition_stats_chk();
    /* publish failures */
}

void test_stats_cb(const acronis_common::TestStatusConstPtr &ptr)
{
    if (ptr->failed)
    {
        my_unit->fault_detected();
    }
    else if(!ptr->failed)
    {
        my_unit->fault_clear();
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "MAPE_a_partition");
    ros::NodeHandle nh("~"); // private handler to get local params
    ros::NodeHandle g_nh; // nh using global namespace for publish/subscribe

    int unit_id = 0;

    if(nh.hasParam("unit_id"))
    {
        nh.getParam("unit_id", unit_id);
    }else{
        ROS_WARN("A unique ID should be assigned to different partition analyzer!\n");
    }

    std::vector<int> upstream, downstream;
    if(nh.hasParam("upstream"))
    {
        nh.getParam("upstream", upstream);
    }
    if(nh.hasParam("downstream"))
    {
        nh.getParam("downstream", downstream);
    }

    /* publish "unit_beacon_#" to any of its downstream units 
     * publish "unit_failure_#" to its own planner
     */
    std::string beacon_name = "unit_beacon_" + std::to_string(unit_id);
    std::string failure_name = "unit_failure_" + std::to_string(unit_id);
    
    ros::Publisher nh_unit_msg = g_nh.advertise<acronis_common::DisBeacon>(beacon_name, 100);
    ros::Publisher nh_unit_plan = g_nh.advertise<acronis_common::BadNodes>(failure_name, 100);
    /* req publishers */
    ros::Publisher nh_req_pub = g_nh.advertise<acronis_common::FailureReq>("unit_req_"+std::to_string(unit_id), 1);

    //my_unit = new Unit(unit_id, nh_unit_msg, nh_unit_plan, nh_req_pub);

    my_unit = new Unit(unit_id, nh_unit_msg, nh_unit_plan, nh_req_pub, g_nh, upstream, downstream, true);
    //my_unit->template update_upstreams<int>(upstream, true);
    
    /* "partition_failure" will not be used in the current unit_analyzer */
    nh_pub1 = g_nh.advertise<acronis_common::BadNodes>("partition_failures_"+std::to_string(unit_id), 10);

    /* subscribe monitoring result from its own monitor */
    ros::Subscriber nh_sub1 = g_nh.subscribe("partition_stats_"+std::to_string(unit_id), 1, partition_stats_cb);
    /* subscribe for test failure injection */
    ros::Subscriber nh_sub_t = g_nh.subscribe("Test_stats_"+std::to_string(unit_id), 1, test_stats_cb);
    //nh_pub1 = nh.advertise<mape_msg::BadNodes>("partition_failures", 10);

    /* subscribe to all upstream/downstrem neighbors for 2-way comm */
    // std::vector<ros::Subscriber> beacon_subscribers, req_subscribers;
    
    // for (uint i = 0; i < upstream.size(); i++)
    // {
    //     //std::cout << i+1 << "beacon subscribed\n";
    //     beacon_subscribers.push_back(g_nh.subscribe("unit_beacon_"+std::to_string(upstream[i]), 1, &Unit::update_data_hdl, my_unit));
    // }
    // for (uint i = 0; i < downstream.size(); i++)
    // {
    //     //std::cout << i+1 << "beacon subscribed\n";
    //     req_subscribers.push_back(g_nh.subscribe("unit_req_"+std::to_string(downstream[i]), 1, &Unit::failure_req_hdl, my_unit));
    // }

    /* subscribe to common channel
     * publish to common channel if necessary 
     */
    ros::Subscriber cc_sub = g_nh.subscribe("unit_common_ch", 1, &Unit::comm_ch_hdl, my_unit);

    if(nh.hasParam("test_add"))
    {
        ros::Publisher ta_pub = g_nh.advertise<acronis_common::CommCH>("unit_common_ch", 1);

        acronis_common::CommCH new_msg;
        acronis_common::IDTuple new_tuple;
        new_tuple.id1 = 5;
        new_tuple.id2 = 7;
        new_msg.rlist.push_back(new_tuple);

        new_tuple.id1 = 5;
        new_tuple.id2 = 9;
        new_msg.alist.push_back(new_tuple);
        new_tuple.id1 = 9;
        new_tuple.id2 = 7;
        new_msg.alist.push_back(new_tuple);

        new_msg.header.stamp = ros::Time::now();
        
        ta_pub.publish(new_msg);
    }
    
    ros::spin();

    return 0;
}
