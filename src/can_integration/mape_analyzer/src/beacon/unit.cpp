#include <ros/ros.h>
#include <mape_analyzer/unit.h>
#include <acronis_common/FailureReq.h>

void Unit::send_beacon_msg(void)
{
    acronis_common::DisBeacon new_msg;
    new_msg.header.stamp = ros::Time::now();
    new_msg.header.frame_id = "unit_beacon";
    new_msg.self_id = this->self_id;
    new_msg.sick_bit = this->sick_bit;
    new_msg.fault_queue.assign(this->fault_queue.begin(), this->fault_queue.end());
    
    msg_pb.publish(new_msg);
}

void Unit::call_planner(void)
{
    /* publish failure assertion and call for failure mitigation */
    acronis_common::BadNodes bn_msg;
    bn_msg.msg_header.stamp = ros::Time::now();
    bn_msg.msg_header.frame_id = "plan_" + std::to_string(this->self_id);

    bn_msg.failures.push_back(std::to_string(this->self_id));

    this->plan_pub.publish(bn_msg);
}

void Unit::sync_failure_req(void)
{
    /* 1. build a sync_request msg
     * 2. publish the msg
     */
    acronis_common::FailureReq new_req;
    new_req.header.stamp = ros::Time::now();
    new_req.self_id = this->self_id;
    
    for(auto itr = this->sync_map.begin(); itr != this->sync_map.end(); itr++)
    {
        if(!itr->second)
        {
            /* if current upstream unit is not synched (never replys) */
            new_req.target.push_back(itr->first);
        }
    }

    this->req_pub.publish(new_req);
}

bool Unit::is_synched(void)
{
    /* at least sync_map has to be initialized
     */
    if (this->sync_map.size() == this->upstreams.size())
    {
        /* check if we get at least one reply 
         * from each upstream unit 
         */
        bool act = true;
        for (auto itr = this->sync_map.begin(); itr != this->sync_map.end(); itr++)
        {
            act &= itr->second;
        }

        return act;
    }

    return false;
}

/* call only when a local failure is detected */
void Unit::fault_detected(void)
{
    if (this->sick_bit == false)
    {
        /* NORMAL -> PENDING
         * we should ask upstream units first 
         * in case of communication delay
         */
        // ask upstream for failure status
        if (!this->is_synched())
        {
            this->sync_failure_req();
        }
        /* lock may required */
        fq_lock.lock();
        
        if(this->fault_queue.empty() ||
           std::find(this->fault_queue.begin(), this->fault_queue.end(), this->self_id) == fault_queue.end())
        {
            /* if node itself is not in the current fault queue */
            /* but ideally current node should only appear in either head or tail of its current non-empty fault queue */
            fault_queue.push_back(this->self_id);
        }
        fq_lock.unlock();

        sb_lock.lock();
        this->sick_bit = true;
        sb_lock.unlock();
    }

    if (this->fault_queue.front() != self_id)
    {
        //send_beacon_msg
        this->send_beacon_msg();
    }
    else
    {        
        if (this->is_synched())
        {
            //call for planner to mitigate
            this->call_planner();
        }
        else{
            /* wait or keep asking for failure report */
            this->sync_failure_req();
        }
    }
}

void Unit::fault_clear(void)
{
    if (this->sick_bit == true)
    {
        this->sick_bit = false;
        this->fault_queue.clear();

        int i = 10;
        while(i > 0)
        {
            this->send_beacon_msg();
            i--;
        }
    }
}

/* merge queue b into queue a */
// template <class T>
static void merge_queues(std::deque<uint32_t> &lista, std::deque<uint32_t> &listb)
{
    std::deque<uint32_t>::iterator lta, ltb;
    bool flag = false;

    for (ltb = listb.begin(); ltb != listb.end(); ltb++)
    {
        if (std::find(lista.begin(), lista.end(), *ltb) == lista.end())
        {
            flag = false;
            for (lta = lista.begin(); lta != lista.end(); lta++)
            {
                if (*lta > *ltb)
                {
                    lista.insert(lta, *ltb);
                    flag = true;
                    break;
                }
            }
            if(!flag){
                lista.push_back(*ltb);
            }
        }
    }
}

template<class Container, typename T>
static void merge_element(Container &list, const T &item)
{
    for (auto ltr = list.begin(); ltr != list.end(); ltr++)
    {
        if (*ltr > item)
        {
            list.insert(ltr, item);
            return;
        }        
    }
    // this could not happen...
    list.push_back(item);
}

template<class Container, typename T>
static void remove_element(Container &list, const T &item)
{
    for (auto ltr = list.begin(); ltr != list.end(); ltr++)
    {
        if (*ltr == item)
        {
            list.erase(ltr);
            return;
        }
    }
}

/* remove or add an item to list */
static void adjust_list(const uint32_t &item, const bool &flag, std::deque<uint32_t> &list)
{
    if (flag)
    {
        // add
        merge_element(list, item);
    }else{
        // remove
        remove_element(list, item);
    }
}

void Unit::update_data_hdl(const acronis_common::DisBeaconConstPtr &ptr)
{
    // std::cout << this->self_id << ": ";
    // for (uint i = 0; i < this->fault_queue.size(); i ++)
    // {
    //     std::cout << this->fault_queue[i] << " ";
    // }
    // std::cout << std::endl;
    
    if (this->sick_bit)
    {
        /* check and update sync_map */
        this->sm_lock.lock();
        /* after initilization of sync_map, all its upstream unit pairs should be in this dict */
        if (this->sync_map.find(ptr->self_id) != this->sync_map.end())
        {
            this->sync_map[ptr->self_id] = true;
        }
        this->sm_lock.unlock();

        /* if an upstream reports failure */
        if (ptr->sick_bit)
        {
            // std::deque<uint32_t> tmp_queue(ptr->fault_queue.begin(), ptr->fault_queue.end());
            // tmp_queue.push_back(this->self_id);
            
            // if (this->sick_bit)
            // {
            // if (this->fault_queue != tmp_queue)
            // {
                //fq_lock.lock();
            std::lock_guard<std::mutex> lg_fq(fq_lock);

            /* add the upstream to fault_queue if it is not in there */
            if(std::find(this->fault_queue.begin(), this->fault_queue.end(), ptr->self_id) == this->fault_queue.end())
            {
                //std::lock_guard<std::mutex> lg_fq(fq_lock);
                //this->fault_queue.assign(tmp_queue.begin(), tmp_queue.end());
                //merge_queues(this->fault_queue, tmp_queue);
                merge_element(this->fault_queue, ptr->self_id);
                //fq_lock.unlock();
                // }
            }
        }
        else if (!ptr->sick_bit)
        {
            /* if the upstream is clear of failure, try to find it in fault_queue and remove it */
            std::lock_guard<std::mutex> lg_fq(fq_lock);
            auto pos = std::find(this->fault_queue.begin(), this->fault_queue.end(), ptr->self_id);
            if (pos != this->fault_queue.end())
            {
                this->fault_queue.erase(pos);
            }
        }
    }
    
}

void Unit::failure_req_hdl(const acronis_common::FailureReqConstPtr &ptr)
{
    if (std::find(ptr->target.begin(), ptr->target.end(), this->self_id) != ptr->target.end())
    {
        /* if curent unit is beding requested 
         * send beacon immediately
         */
        this->send_beacon_msg();
    }
}

void Unit::comm_ch_hdl(const acronis_common::CommCHConstPtr &ptr)
{
    if(!ptr->rlist.empty())
    {
        //this->sbl_lock.lock();
        
        for (uint i = 0; i < ptr->rlist.size(); i++)
        {
            if (ptr->rlist[i].id2 == this->self_id)
            {
                //remove the corresponding beacon subscriber
                sb_list[ptr->rlist[i].id1].shutdown();
                sb_list[ptr->rlist[i].id1].~Subscriber();
                
                this->sb_list.erase(ptr->rlist[i].id1);
                //remove item from upstream list
                remove_element(this->upstreams, ptr->rlist[i].id1);
                // update sync_map
                this->sm_lock.lock();
                this->sync_map.erase(ptr->rlist[i].id1);
                this->sm_lock.unlock();
            }
            if(ptr->rlist[i].id1 == this->self_id)
            {
                // remove sync_req subscriber
                sb_list[ptr->rlist[i].id2].shutdown();
                sb_list[ptr->rlist[i].id2].~Subscriber();
                
                this->sb_list.erase(ptr->rlist[i].id2);
                // remove from downstream list
                remove_element(this->downstreams, ptr->rlist[i].id2);
            }
        }
        //this->sbl_lock.unlock();
    }
    if( !ptr->alist.empty())
    {
        for(uint i = 0; i < ptr->alist.size(); i++)
        {
            if(ptr->alist[i].id2 == this->self_id)
            {
                //add a new beacon subscriber
                this->sb_list[ptr->alist[i].id1] = this->unit_hdl.subscribe("unit_beacon_"+std::to_string(ptr->alist[i].id1), 1, &Unit::update_data_hdl, this);
                merge_element(this->upstreams, ptr->alist[i].id1);

                //update sync_map
                this->sm_lock.lock();
                this->sync_map[ptr->alist[i].id1] = false;
                this->sm_lock.unlock();
            }
            if(ptr->alist[i].id1 == this->self_id)
            {
                // add a new sync_req subscriber
                this->sb_list[ptr->alist[i].id2] = this->unit_hdl.subscribe("unit_req_"+std::to_string(ptr->alist[i].id2), 1, &Unit::failure_req_hdl, this);
                merge_element(this->downstreams, ptr->alist[i].id2);
            }
        }
    }
}
