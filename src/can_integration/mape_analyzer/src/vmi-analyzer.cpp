#include <iostream>
#include <map>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <ros/ros.h>

#include <acronis_common/VMIMonitor.h>

typedef enum {
    PASS = 0,
    UNKOWN = 1,
    INVALID_PROC = 2,
    NEG_PID = 3,
    NEG_CR3 = 4,
    NEG_PPID = 5,
    NEG_ADDR = 6,
    NEW_PID = 11,
    NEW_CR3 = 12,
    NEW_PROC_K = 13,
    NEW_PROC = 14
} CHK_STATUS;


typedef struct {
    std::string proc_name;
    int32_t pid;
    int32_t ppid;
    bool init; // if it has been initilized

    std::map<uint32_t, uint64_t> vcpu_cr3;
    // uint32_t vcpu;

    // uint64_t cr3;

    void reset();
} VMI_proc_t;

// <process name, recorded>
std::map<std::string, bool> white_proc_list;

//std::map<std::string, VMI_proc_t> proc_list;
// <base_addr, process>
std::map<uint64_t, VMI_proc_t> proc_list;

void VMI_proc_t::reset()
{
    // vcpu = 0;            
    // cr3 = 0;
    pid = -1;
    ppid = -1;
    init = false;
}

/* check if item is in white list */
bool wproc_list_contains(std::string item)
{
    bool ret = false;
    if (white_proc_list.find(item) != white_proc_list.end())
    {
        ret = true;
    }

    return ret;
}

/* initialize white process list with the given list of process names */
void wproc_list_init(std::vector<std::string> names)
{
    for (auto iter = names.begin(); iter != names.end(); iter++)
    {
        white_proc_list[*iter] = false;
    }
}

CHK_STATUS proc_list_check(const acronis_common::VMIMonitorConstPtr &msg_ptr)
{
    CHK_STATUS ret = PASS;
    VMI_proc_t old;

    if (!wproc_list_contains(msg_ptr->proc_name))
    {
        return INVALID_PROC;
    }
     
    // proc name is in whitelist
    if (white_proc_list[msg_ptr->proc_name])
    {
        // process has been recorded
        // get recorded info
        if (proc_list.find(msg_ptr->base_addr) != proc_list.end())
        {
            // this process has been recorded
            old = proc_list[msg_ptr->base_addr];
            /* old value is already initilized, go for a check */
            if (old.pid == msg_ptr->pid)
            {
                /* pid matches, go on */
                uint64_t _cr3 = msg_ptr->cr3;
                if (old.vcpu_cr3.find(msg_ptr->vcpu) != old.vcpu_cr3.end())
                {
                    // uint64_t old_cr3 = old.vcpu_cr3[msg_ptr->vcpu]
                    if (old.vcpu_cr3[msg_ptr->vcpu] != _cr3)
                    {
                        ret = NEG_CR3;
                    }
                    
                }
                else
                {
                    /* a new <vcpu, cr3> comes */
                    old.vcpu_cr3[msg_ptr->vcpu] = _cr3;
                    return NEW_CR3;
                }
            }
            else
            {
                // invalid pid
                ret = NEG_PID;
            }
        }
        else if (msg_ptr->ppid < 3 && msg_ptr->ppid > 0)
        {
            // newly spawned by init/kthread
            VMI_proc_t new_proc;
            new_proc.pid = msg_ptr->pid;
            new_proc.ppid = msg_ptr->ppid;
            new_proc.proc_name = msg_ptr->proc_name;
            new_proc.vcpu_cr3[msg_ptr->vcpu] = msg_ptr->cr3;
            new_proc.init = true;

            proc_list[msg_ptr->base_addr] = new_proc;
            white_proc_list[msg_ptr->proc_name] = true;

            ret = NEW_PROC_K;
        }
        else
        {
            ret = NEG_PID;
        }
        
    }
    else
    {
        // user-mode process never recorded before
        VMI_proc_t new_proc;
        new_proc.pid = msg_ptr->pid;
        new_proc.ppid = msg_ptr->ppid;
        new_proc.proc_name = msg_ptr->proc_name;
        new_proc.vcpu_cr3[msg_ptr->vcpu] = msg_ptr->cr3;
        new_proc.init = true;

        proc_list[msg_ptr->base_addr] = new_proc;
        white_proc_list[msg_ptr->proc_name] = true;

        ret = NEW_PROC;
    }   

    return ret;
}

void vmimonitor_msg_cb(const acronis_common::VMIMonitorConstPtr &msg_ptr)
{
    /* validate process list */
    if (wproc_list_contains(msg_ptr->proc_name))
    {
        switch (proc_list_check(msg_ptr))
        {
            case NEG_CR3:
                ROS_INFO("Wrong CR3: %lu for %s on VCPU %d\n", msg_ptr->cr3, msg_ptr->proc_name.c_str(), msg_ptr->vcpu);
                break;
            case NEG_PID:
                ROS_INFO("Wrong PID: %d for %s\n", msg_ptr->pid, msg_ptr->proc_name.c_str());  
                break;     
            case INVALID_PROC:
                ROS_INFO("Invalid process %s, PID: %d\n", msg_ptr->proc_name.c_str(), msg_ptr->pid);
            default:
                break;
        }  
    }
    else
    {
        ROS_INFO("Process %s not allowed!\n", msg_ptr->proc_name.c_str()); 
    }
    
}

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "vmianalyzer_node");
    ros::NodeHandle nh;
    ros::NodeHandle p_nh("~");
    std::vector<std::string> proc_list_p;
    
    if(p_nh.hasParam("proc_list"))
    {
        p_nh.getParam("proc_list", proc_list_p);
        wproc_list_init(proc_list_p);
        ROS_INFO("Process white list initialized!\n");
    }
    else
    {
        ROS_INFO("No process white list specified!\n");
        exit(1);
    }

    ros::Subscriber nh_sub1 = nh.subscribe("VMI_monitor", 1000, vmimonitor_msg_cb);

    ros::spin();
    return 0;
}