#include <iostream>
#include <map>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <ros/ros.h>
/*
#include <mape_msg/PartitionLogs.h>
#include <mape_msg/BadNodes.h>
*/
#include <acronis_common/PartitionLogs.h>
#include <acronis_common/BadNodes.h>
#include <acronis_common/Processes.h>

#define MAX_UP_DIFF 3
#define MAX_CHECKS 3
#define MAX_COUNTERS 3
#define MAX_PIDS 500
#define MAX_MEM_USAGE 0.8
#define MAX_CPU_USAGE 90
//#define MIN_MEM_AVAILABLE 512<<20 // 512MB = 512 *1024 *1024B

struct stats{
    uint32_t update_tag;
    uint32_t num_processes;
    float cpu_usage;
    double mem_available;
    double mem_total;
    int status;
    //std::map<uint32_t, uint64_t> proc_list;
};

typedef std::map<uint32_t, uint64_t> proc_list_t;

/* address - stats, parition statistics log */
std::map<std::string, struct stats> partition_logs;
/* partition - proc_list */
std::map<std::string, proc_list_t> partition_proclist;

static uint32_t cur_update;
static ros::Publisher nh_pub1;

/* each container has multiple counters 
 * counter[0] holds the sum of counter[1:N]
 * counter[1:N] holds individual count of different invalidations
 * counter[1] -> process
 * counter[2] -> memory
 * counter[3] -> cpu 
 */
std::map<std::string, std::array<uint8_t, (MAX_COUNTERS+1)>> suspect_counts;

#define SC_INDEX_PROC 1
#define SC_INDEX_MEM  2
#define SC_INDEX_CPU  3

std::vector<std::string> suspects, failures;


void publish_failures(void)
{
    /* check for failure */
    //std::cout << "size of failures is " << failures.size() << std::endl;
    if (failures.size() > 0)
    {
        /* publish failures and reset it */
        //mape_msg::BadNodes msg;
        acronis_common::BadNodes msg;
        msg.msg_header.stamp = ros::Time::now();
        msg.failures.reserve(failures.size());
        std::copy(failures.begin(), failures.end(), std::back_inserter(msg.failures));
        
        nh_pub1.publish(msg);
        std::cout << ros::Time::now() << std::endl; 
        /* clear published partitions */
        for (uint i = 0; i < failures.size(); i++)
        {
            /* erase partition info from suspect list */
            std::vector<std::string>::iterator position = std::find(suspects.begin(), suspects.end(), failures[i]);
            if (position != suspects.end())
            {
                suspects.erase(position);
            }
            /* erase partition info from suspect_counts */
            if (suspect_counts.find(failures[i]) != suspect_counts.end())
            // suspects.erase(std::remove(suspects.begin(), suspects.end(), failures[i]), suspects.end());
            {
                suspect_counts.erase(failures[i]);
            }
            /* erase partition info from partition_logs */
            if (partition_logs.find(failures[i]) != partition_logs.end())
            {
                partition_logs.erase(failures[i]);
            }
        }
        failures.clear();
    }
}

void check_usage_counters(const std::string partition, const int counter_id)
{
    /* update counts first */
    if (suspect_counts.find(partition) == suspect_counts.end())
    {
        /* first appearance, init count value */
        suspect_counts[partition][counter_id] = 1;
        /* sum of counter values */
        suspect_counts[partition][0] = 1;
    }
    else if (suspect_counts[partition][0] >= MAX_CHECKS)
    {
        std::vector<std::string>::iterator position = std::find(suspects.begin(), suspects.end(), partition);
        /* count value exceeds MAX_CHECK */
        if (position != suspects.end())
        {
            /* Parition has been in the list, then we put it to failure list to be published */
            std::cout << "[failure] " << partition << std::endl;
            failures.push_back(partition);
            /* erase Parition from suspect list */
            // suspects.erase(position);
        }
        else
        {
            /* Partition never appears in suspect list, then we put it in the list */
            std::cout << "[suspect] " << partition << std::endl;
            suspects.push_back(partition);
        }
    }
    else
    {
        suspect_counts[partition][counter_id]++;
        suspect_counts[partition][0]++;
    }

    publish_failures();
}


void partition_stats_chk(void)
{
    if (partition_logs.size() == 0)
    {
        return;
    }

    /* check if there're any far behind the current update period */
    std::map<std::string, struct stats>::iterator iter;
    uint32_t up_thd;

    if (cur_update > 3)
    {
        up_thd = cur_update - 3;
    }
    else
    {
        up_thd = cur_update;
    }
    
    for (iter = partition_logs.begin(); iter != partition_logs.end(); iter++)
    {
        std::cout << "[DEBUG] parition: " << iter->first << std::endl;
        std::cout << "status: " << iter->second.status << std::endl;
        std::cout << "process: " << iter->second.num_processes << std::endl;
        std::cout << "cpu usage: " << iter->second.cpu_usage << std::endl;
        std::cout << "mem usage: " << (1 - iter->second.mem_available / iter->second.mem_total) << std::endl;
        std::cout << "tag: " << iter->second.update_tag << std::endl;
        std::cout << "thd: " << up_thd << std::endl;
        /* if a partiton is far behind the update threshold count
         * then that partition is probably offline
         */
        if (iter->second.update_tag < up_thd)
        {
            if ((suspects.size() > 0) && (std::find(suspects.begin(), suspects.end(), iter->first) != suspects.end()) )
            {
                std::cout << "[failure tag] " << iter->first << std::endl;
                /* if current partition is already in the suspect list
                 * then put it in the failure list for further plan action
                 */
                failures.push_back(iter->first);

                publish_failures();
            }
            else
            {
                std::cout << "[suspect tag] " << iter->first << std::endl;
                /* first appearance, put it in the suspect list */
                suspects.push_back(iter->first);
            }
        }
        
        /* check if a partition has abnormal resource usage */
        /* TODO: define rules to check the usage info */
        if(iter->second.status != 1)
        {
            failures.push_back(iter->first);
            publish_failures();
        }
        if (iter->second.num_processes >= MAX_PIDS)
        {
            /* possible forkbomb? */
            check_usage_counters(iter->first, SC_INDEX_PROC);
        }
        if ((1 - iter->second.mem_available / iter->second.mem_total) >= MAX_MEM_USAGE)
        {
            /* possible membomb? */
            check_usage_counters(iter->first, SC_INDEX_MEM);
        }
        if(iter->second.cpu_usage >= MAX_CPU_USAGE)
        {
            /* possible cpu DDoS? */
            check_usage_counters(iter->first, SC_INDEX_CPU);
        }
    }
}

void partition_process_cb(const acronis_common::ProcessesConstPtr &data_ptr)
{
    if (!partition_proclist.count(data_ptr->header.frame_id))
    {
        /* current partition's process list is not logged */ 
        for (uint i = 0; i < data_ptr->list.size(); i++)
        {
            partition_proclist[data_ptr->header.frame_id][data_ptr->list[i].pid] = data_ptr->list[i].addr;
        }
    }
    else{
        if (data_ptr->list.size() != partition_proclist.size())
        {
            /* number of processes changed -> might be abnormal */
            check_usage_counters(data_ptr->header.frame_id, SC_INDEX_PROC);
        }
        else
        {
            /* number of processes remains same, go check each process */
            for (uint i = 0; i < data_ptr->list.size(); i++)
            {
                if (partition_proclist[data_ptr->header.frame_id].count(data_ptr->list[i].pid))
                {
                    /* current pid has been logged */
                    if (partition_proclist[data_ptr->header.frame_id][data_ptr->list[i].pid] != data_ptr->list[i].addr)
                    {
                        /* current process addr changed -> might be abnormal */
                        check_usage_counters(data_ptr->header.frame_id, SC_INDEX_PROC);
                    }
                }
                else
                {
                    /* current pid has not been logged -> might be abnormal */
                    check_usage_counters(data_ptr->header.frame_id, SC_INDEX_PROC);
                }
            }
        }
    }
    
}

//void partition_stats_cb(const mape_msg::PartitionLogsConstPtr &data_ptr)
void partition_stats_cb(const acronis_common::PartitionLogsConstPtr &data_ptr)
{
    cur_update++;
    /* read new statistics and update logs */
    for (uint i = 0; i < data_ptr->logs.size(); i++)
    {
        struct stats new_log;

        /* update the tag */
        new_log.update_tag = cur_update;
        /* update statistics */
        new_log.num_processes = data_ptr->logs[i].num_processes;
        new_log.mem_available = data_ptr->logs[i].mem_avalable;
        new_log.mem_total = data_ptr->logs[i].mem_total;
        new_log.cpu_usage = data_ptr->logs[i].cpu_usage;
        new_log.status = data_ptr->logs[i].status;
        
        partition_logs[data_ptr->logs[i].partition_addr] = new_log;
    }

    /*  check log data for anomaly data*/
    partition_stats_chk();
    /* publish failures */
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "MAPE_a_partition");
    ros::NodeHandle nh;

    ros::Subscriber nh_sub1 = nh.subscribe("partition_status", 1, partition_stats_cb);
    //nh_pub1 = nh.advertise<mape_msg::BadNodes>("partition_failures", 10);
    nh_pub1 = nh.advertise<acronis_common::BadNodes>("partition_failures", 10);
    
    ros::spin();

    return 0;
}
