import os
import rospy
from Queue import Queue
import svm_training_generator_multi as STGA
from libsvm.python import svm
from libsvm.python import svmutil
from acronis_common.msg import VMIMonitor, Syscalls


class SVMDetector(object):
    def __init__(self, ocsvm_model, patterns):
        self.ocsvm_model = ocsvm_model
        self.patterns = patterns
        self.svm_params = svm.svm_parameter('-t 2 -s 1 -n 0.01')
        self.queue = Queue(5)
        self.raw_syscalls = list()
        self.pids = list()
        self.ready = False

    def predict(self):
        # if enough data is got
        if not self.queue.empty():
            res = list()
            raw_data = self.queue.get()
            for idx, pattern in enumerate(self.patterns):
                res[idx] = STGA.count_patterns(raw_data, pattern)
                res = STGA.reduce_results(res)
                labels = [1 for i in range(len(res))]

            p_label, _, _ = svmutil.svm_predict(labels, res, self.ocsvm_model, self.svm_params)

            return p_label

    # multi-voter analyzer
    def analyze(self, prediction):
        res = 0
        for i in range(5):
            res += predict()
        
    def get_raw_input(self, data):
        self.ready = False
        raw_syscalls = list()
        tmp_syscalls = list()
        pids = list()
    
        while len(self.raw_syscalls) < 1000:
            tmp_syscalls.append(data.nr_syscall)
            pids.append(data.pid)
        raw_syscalls.append(tmp_syscalls)
        self.queue.put(raw_syscalls)
        self.ready = True

    def start(self):
        self.pub = rospy.Publisher()

    
    
    

# anaomaly detection using trained OCSVM model
if __name__ == '__main__':
    # init ros node
    rospy.init_node('ocsvm_detector')

    # get saved model first
    model_path = './svm.model'
    pattern_path = './patterns'
    # read model path from a private ros param
    if rospy.has_param('~model'):
        model_path = rospy.get_param('~model')
    if rospy.has_param('~patterns'):
        pattern_path = rospy.get_param('~patterns')
    # load model and patterns (feature extraction)
    ocsvm_model = svmutil.svm_load_model(model_path)
    pat_lists = STGA.get_patterns(pattern_path)

    detector = SVMDetector(ocsvm_model, pat_lists)

    vmi_sub = rospy.Subscriber('vmi_subscriber', VMIMonitor, detector.get_raw_input)
    
