#!/usr/bin/env python
import rosbag
import rospy
import threading
import random
from nmea_msgs.msg import Sentence
# from sensor_msgs.msg import PointCloud2


def publish_nmea(pub, rate, nmea_bag):
    print("Playing %s" % nmea_bag)
    for topic, newmsg, t in rosbag.Bag(nmea_bag).read_messages():
        # new_ts = rospy.get_time()
        msg = newmsg.sentence
        _msg = msg.split(',')
        if _msg[0][:2] == 'QQ':
            roll = float(_msg[4]) + round(random.uniform(0, 20), 3)
            _msg[4] = str(roll)

            pitch = float(_msg[5]) + round(random.uniform(0, 20), 3)
            _msg[5] = str(pitch)
        
            yaw = float(_msg[6]) + round(random.uniform(0, 20), 3)
            _msg[6] = str(yaw)
        elif _msg[0] == '$PASHR':
            roll = float(_msg[4]) + round(random.uniform(0, 20), 3)
            _msg[4] = str(roll)

            pitch = float(_msg[5]) + round(random.uniform(0, 20), 3)
            _msg[5] = str(pitch)
        
            yaw = float(_msg[2]) + round(random.uniform(0, 20), 3)
            _msg[2] = str(yaw)
        elif _msg[0][3:6] == 'GGA':
            lat = float(_msg[2]) + round(random.uniform(0, 20), 3)
            _msg[2] = str(lat)

            lon = float(_msg[4]) + round(random.uniform(0, 20), 3)
            _msg[4] = str(lon)
        
            h = float(_msg[9]) + round(random.uniform(0, 20), 3)
            _msg[9] = str(h)
        elif _msg[0] == '$GPRMC':
            lat = float(_msg[3]) + round(random.uniform(0, 20), 3)
            _msg[3] = str(lat)

            lon = float(_msg[5]) + round(random.uniform(0, 20), 3)
            _msg[5] = str(lon)
        
            # h = 0.0 + round(random.uniform(0, 20), 3)
            # _msg[5] = str(yaw)
        newmsg.sentence = ",".join(_msg)
        newmsg.header.stamp = rospy.Time.now()
        if topic == '/nmea_sentence':
            pub.publish(newmsg)
            rate.sleep()

            
def publish_pc2(pub, rate, pc2_bag):
    print("Playing %s" % pc2_bag)
    for topic, msg, t in rosbag.Bag(pc2_bag).read_messages():
        # new_ts = rospy.get_time()
        msg.header.stamp = rospy.Time.now()
        if topic == '/points_raw':
            pub.publish(msg)
            rate.sleep()
    

if __name__ == '__main__':
    # play the fucking bag file with real timestamp
    rospy.init_node("mape_player")
    pub_nmea = rospy.Publisher('/nmea_sentence', Sentence, queue_size=100)
    # pub_pc2 = rospy.Publisher('/points_raw', PointCloud2, queue_size=100)

    nmea_bag = "/data/storage/autoware_bag/nmea_100s.bag"
    # pc2_bag = "/data/storage/autoware_bag/pc2_100s.bag"

    if rospy.has_param("nmea_bag"):
        nmea_bag = rospy.get_param("nmea_bag")
    # if rospy.has_param("pc2_bag"):
    #     pc2_bag = rospy.get_param("pc2_bag")
        
    nmea_r = rospy.Rate(25)
    # pc2_r = rospy.Rate(10)

    nmea = threading.Thread(target=publish_nmea,
                            args=(pub_nmea, nmea_r, nmea_bag))
    # pc2 = threading.Thread(target=publish_pc2, args=(pub_pc2, pc2_r, pc2_bag))

    try:
        # thread.start_new_thread(publish_nmea, (pub_nmea, nmea_r, ))
        # thread.start_new_thread(publish_pc2, (pub_pc2, pc2_r))
        nmea.start()
        # pc2.start()
    except:
        print("start publisher error")

    nmea.join()
    # pc2.join()
        
    print("Publish completed, Now you can quit.")
    rospy.spin()
