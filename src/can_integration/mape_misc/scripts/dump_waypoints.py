#!/usr/bin/env python
import rosbag
import rospy
import sys
from geometry_msgs import TwistStamped, Twist


def dump_waypoints(out_file, topic, msg):
    new_record = msg.header.stamp.sec + ',' \
                 + msg.header.stamp.nsec + ',' \
                 + msg.pose.position.x + ',' \
                 + msg.pose.position.y + ',' \
                 + msg.pose.position.z + ',' \
                 + msg.pose.orientation.x + ',' \
                 + msg.pose.orientation.y + ',' \
                 + msg.pose.orientation.z + ',' \
                 + msg.pose.orientation.w + ',' \
                 + msg.twist.linear.x + ',' \
                 + msg.twist.linear.y + ',' \
                 + msg.twist.linear.z + ',' \
                 + msg.twist.angular.x + ',' \
                 + msg.twist.angular.y + ',' \
                 + msg.twist.angular.z + ',' \
                 + msg.dtlane.dist + ',' \
                 + msg.dtlane.dir + ',' \
                 + msg.dtlane.apara + ',' \
                 + msg.dtlane.r + ',' \
                 + msg.dtlane.slope + ',' \
                 + msg.dtlane.cant + ',' \
                 + msg.dtlane.lw + ',' \
                 + msg.dtlane.rw

    out_file.write(new_record)

    
def dump_twist(out_file, topic, msg):
    new_record = msg.header.stamp.sec + ',' \
                 + msg.header.stamp.nsec + ',' \
                 + msg.twist.linear.x + ',' \
                 + msg.twist.linear.y + ',' \
                 + msg.twist.linear.z + ',' \
                 + msg.twist.angular.x + ',' \
                 + msg.twist.angular.y + ',' \
                 + msg.twist.angular.z

    out_file.write(new_record)


if __name__ == '__main__':
    in_bag = rosbag.Bag(sys.argv[1], 'r')

    out_path = sys.argv[2]
    bw = open(out_path+'/bw.csv', 'w')
    fw = open(out_path+'/fw.csv', 'w')
    tc = open(out_path+'/tc.csv', 'w')

    wp_title = 'stamp.sec,' \
               + 'stamp.nsec,' \
               + 'pose.position.x,' \
               + 'pose.position.y,' \
               + 'pose.position.z,' \
               + 'pose.orientation.x,' \
               + 'pose.orientation.y,' \
               + 'pose.orientation.z,' \
               + 'pose.orientation.w,' \
               + 'twist.linear.x,' \
               + 'twist.linear.y,' \
               + 'twist.linear.z,' \
               + 'twist.angular.x,' \
               + 'twist.angular.y,' \
               + 'twist.angular.z,' \
               + 'dtlane.dist,' \
               + 'dtlane.dir,' \
               + 'dtlane.apara,' \
               + 'dtlane.r,' \
               + 'dtlane.slope,' \
               + 'dtlane.cant,' \
               + 'dtlane.lw,' \
               + 'dtlane.rw'

    twist_title = 'stamp.sec,' \
                  + 'stamp.nsec,' \
                  + 'twist.linear.x,' \
                  + 'twist.linear.y,' \
                  + 'twist.linear.z,' \
                  + 'twist.angular.x,' \
                  + 'twist.angular.y,' \
                  + 'twist.angular.z' \

    bw.write(wp_title)
    fw.write(wp_title)
    tc.write(twist_title)
    
    for topic, msg, t in in_bag.read_messages():
        if topic == '/base_waypoints':
            dump_waypoints(topic, msg)
        elif topic == '/final_waypoints':
            dump_waypoints(topic, msg)
        elif topic == '/twist_cmd':
            dump_twist(topic, msg)
        else:
            pass

    bw.close()
    fw.close()
    tc.close()
