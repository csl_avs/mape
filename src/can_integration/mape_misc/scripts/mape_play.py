#!/usr/bin/env python
import rosbag
import rospy
from nmea_msgs.msg import Sentence
from sensor_msgs.msg import PointCloud2

# def publish_nmea(pub, rate):
    

if __name__ == '__main__':
    # play the fucking bag file with real timestamp
    rospy.init_node("mape_player")
    pub_nmea = rospy.Publisher('/nmea_sentence', Sentence, queue_size=100)
    pub_pc2 = rospy.Publisher('/points_raw', PointCloud2, queue_size=100)

    # nmea_r = rospy.Rate(25)
    # pc2_r = rospy.Rate(10)
    for topic, msg, t in rosbag.Bag("/data/storage/autoware_bag/autoware_100s.bag").read_messages():
        # new_ts = rospy.get_time()
        msg.header.stamp = rospy.Time.now();
        if topic == '/nmea_sentence':
            pub_nmea.publish(msg)
        else:
            pub_pc2.publish(msg)

    print("Publish completed, Now you can quit.")
    rospy.spin();
    
