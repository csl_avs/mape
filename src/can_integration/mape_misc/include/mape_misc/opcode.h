#include <stdio.h>

typedef enum {
    START = 1,
    STOP = 2,
    FREEZE = 3,
    RESTART = 4,
    RESUME = 5,
    EXECUTE = 11,
    ROSLAUNCH = 12,
    ROSRUN = 13
} Opcode;
