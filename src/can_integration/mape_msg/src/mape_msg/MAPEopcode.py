#!/usr/bin/env python
from enum import Enum


class OPCODE(Enum):
    NULL = 0
    START = 1
    STOP = 2
    FREEZE = 3
    RESTART = 4
    EXECUTE = 11
