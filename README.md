# README #

Cyber Security Lab - Autonomous Vehicle Security Project

NOTE: Code has been modified to run on ROS Kinetic and cope with Autoware 1.5.1

### What is this repository for? ###

CSL-MAPE (Cyber Security Lab - Monitor Analysis Plan Execute) project repo.
It aims to provide a security guard for ROS-based autonomous driving systems.

### How to use/compile the code ###

ROS Kinetic is recommended, better with Ubuntu 16.04 LTS AMD64

For legacy version on Indigo, please clone the 'indigo' branch
For the 'indigo' branch, ROS Indigo is recommended, better with Ubuntu 14.04 Lts AMD64.

Please put src/ under your catkin_ws to compile it.
Gcc >= 4.7 and python >= 2.7.6 (3.x.x not supported and tested!) are required.

Follow http://wiki.ros.org/indigo/Installation/Ubuntu to install catkin toolchain.

Follow http://wiki.ros.org/catkin/Tutorials/create_a_workspace to set up and build source code.

### A quick guide ###
1. src/mape-msg contains all the messages will be used. Add your own message defininations and include them in your package

2. src/monitor contains all monitoring nodes. Add you own code and define a corresponding node, a separate folder in src/monitor/src/your_new_node is encouraged. Adjust makefiles after you have done all changes.

3. src/analyzer contains all analysis nodes.

4. src/planner is responsible for gather analysis results and plans changes.Written by python.

5. src/executor is responsible for currying out the plan decision. Written by python. Currently executor can execute decisions at both container level and ROS node(process) level.

6. src/mape_misc is used to store public python modules or c++ libs.
